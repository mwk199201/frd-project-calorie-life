import { compare, hash } from 'bcryptjs'

// deepcode ignore HardcodedSecret: this is number of round, not salt
const ROUND = 12

export function hashPassword(password: string) {
  return hash(password, ROUND)
}

export function comparePassword(input: {
  password: string
  hash_password: string
}) {
  return compare(input.password, input.hash_password)
}

