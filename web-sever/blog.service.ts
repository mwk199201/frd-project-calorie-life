import { Knex } from "knex";
import { CommentItem } from "./api.types";


export class BlogService {
    constructor(private knex: Knex) { }

    async getBlogList(user_id: number) {

        let result = await this.knex.raw(/* sql */ `
    select
      blog.id,
      blog.poster_id,
      blog.content,
      blog.updated_at,
      profile.nickname,
      profile.icon_image,
      blog.title,
      blog.image,
      (select count(*)from blog_like
        where blog_like.blog_id = blog.id
      )as like_count,
      (select count(*) 
       from blog_like 
       where blog_like.blog_id = blog.id 
         and blog_like.user_id = ?
      ) as has_like,
      (select count(*) 
       from blog_save 
       where blog_save.blog_id = blog.id 
         and blog_save.user_id = ?
      ) as has_save,
      (select count(*) 
       from blog_block 
       where blog_block.blog_id = blog.id 
         and blog_block.user_id = ?
      ) as has_block,
    (select count(*) 
     from blog_report 
     where blog_report.blog_id = blog.id 
       and blog_report.user_id = ?
    ) as has_report
    from blog
    left join blog_like on blog_like.blog_id = blog.id
    left join profile on blog.poster_id = profile.user_id
    left join blog_save on blog_save.blog_id = blog.id
    left join blog_block on blog_block.blog_id = blog.id
    left join blog_report on blog_report.blog_id = blog.id
    group by blog.id, profile.nickname, profile.icon_image
    ORDER BY blog.updated_at DESC;

  `, [user_id, user_id, user_id, user_id])


        return { blogList: result.rows }
    }


    async getBlogListBySearch(user_id: number, value: string) {

        let result = await this.knex.raw(/* sql */ `
    select
      blog.id,
      blog.poster_id,
      blog.content,
      blog.updated_at,
      profile.nickname,
      profile.icon_image,
      blog.title,
      blog.image,
      (select count(*)from blog_like
        where blog_like.blog_id = blog.id
      )as like_count,
      (select count(*) 
       from blog_like 
       where blog_like.blog_id = blog.id 
         and blog_like.user_id = ?
      ) as has_like,
      (select count(*) 
       from blog_save 
       where blog_save.blog_id = blog.id 
         and blog_save.user_id = ?
      ) as has_save,
      (select count(*) 
       from blog_block 
       where blog_block.blog_id = blog.id 
         and blog_block.user_id = ?
      ) as has_block,
    (select count(*) 
     from blog_report 
     where blog_report.blog_id = blog.id 
       and blog_report.user_id = ?
    ) as has_report
    from blog
    left join blog_like on blog_like.blog_id = blog.id
    left join profile on blog.poster_id = profile.user_id
    left join blog_save on blog_save.blog_id = blog.id
    left join blog_block on blog_block.blog_id = blog.id
    left join blog_report on blog_report.blog_id = blog.id
    where blog.title ILIKE ?
    group by blog.id, profile.nickname, profile.icon_image
    ORDER BY blog.updated_at DESC;

  `, [user_id, user_id, user_id, user_id, `%${value}%`])


        return { blogList: result.rows }
    }

    async getComment(blog_id: number, counter: number) {
        const limit = 5 * counter
        // const offset = limit * counter
        // console.log('counter', counter, 'limit', limit,);

        let comment_records: CommentItem[] = await this.knex.raw(/*sql*/`
            select
            comment_record.id,
            profile.nickname,
            profile.icon_image,
            comment_record.content,
            comment_record.updated_at
            from comment_record
            left join profile on profile.user_id = comment_record.commenter_id
            where blog_id = ?
        ORDER BY comment_record.updated_at DESC
        LIMIT ? 
        `, [blog_id, limit])
        // lIMIT = ${limit} OFFSET = ${limit ** counter}

        return comment_records
    }


    async postComment(blog_id: number, content: string, user_id: number) {

        await this.knex.insert([
            {
                content: content,
                blog_id: blog_id,
                commenter_id: user_id
            }
        ]).into('comment_record')



    }

    async addLike(blog_id: number, user_id: number) {

        await this.knex.insert([
            {
                user_id: user_id,
                blog_id: blog_id
            }
        ]).into('blog_like')
    }

    async deleteLike(blog_id: number, user_id: number) {

        await this.knex('blog_like')
            .where({
                'blog_id': `${blog_id}`,
                'user_id': `${user_id}`
            })
            .del()
    }

    async postBlog(user_id: number, content: string | string[], title: string | string[], image?: string) {
        // console.log(image);

        let id = await this.knex.insert([

            {
                poster_id: user_id,
                title: title,
                content: content,
                image: image
            }
        ]).into("blog")
            .returning('id')
        console.log(id, 'userid', user_id)
        id = id[0].id
        await this.knex.insert([
            {
                user_id: user_id,
                blog_id: id,
            }
        ]).into("blog_history")
    }

    async saveBlog(blog_id: number, user_id: number) {
        // console.log("save blog", user_id, blog_id);
        await this.knex.insert([
            {
                user_id: user_id,
                blog_id: blog_id
            }
        ]).into('blog_save')

        let saved_blog_id = await this.knex.select('blog_id')
            .from('blog_save').where('user_id', `${user_id}`)

        return saved_blog_id

    }

    async cancelSaveBlog(blog_id: number, user_id: number) {

        await this.knex('blog_save')
            .where({
                'blog_id': `${blog_id}`,
                'user_id': `${user_id}`
            })
            .del()
    }
    async cancelBlockedBlog(blog_id: number, user_id: number) {

        await this.knex('blog_block')
            .where({
                'blog_id': `${blog_id}`,
                'user_id': `${user_id}`
            })
            .del()

        return { success: true }
    }

    async blockBlog(blog_id: number, user_id: number) {
        // console.log("block blog", user_id, blog_id);
        await this.knex.insert([
            {
                user_id: user_id,
                blog_id: blog_id
            }
        ]).into('blog_block')

    }
    async reportBlog(blog_id: number, user_id: number, statement: string) {
        // console.log("report blog", user_id, blog_id);
        await this.knex.insert([
            {
                user_id: user_id,
                blog_id: blog_id,
                statement: statement
            }
        ]).into('blog_report')

    }

    async deleteBlog(blog_id: number, user_id: number) {
        await this.knex('blog_history')
            .where({
                'blog_id': `${blog_id}`,
            })
            .del()
        await this.knex('blog_block')
            .where({
                'blog_id': `${blog_id}`,
            })
            .del()
        await this.knex('blog_save')
            .where({
                'blog_id': `${blog_id}`,
            })
            .del()
        await this.knex('blog_report')
            .where({
                'blog_id': `${blog_id}`,
            })
            .del()
        await this.knex('blog_like')
            .where({
                'blog_id': `${blog_id}`,
            })
            .del()
        await this.knex('comment_record')
            .where({
                'blog_id': `${blog_id}`,
            })
            .del()

        await this.knex('blog')
            .where({
                'id': `${blog_id}`,
            })
            .del()


        return { success: true }
    }

    async blogHistory(blog_id: number, user_id: number) {
        await this.knex.insert([
            {
                user_id: user_id,
                blog_id: blog_id
            }
        ]).into('blog_history')
    }


}