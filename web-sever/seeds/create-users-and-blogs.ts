// import { CloudWatchLogs } from "aws-sdk";
import { Knex } from "knex";
import path from "path";
import xlsx from 'xlsx'
import { hashPassword } from "../hash";

type User = {
    username: string
    password: string
    email: string | null
    phone_number: string | null
    admin: boolean
    payment: boolean

}
type Profile = {
    username: string
    name: string
    height: number
    weight: number
    age: number
    icon_image: string | null
    aim: string
    user_role: string
    activity: number
    gender: string
}

type Blog = {
    username: string
    content: string
    image: string | null
    blog_key: number
}

type Comment_record = {
    commenter: string
    content: string
    blog_key: number
}

type Blog_ike = {
    username: string
    blog_key: number
}

type Room = {
    user1_name: string
    user2_name: string
}
type Chatroom_record = {
    sender_name: string
    room_id: number
    message: string
    image:string
}
type User_read_message = {
    username: string
    room_id: number
    last_message_id: number
}


let calories_path = path.resolve(__dirname, "../excel/calorieslife.xlsx")
let workbook = xlsx.readFile(calories_path)

let users = xlsx.utils.sheet_to_json<User>(workbook.Sheets.user, { raw: true })
let profiles = xlsx.utils.sheet_to_json<Profile>(workbook.Sheets.profile, { raw: true })
let blogs = xlsx.utils.sheet_to_json<Blog>(workbook.Sheets.blog, { raw: true })
let comment_records = xlsx.utils.sheet_to_json<Comment_record>(workbook.Sheets.comment_record, { raw: true })
let likes = xlsx.utils.sheet_to_json<Blog_ike>(workbook.Sheets.like, { raw: true })
let rooms = xlsx.utils.sheet_to_json<Room>(workbook.Sheets.room, { raw: true })
let chatroom_records = xlsx.utils.sheet_to_json<Chatroom_record>(workbook.Sheets.charoom_record, { raw: true })
let user_read_messages = xlsx.utils.sheet_to_json<User_read_message>(workbook.Sheets.user_read_message, { raw: true })
export async function seed(knex: Knex): Promise<void> {



    let id: number
    let user_id_dict: Record<string, number> = {}

    let blog_key: number
    let blog_id_dict: Record<number, number> = {}

    let last_message_id: number
    let last_message_id_dict: Record<string, number> = {}

    for (let user of users) {
        // console.log(user);

        let row = await knex
            .select('id')
            .from('user')
            .where({ username: user.username })
            .first()
        if (!row) {
            let rows = await knex
                .insert({
                    username: user.username,
                    hash_password: await hashPassword(user.password.toString()),
                    email: user.email,
                    phone_number: user.phone_number,
                    admin: user.admin,
                    payment: user.payment
                }).into('user')
                .returning('id')
            id = rows[0].id
        } else {
            id = row.id
        }
        user_id_dict[user.username] = id
    }
    let user_list = Object.keys(user_id_dict)
    // console.log(user_list);


    for (let profile of profiles) {
        // console.log(profile);

        let user_id = user_id_dict[profile.username]
        let nickname = profile.name
        let height = profile.height
        let weight = profile.weight
        let age = profile.age
        let icon_image = profile.icon_image
        let aim = profile.aim
        let role = profile.user_role
        let activity = profile.activity
        let gender = profile.gender

        let row = await knex
            .select('id')
            .from('profile')
            .where({ user_id: user_id })
            .first()
        if (!row) {
            let row = await knex
                .insert({
                    user_id: user_id,
                    nickname: nickname,
                    height: height,
                    weight: weight,
                    age: age,
                    icon_image: icon_image,
                    aim: aim,
                    user_role: role,
                    activity: activity,
                    gender: gender
                }).into('profile')


        }
    }

    for (let blog of blogs) {
        // console.log(blog);

        let user_id = user_id_dict[blog.username]

        let row = await knex
            .select('id')
            .from('blog')
            .where({ content: blog.content })
            .first()
        if (!row) {
            let rows = await knex
                .insert({
                    poster_id: user_id,
                    content: blog.content,
                    image: blog.image,
                }).into('blog')
                .returning('id')
            id = rows[0].id

        }
        //  else {
        //     id = row.id
        // }
        // blog_id_dict[blog.blog_key] = id
    }
    // console.log('blog_id_dict', blog_id_dict);

    for (let comment_record of comment_records) {
        // console.log(comment_record);
        let user_id = user_id_dict[comment_record.commenter]

        let row = await knex
            .select('id')
            .from('comment_record')
            .where({ content: comment_record.content })
            .first()
        if (!row) {
            let rows = await knex
                .insert({
                    commenter_id: user_id,
                    blog_id: comment_record.blog_key,
                    content: comment_record.content
                }).into('comment_record')
        }

    }


    for (let like of likes) {


        let user_id = user_id_dict[like.username]
        let row = await knex
            .select('id')
            .from('blog_like')
            .where({ user_id: user_id })
            .andWhere({ blog_id: like.blog_key })
            .first()
        if (!row) {

            let rows = await knex
                .insert({
                    user_id: user_id,
                    blog_id: like.blog_key
                }).into('blog_like')
        }
    }

    for (let room of rooms) {
        let user1_id = user_id_dict[room.user1_name]
        let user2_id = user_id_dict[room.user2_name]
        let row = await knex
            .select('id')
            .from('room')
            .where({ user1_id: user1_id })
            .andWhere({ user2_id: user2_id })
            .first()

        if (!row) {

            let rows = await knex
                .insert({
                    user1_id: user1_id,
                    user2_id: user2_id,

                }).into('room')
        }
    }


    for (let chatroom_record of chatroom_records) {

        let sender_id = user_id_dict[chatroom_record.sender_name]
        console.log("chatroom_record", chatroom_record);
        let row = await knex
            .select('id')
            .from('chatroom_record')
            .where({ sender_id: sender_id })
            .first()
        if (!row) {

            let rows = await knex
                .insert({
                    sender_id: sender_id,
                    room_id: chatroom_record.room_id,
                    message: chatroom_record.message,
                    image:chatroom_record.image


                }).into('chatroom_record')
        }
    }
    for (let user_read_message of user_read_messages) {
        console.log(user_read_message);

        let user_id = user_id_dict[user_read_message.username]
        let row = await knex
            .select('id')
            .from('user_read_message')
            .where({ user_id: user_id })
            .andWhere({ chatroom_record: user_read_message.room_id })
            .first()
        if (!row) {

            let rows = await knex
                .insert({
                    user_id: user_id,
                    chatroom_record: user_read_message.room_id,
                    message: user_read_message.last_message_id
                }).into('user_read_message')
        }
    }




};
