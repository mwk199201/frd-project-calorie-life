import { Knex } from "knex";
import path from "path";
import xlsx from 'xlsx'

type Food = {
    food_chi_name: string;
    food_eng_name: string;
    food_type: string;
    food_portion: number;
    food_calories: number;
    protein: number;
    carbohydrate: number;
    total_fat: number;
    dietary_fibre: number;
    sugars: number;
    saturated_fat: number;
    trans_fat: number;
    cholesterol: number;
    sodium: number;
}

type Exercise = {
    exercise_type: string;
    exercise_mins: number;
    exercise_METs: number;
}

let food_path = path.resolve(__dirname, "../excel/food_nutrient.xlsx")
let workbook = xlsx.readFile(food_path)

let food_nutrient = xlsx.utils.sheet_to_json<Food>(workbook.Sheets.food_nutrient, { raw: true })
let exercises = xlsx.utils.sheet_to_json<Exercise>(workbook.Sheets.exercise_calories, { raw: true })

// for (let exercise of exercises){
//     console.log(exercise)
// }

export async function seed(knex: Knex): Promise<void> {
    for (let nutrient of food_nutrient) {
        let row = await knex
            .select('id')
            .from('food_calories_data')
            .where({ food_chi_name: nutrient.food_chi_name })
            .first()

        if (!row) {
            let rows = await knex
                .insert({
                    food_chi_name: nutrient.food_chi_name,
                    food_eng_name: nutrient.food_eng_name,
                    food_type: nutrient.food_type,
                    food_weight: nutrient.food_portion,
                    calories: nutrient.food_calories,
                    protein: nutrient.protein,
                    carbohydrate: nutrient.carbohydrate,
                    total_fat: nutrient.total_fat,
                    dietary_fibre: nutrient.dietary_fibre,
                    sugar: nutrient.sugars,
                    saturated_fat: nutrient.saturated_fat,
                    trans_fat: nutrient.trans_fat,
                    cholesterol: nutrient.cholesterol,
                    sodium: nutrient.sodium,
                    isselected: false
                }).into('food_calories_data')
        }
    }

    for(let exercise of exercises){
        let row = await knex
        .select('id')
        .from('exercise_calories_data')
        .where({ exercise_type: exercise.exercise_type })
        .first()

        if (!row) {
            let rows = await knex
                .insert({
                    exercise_type: exercise.exercise_type,
                    exercise_mins: exercise.exercise_mins,
                    exercise_METs: exercise.exercise_METs,
                    isselected: false
                }).into('exercise_calories_data')
    }
}}