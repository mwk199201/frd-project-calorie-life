import { Server } from "socket.io";
import { Knex } from "knex";
import formidable from "formidable";

export class ChatItemService {
  sendMessage: any;
  constructor(private knex: Knex, private io: Server) { }

  async listChatItem(filter: { user_id: number }) {
    let result = await this.knex.raw(
      /* sql */ `
      with roomlist as (
        select
            room.id,
            (
                case
                    when user1_id = ? then user2_id
                    else user1_id
                end
            ) as other_user_id
        from
            room
        where
            user1_id = ?
            or user2_id = ?
    ),
    messagelist as (
        select
            max(chatroom_record.id) as message_id
        from
            chatroom_record
        where
            room_id in (
                select
                    roomlist.id
                from
                    roomlist
            )
        group by
            room_id
    )
    select
        chatroom_record.id as message_id,
        chatroom_record.created_at,
        roomlist.id as room_id,
        message,
        sender_id,
        other_user_id,
        nickname,
        icon_image,
        user_role
    from
        chatroom_record
        right outer join roomlist on roomlist.id = room_id
        inner join profile on profile.user_id = other_user_id
    where
        chatroom_record.id in (
            select
                message_id
            from
                messagelist
        ) OR chatroom_record.id is NULL
    ORDER BY
        chatroom_record.updated_at DESC;
      `,
      [filter.user_id, filter.user_id, filter.user_id]
    );

    // console.log(result.rows.length);
    let rooms = result["rows"];
    return rooms;
  }

  async getRoomId(input: { user1_id: number; user2_id: number }) {
    return this.knex.transaction(async (knex) => {
      let user1_id = input.user1_id;
      let user2_id = input.user2_id;
      if (user1_id > user2_id) {
        user1_id = input.user2_id;
        user2_id = input.user1_id;
      }
      let row = await knex
        .select("id")
        .from("room")
        // .where("user1_id",user1_id) .andWhere('user2_id',user2_id)
        .where({ user1_id, user2_id })
        .first();
      if (row) {
        return row.id;
      }
      // [row] = await knex("room").insert({ user1_id, user2_id }).returning("id");
      const room = await knex("room")
        .insert({ user1_id, user2_id })
        .returning("id");
      return room[0].id;
    });
  }

  async getOtherUserId(
    input: { user_id: number; room_id: number },
    knex = this.knex
  ) {
    let room = await knex
      .select("user1_id", "user2_id")
      .from("room")
      .where("room.id", input.room_id)
      .first();
    if (!room) {
      return false;
    }
    if (!(room.user1_id == input.user_id || room.user2_id == input.user_id)) {
      return false;
    }
    let other_user_id =
      room.user1_id == input.user_id ? room.user2_id : room.user1_id;
    return other_user_id;
  }

  async getRoomMessages(input: { user_id: number; room_id: number }) {
    return this.knex.transaction(async (knex) => {
      let other_user_id = await this.getOtherUserId(input, knex);
      let other_user = await knex
        .select("nickname", "user_id", "icon_image")
        .from("profile")
        .where("user_id", other_user_id)
        .first();
      const messages = await knex
        .select("sender_id", "message",'created_at','image','id')
        .from("chatroom_record")
        .where("room_id", input.room_id)
      return { other_user, messages };  
    });
  }

  async sendChatItem(input: {
    user_id: number;
    room_id: number;
    content: string[];
    image: formidable.File[];
  }) {
    return this.knex.transaction(async (knex) => {
      let other_user_id = await this.getOtherUserId(input, knex);
      if (!other_user_id) {
        return false;
      }
      let sender_id = input.user_id;
      const messages = input.content.map((_, i) => ({
        room_id: input.room_id,
        sender_id,
        message: input.content[i],
        image: input.image[i]?.newFilename || null,

      }));
await knex.insert(messages).into("chatroom_record")
      return messages 
      
    });
  }

  async deleteChatItem(input: {
    user_id: number;
    room_id: number;
    message_id: number;
  }) {
    console.log(`the is${input.user_id}& ${input.message_id}`)
    console.log(`theno is${input.user_id}& ${input.message_id}`)

    return this.knex.transaction(async (knex) => {
      let other_user_id = await this.getOtherUserId(input, knex);
      if (!other_user_id) {
        return false;
      }
      await knex('chatroom_record')
        .where({ id: input.message_id, sender_id: input.user_id })
        .del();
    });
  }
}
