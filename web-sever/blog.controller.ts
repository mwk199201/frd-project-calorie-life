import { Request } from "express";
import { BlogService } from "./blog.service";
import { FileController } from "./file.controller";
import { getJWTPayload } from "./guards";
// import { HttpError } from "./http.error";
import { RestController } from "./rest.controller";
// import { UpdateAvatarResult } from "./api.types";


export class BlogController extends RestController {
    constructor(private blogService: BlogService,
        private fileController: FileController

    ) {
        super()
        this.router.get('/blogs', this.wrapControllerMethod(this.getBlogList))
        this.router.post('/blog', this.wrapControllerMethod(this.postBlog))
        this.router.post('/blog/:blog_id', this.wrapControllerMethod(this.getComment))
        this.router.post('/blogs/search', this.wrapControllerMethod(this.getBlogListBySearch))
        this.router.post('/blog/post/:blog_id', this.wrapControllerMethod(this.postComment))
        this.router.post('/blog/:blog_id/like', this.wrapControllerMethod(this.addLike))
        this.router.post('/blog/:blog_id/save', this.wrapControllerMethod(this.saveBlog))
        this.router.post('/blog/:blog_id/block', this.wrapControllerMethod(this.blockBlog))
        this.router.post('/blog/:blog_id/report', this.wrapControllerMethod(this.reportBlog))
        this.router.delete('/blog/:blog_id/save', this.wrapControllerMethod(this.cancelSaveBlog))
        this.router.delete('/blog/:blog_id/like', this.wrapControllerMethod(this.deleteLike))
        this.router.delete('/blog/:blog_id/block', this.wrapControllerMethod(this.cancelBlockedBlog))
        this.router.delete('/blog/del/:blog_id', this.wrapControllerMethod(this.deleteBlog))

    }

    getBlogList = (req: Request) => {
        let payload = getJWTPayload(req)
        return this.blogService.getBlogList(payload.id)
    }

    getBlogListBySearch = (req: Request) => {
        let payload = getJWTPayload(req)
        let value = req.body.value
        return this.blogService.getBlogListBySearch(payload.id, value)

    }




    getComment = async (req: Request) => {


        let comment_records = await this.blogService.getComment(req.body.id, req.body.counter)
        return { comment_records }

    }

    postComment = async (req: Request) => {

        let payload = getJWTPayload(req)
        let blog_id = +req.params.blog_id
        await this.blogService.postComment(blog_id, req.body.commentContent, payload.id)
        let comment_records = await this.blogService.getComment(req.body.blog_id, 1)
        return { ok: true, comment_records }
    }

    addLike = async (req: Request) => {
        let payload = getJWTPayload(req)
        let blog_id = +req.params.blog_id

        await this.blogService.addLike(blog_id, payload.id)
        return { ok: true }
    }

    deleteLike = async (req: Request) => {
        let payload = getJWTPayload(req)
        let blog_id = +req.params.blog_id
        // console.log(blog_id);


        await this.blogService.deleteLike(blog_id, payload.id)
        return { ok: false }
    }

    postBlog = async (req: Request) => {
        let payload = getJWTPayload(req)
        let user_id = payload.id
        // let content = req.body.content
        // let image = req.body?.image
        let input = await this.fileController.upload({
            req,
            fields: [{ name: "image", mimeTypePrefix: 'image/' }],

        })
        let image = input.files.image
        let file = Array.isArray(image) ? image[0] : image
        let filename = file?.newFilename

        let content = input.fields.content
        let title = input.fields.title

        await this.blogService.postBlog(user_id, content, title, filename)


        // console.log('image', image);
        // console.log('content', content);

        // console.log('newBlog', req.body);


        return { ok: true }
    }

    saveBlog = async (req: Request) => {
        let payload = getJWTPayload(req)
        let blog_id = +req.params.blog_id

        return await this.blogService.saveBlog(blog_id, payload.id)

    }
    cancelSaveBlog = async (req: Request) => {
        let payload = getJWTPayload(req)
        let blog_id = +req.params.blog_id
        // console.log(blog_id);


        await this.blogService.cancelSaveBlog(blog_id, payload.id)
        return { ok: false }
    }

    blockBlog = async (req: Request) => {
        let payload = getJWTPayload(req)
        let blog_id = +req.params.blog_id

        return await this.blogService.blockBlog(blog_id, payload.id)

    }
    cancelBlockedBlog = async (req: Request) => {
        let payload = getJWTPayload(req)
        let blog_id = +req.params.blog_id
        // console.log(blog_id);


        await this.blogService.cancelBlockedBlog(blog_id, payload.id)
        return { ok: true }
    }
    reportBlog = async (req: Request) => {

        let payload = getJWTPayload(req)
        let blog_id = +req.params.blog_id
        let statement = req.body.statement
        // console.log(statement);

        await this.blogService.reportBlog(blog_id, payload.id, statement)
        return { ok: true }
    }

    deleteBlog = async (req: Request) => {
        let payload = getJWTPayload(req)
        let blog_id = +req.params.blog_id
        return await this.blogService.deleteBlog(blog_id, payload.id)

    }


}