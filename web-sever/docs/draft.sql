with roomlist as (
    select
        room.id,
        (
            case
                when user1_id = ? then user2_id
                else user1_id
            end
        ) as other_user_id
    from
        room
    where
        user1_id = ?
        or user2_id = ?
),
messagelist as (
    select
        max(chatroom_record.id) as message_id
    from
        chatroom_record
    where
        room_id in (
            select
                roomlist.id
            from
                roomlist
        )
    group by
        room_id
)
select
    chatroom_record.id as message_id,
    room_id,
    message,
    sender_id,
    other_user_id,
    nickname,
    icon_image
from
    chatroom_record
    inner join roomlist on roomlist.id = room_id
    inner join profile on profile.user_id = other_user_id
where
    chatroom_record.id in (
        select
            message_id
        from
            messagelist
    )
ORDER BY
    chatroom_record.updated_at DESC;