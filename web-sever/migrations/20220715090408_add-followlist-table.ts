import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

    if (!(await knex.schema.hasTable('follow_list'))) {
        await knex.schema.createTable('follow_list', table => {
            table.increments('id')
            table.integer('leader_id').unsigned().notNullable().references('user.id')
            table.integer('follower_id').unsigned().notNullable().references('user.id')
            table.timestamps(false, true)
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('follow_list')

}

