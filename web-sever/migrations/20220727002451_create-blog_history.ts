import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('blog_history'))) {
        await knex.schema.createTable('blog_history', table => {
            table.increments('id')
            table.integer('user_id').unsigned().notNullable().references('user.id')
            table.integer('blog_id').unsigned().notNullable().references('blog.id')
            table.timestamps(false, true)
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('blog_history')
}

