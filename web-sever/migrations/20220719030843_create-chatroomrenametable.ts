import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    `
     ALTER TABLE user_read_message RENAME COLUMN chatroom_record TO last_message_id;
     CREATE TYPE room_id AS foreign key ('room.id');
     ALTER TABLE profile ADD COLUMN room_id room_id NOT NULL;
    `
   
}


export async function down(knex: Knex): Promise<void> {
    `
    ALTER TABLE "user_read_message" RENAME COLUMN  last_message_id  TO chatroom_record;
    ALTER TABLE user_read_message DROP COLUMN room_id; DROP TYPE room_id;
   `
}

