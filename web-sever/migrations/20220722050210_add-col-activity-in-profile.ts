import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE profile ADD COLUMN activity integer not null;
    ALTER TABLE profile ADD COLUMN gender varchar(10) not null;

  `)
}


export async function down(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE profile DROP COLUMN activity;
    ALTER TABLE profile DROP COLUMN gender;

  `)
}

