import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE "daily_diet" ADD COLUMN food_weight integer;

  `)
}


export async function down(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE "daily_diet" DROP COLUMN food_weight;
  `)
}

