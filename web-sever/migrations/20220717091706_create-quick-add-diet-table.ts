import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('quick_add_diet'))) {
        await knex.schema.createTable('quick_add_diet', table => {
          table.increments('id')
          table.integer('user_id').unsigned().notNullable().references('user.id')
          table.string('food_name', 255).notNullable()
          table.string('diet_type').notNullable()
          table.integer('calories').notNullable()
          table.float('protein').notNullable()
          table.float('carbohydrate').notNullable()
          table.float('fat').notNullable()
          table.timestamps(false, true)
        })
      }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('quick_add_diet')
}

