import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('blog_save'))) {
        await knex.schema.createTable('blog_save', table => {
            table.increments('id')
            table.integer('user_id').unsigned().notNullable().references('user.id')
            table.integer('blog_id').unsigned().notNullable().references('blog.id')
            table.timestamps(false, true)
        })
    }
    if (!(await knex.schema.hasTable('blog_block'))) {
        await knex.schema.createTable('blog_block', table => {
            table.increments('id')
            table.integer('user_id').unsigned().notNullable().references('user.id')
            table.integer('blog_id').unsigned().notNullable().references('blog.id')
            table.timestamps(false, true)
        })
    } if (!(await knex.schema.hasTable('blog_report'))) {
        await knex.schema.createTable('blog_report', table => {
            table.increments('id')
            table.integer('user_id').unsigned().notNullable().references('user.id')
            table.integer('blog_id').unsigned().notNullable().references('blog.id')
            table.string('statement', 255).notNullable()
            table.timestamps(false, true)
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('blog_report')
    await knex.schema.dropTableIfExists('blog_save')
    await knex.schema.dropTableIfExists('blog_block')

}

