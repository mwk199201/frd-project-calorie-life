import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('daily_water'))) {
        await knex.schema.createTable('daily_water', table => {
          table.increments('id')
          table.integer('user_id').unsigned().notNullable().references('user.id')
          table.integer('cup_of_water').notNullable()
          table.timestamps(false, true)
        })
      }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('daily_water')
}


