import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE "profile" RENAME COLUMN name TO nickname;
    ALTER TABLE "like" RENAME TO blog_like;

  `)
}


export async function down(knex: Knex): Promise<void> {
  try{
    await knex.raw(/* sql */ `
    ALTER TABLE "profile" RENAME COLUMN nickname TO name;
    ALTER TABLE blog_like RENAME TO "like";
    `)
  }catch{

  }
}

