import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE blog ADD COLUMN title varchar(60) DEFAULT 'whatever' not null;

  `)
}


export async function down(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE blog DROP COLUMN title;

  `)
}

