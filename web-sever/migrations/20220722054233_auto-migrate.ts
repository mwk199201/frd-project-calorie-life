import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
 
 
  await knex.schema.alterTable('chatroom_record', table => {
    table.string('image', 255).nullable()
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('chatroom_record', table => {
    table.dropColumn('image')
  })
 
}
