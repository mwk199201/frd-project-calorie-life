import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE "exercise_calories_data" ADD COLUMN isSelected boolean;

  `)
}


export async function down(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE "exercise_calories_data" DROP COLUMN isSelected;

  `)
}
