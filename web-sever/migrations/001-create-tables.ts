import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  if (!(await knex.schema.hasTable('user'))) {
    await knex.schema.createTable('user', table => {
      table.increments('id')
      table.string('username', 255).notNullable().unique()
      table.string('hash_password').nullable()
      table.string('email', 255).nullable()
      table.integer('phone_number').nullable()
      table.boolean('admin').notNullable()
      table.boolean('payment').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('profile'))) {
    await knex.schema.createTable('profile', table => {
      table.increments('id')
      table.integer('user_id').unsigned().notNullable().references('user.id')
      table.string('name', 255).notNullable()
      table.integer('height').notNullable()
      table.integer('weight').notNullable()
      table.integer('age').notNullable()
      table.string('icon_image', 255).nullable()
      table.string('aim', 255).notNullable()
      // table.emun('admin','trainer','nutritionist','user')('role').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('daily_food_record'))) {
    await knex.schema.createTable('daily_food_record', table => {
      table.increments('id')
      table.integer('user_id').unsigned().notNullable().references('user.id')
      table.integer('daily_calories_ingest').notNullable()
      table.integer('daily_calories_absorb').notNullable()
      table.integer('daily_exercises_ingest').notNullable()
      table.integer('water_absorb').notNullable()
      table.integer('recommend_diet_percentage').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('daily_diet'))) {
    await knex.schema.createTable('daily_diet', table => {
      table.increments('id')
      table.integer('user_id').unsigned().notNullable().references('user.id')
      table.string('diet_type').notNullable()
      table.string('food_name', 255).notNullable()
      table.string('food_type', 255).notNullable()
      table.integer('calories').notNullable()
      table.float('protein').notNullable()
      table.float('carbohydrate').notNullable()
      table.float('total_fat').notNullable()
      table.float('sugar').notNullable()
      table.float('saturated_fat').notNullable()
      table.float('trans_fat').notNullable()
      table.float('cholesterol').notNullable()
      table.float('dietary').notNullable()
      table.float('sodium').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('daily_exercise_record'))) {
    await knex.schema.createTable('daily_exercise_record', table => {
      table.increments('id')
      table.integer('user_id').unsigned().notNullable().references('user.id')
      table.string('exercise_type', 255).notNullable()
      table.integer('exercise_mins').notNullable()
      table.integer('exercise_calories').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('food_calories_data'))) {
    await knex.schema.createTable('food_calories_data', table => {
      table.increments('id')
      table.string('food_chi_name', 255).notNullable()
      table.string('food_type', 255).notNullable()
      table.string('food_eng_name', 255).notNullable()
      table.integer('food_weight').notNullable()
      table.integer('calories').notNullable()
      table.float('protein').notNullable()
      table.float('carbohydrate').notNullable()
      table.float('total_fat').notNullable()
      table.float('sugar').notNullable()
      table.float('saturated_fat').notNullable()
      table.float('trans_fat').notNullable()
      table.float('cholesterol').notNullable()
      table.float('dietary_fibre').notNullable()
      table.float('sodium').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('exercise_calories_data'))) {
    await knex.schema.createTable('exercise_calories_data', table => {
      table.increments('id')
      table.string('exercise_type', 255).notNullable()
      table.integer('exercise_mins').notNullable()
      table.float('exercise_METs').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('blog'))) {
    await knex.schema.createTable('blog', table => {
      table.increments('id')
      table.integer('poster_id').unsigned().notNullable().references('user.id')
      table.text('content').notNullable()
      table.string('image', 255).nullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('comment_record'))) {
    await knex.schema.createTable('comment_record', table => {
      table.increments('id')
      table.integer('commenter_id').unsigned().notNullable().references('user.id')
      table.integer('blog_id').unsigned().notNullable().references('blog.id')
      table.string('content', 255).notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('like'))) {
    await knex.schema.createTable('like', table => {
      table.increments('id')
      table.integer('user_id').unsigned().notNullable().references('user.id')
      table.integer('blog_id').unsigned().notNullable().references('blog.id')
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('chatroom_record'))) {
    await knex.schema.createTable('chatroom_record', table => {
      table.increments('id')
      table.integer('sender_id').unsigned().notNullable().references('user.id')
      table.integer('receiver_id').unsigned().notNullable().references('user.id')
      table.string('message', 255).notNullable()
      table.timestamps(false, true)
    })
  }
}

export async function down(knex: Knex): Promise<void> {
  try{
  await knex.schema.dropTableIfExists('chatroom_record')
  await knex.schema.dropTableIfExists('like')
  await knex.schema.dropTableIfExists('comment_record')
  await knex.schema.dropTableIfExists('blog')
  await knex.schema.dropTableIfExists('exercise_calories_data')
  await knex.schema.dropTableIfExists('food_calories_data')
  await knex.schema.dropTableIfExists('daily_exercise_record')
  await knex.schema.dropTableIfExists('daily_diet')
  await knex.schema.dropTableIfExists('daily_food_record')
  await knex.schema.dropTableIfExists('profile')
  await knex.schema.dropTableIfExists('user')
  }catch{
    
  }
}
