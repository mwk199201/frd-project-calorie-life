import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.from('user_read_message ').delete()
  await knex.from('chatroom_record').delete()
  await knex.from('room').delete()

  await knex.schema.alterTable('room', table => {
    table.integer('user1_id').unsigned().notNullable().references('user.id')
    table.integer('user2_id').unsigned().notNullable().references('user.id')
  })
  await knex.schema.alterTable('chatroom_record', table => {
    table.integer('room_id').unsigned().notNullable().references('room.id')
    table.dropColumn('receiver_id')
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('chatroom_record', table => {
    table.integer('receiver_id').unsigned().notNullable().references('user.id')
    table.dropColumn('room_id')
  })
  await knex.schema.alterTable('room', table => {
    table.dropColumn('user2_id')
    table.dropColumn('user1_id')
  })

}
