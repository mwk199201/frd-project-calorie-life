import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('user_read_message'))) {
        await knex.schema.createTable('user_read_message', table => {
          table.increments('id')
          table.integer('user_id').unsigned().notNullable().references('user.id')
          table.integer('chatroom_record').unsigned().notNullable().references('chatroom_record.id')
          table.string('message', 255).notNullable()
        })
      }
}
    

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('user_read_message')
}

