import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    CREATE TYPE user_role AS ENUM ('admin','trainer','nutritionist','user');
    ALTER TABLE profile ADD COLUMN user_role user_role NOT NULL;
  `)
}


export async function down(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE profile DROP COLUMN user_role; DROP TYPE user_role;
  `)
}
