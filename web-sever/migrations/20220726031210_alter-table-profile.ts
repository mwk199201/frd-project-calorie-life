import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE profile ALTER COLUMN activity TYPE integer;
  `)
}


export async function down(knex: Knex): Promise<void> {
    await knex.raw(/* sql */ `
    ALTER TABLE profile ALTER COLUMN activity TYPE integer ;
  `)
}

