import console from "console";
import { Knex } from "knex";
// import { knex } from "./db";

export type ExercisesData = {
    exercise_type: string;
    exercise_mins: number;
    exercise_calories: number;
}

export type DailyDiet = {
    user_id: string | number
    food_name: string,
    diet_type: string,
    food_type: string,
    calories: number,
    protein: number,
    carbohydrate: number,
    total_fat: number,
    sugar: number,
    saturated_fat: number,
    trans_fat: number,
    cholesterol: number,
    dietary_fiber: number,
    sodium: number,
    food_weight: number,
}

export type QuickAddDiet = {
    user_id: string | number,
    food_name: string,
    diet_type: string,
    calories: number,
    protein: number,
    carbohydrate: number,
    fat: number,
}

export type QuickAddDietPatchItem = {
    calories: number,
    protein: number,
    carbohydrate: number,
    fat: number,
}

export type RecentFoodList = {
    calories: number;
    carbohydrate: number;
    cholesterol: number;
    dietary: number;
    food_weight: number;
    protein: number;
    saturated_fat: number;
    sodium: number;
    sugar: number;
    total_fat: number;
    trans_fat: number;
};

export class FoodDataService {
    constructor(private knex: Knex) { }
    async getFoodDataByName(searchItem: string) {
        let foodData = await this.knex.select('*').from('food_calories_data').where('food_eng_name', 'like', `%${searchItem}%`)
        return { foodData }
    }

    async getExerciseDataByName(searchItem: string) {
        let exerciseData = await this.knex.select('*').from('exercise_calories_data').where('exercise_type', 'like', `%${searchItem}%`)
        return { exerciseData }
    }

    async getFoodDataByID(foodID: string | number) {
        // console.log(foodID)
        let foodData = await this.knex.select('*').from('food_calories_data').where('id', foodID)
        return { foodData }
    }

    async getFoodDataRecords(date:string,user_id:number){
        let nextDay=new Date(date).getDate()+1
        var nextDate=new Date()
        nextDate.setDate(nextDay)
        let endDate = nextDate.toISOString().split('T')[0]
        // console.log(endDate)
        // console.log(date)
        let foodData = await this.knex.select('*').from('daily_diet')
        .where({user_id})
        .where('created_at','>=',date)
        .where('created_at','<',endDate)
        // console.log(foodData)
        return foodData
    }

    async getQuickAddFoodDataRecord(date:string,id:number){
        // console.log('date', date)
        let nextDay = new Date(date).getDate() + 1
        var nextDate = new Date()
        nextDate.setDate(nextDay)
        let endDate = nextDate.toISOString().split('T')[0]
        // console.log(endDate)
        let foodData = await this.knex.select('*').from('quick_add_diet')
        .where('user_id',id)
        .where('created_at','>=',date)
        .where('created_at','<',endDate)
        

        // let foodData = await this.knex.raw(/*sql*/`
        // select id,user_id,food_name,diet_type,calories,protein,carbohydrate,fat
        // from quick_add_diet where created_at between ? and ?`,
        // [date,endOfDay])
        // console.log(foodData)
        // let foodData = await this.knex.select('*').from('quick_add_diet')
        // console.log(foodData)
        return foodData
    }

    async getQuickFoodDataByID(foodID: string | number) {
        // console.log('quickAddFoodID',foodID)
        let foodData = await this.knex.select('*').from('quick_add_diet').where('id', foodID)
        return foodData
    }

    async getRecentFoodDataByID(foodID: string | number) {
        // console.log('quickAddFoodID',foodID)
        let foodData = await this.knex.select('*').from('daily_diet').where('id', foodID)
        return foodData
    }

    async getWaterByDate(date: string, user_id: number) {
        // console.log('date', date)
        let nextDay = new Date(date).getDate() + 1
        var nextDate = new Date()
        nextDate.setDate(nextDay)
        let endDate = nextDate.toISOString().split('T')[0]
        let water = await this.knex.select('*').from('daily_water')
            .where({ user_id })
            .where('created_at', '>=', date)
            .where('created_at', '<', endDate)
        return water
    }

    async getExerciseRecord(date:string,user_id:number) {
        // console.log('quickAddFoodID',foodID)
        let nextDay=new Date(date).getDate()+1
        var nextDate=new Date()
        nextDate.setDate(nextDay)
        let endDate=nextDate.toISOString().split('T')[0]
        let exercise = await this.knex.select('*').from('daily_exercise_record')
        .where({user_id})
        .where('created_at','>=',date)
        .where('created_at','<',endDate)
        return exercise
    }
    



    async addDiet(dietItem: DailyDiet, userId: number) {
        await this.knex.insert([
            {
                user_id: userId,
                food_name: dietItem.food_name,
                diet_type: dietItem.diet_type,
                food_type: dietItem.food_type,
                calories: dietItem.calories,
                protein: dietItem.protein,
                carbohydrate: dietItem.carbohydrate,
                total_fat: dietItem.total_fat,
                sugar: dietItem.sugar,
                saturated_fat: dietItem.saturated_fat,
                trans_fat: dietItem.trans_fat,
                cholesterol: dietItem.cholesterol,
                dietary: dietItem.dietary_fiber,
                sodium: dietItem.sodium,
                food_weight: dietItem.food_weight,
            }
        ]).into('daily_diet')
    }

    async quickAddDiet(quickDietItem:QuickAddDiet,userId:number){
        // console.log({userId})
        await this.knex.insert([
            {
                user_id: userId,
                food_name: quickDietItem.food_name,
                diet_type: quickDietItem.diet_type,
                calories: quickDietItem.calories,
                protein: quickDietItem.protein,
                carbohydrate: quickDietItem.carbohydrate,
                fat: quickDietItem.fat
            }
        ]).into('quick_add_diet')
    }

    async addExercise(exerciseList: ExercisesData, userId: number) {


        await this.knex.insert([
            {
                user_id: userId,
                exercise_type: exerciseList.exercise_type,
                exercise_mins: exerciseList.exercise_mins,
                exercise_calories: exerciseList.exercise_calories,
            }
        ]).into('daily_exercise_record')
    }

    async checkWater(cupOfWater: number, userId: number, date: string) {
        let nextDay = new Date(date).getDate() + 1
        var nextDate = new Date()
        nextDate.setDate(nextDay)
        let endDate = nextDate.toISOString().split('T')[0]
        let row = await this.knex.select('*').from('daily_water')
            .where('user_id', userId)
            .where('created_at', '>=', date)
            .where('created_at', '<', endDate)
            .first()
        // let id=row[0].id

        if (!row) {
            await this.knex.insert([
                {
                    user_id: userId,
                    cup_of_water: cupOfWater,
                }
            ]).into('daily_water')
        }
        else {
            let id = row.id;
            await this.knex('daily_water').update(
                {
                    user_id: userId,
                    cup_of_water: cupOfWater,
                }
            ).where({ id })
        }

    }



    async updateQuickDietList(id: number, dietList: QuickAddDietPatchItem) {
        // console.log({dietList})
        await this.knex('quick_add_diet').update(
            {
                calories: dietList.calories,
                protein: dietList.protein,
                carbohydrate: dietList.carbohydrate,
                fat: dietList.fat
            }).where({ id })
    }

    async updateRecentDietList(id: number, dietList: RecentFoodList) {
        // console.log({dietList})
        await this.knex('daily_diet').update(
            {
                calories: dietList.calories,
                protein: dietList.protein,
                carbohydrate: dietList.carbohydrate,
                total_fat: dietList.total_fat,
                sugar: dietList.sugar,
                saturated_fat: dietList.saturated_fat,
                trans_fat: dietList.trans_fat,
                cholesterol: dietList.cholesterol,
                dietary: dietList.dietary,
                sodium: dietList.sodium,
                food_weight: dietList.food_weight
            }).where({ id })
    }

    async deleteRecentItem(id: number) {
        await this.knex('daily_diet').where({ id }).del()
    }

    async deleteQuickItem(id: number) {
        await this.knex('quick_add_diet').where({ id }).del()
    }

    async deleteExerciseItem(id:number){
        await this.knex('daily_exercise_record').where({id}).del()
    }
}




// async function main() {
//     let foodData = new FoodDataService(knex)
//     let data = await foodData.getFoodData();
// // }
// if (process.argv[1] === __filename) {
//     main().catch(e => console.error(e))
//         .finally(() => knex.destroy())
// }
