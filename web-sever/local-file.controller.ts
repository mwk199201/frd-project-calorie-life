import formidable from 'formidable'
import { access,  mkdirSync, unlink } from 'fs'
import { RestController } from './rest.controller'
import express from 'express'
import { FileController, FileUploadOptions, FileUploadResult } from './file.controller'
import { HttpError } from './http.error'
import { join } from 'path'


const uploadDir = 'uploads'
mkdirSync(uploadDir, { recursive: true })


export class LocalFileController extends RestController
    implements FileController {
    counter = 0

    constructor() {
        super()
        this.router.use('/uploads', express.static(uploadDir))
    }
    delete = (filename: string) => {
        let file = join(uploadDir, filename)
        access(file, error => {
            if (error) {
                console.error('failed to check file access right:', file)
                return
            }
            unlink(file, error => {
                if (error) {
                    console.error('failed to delete file:', file)
                }
            })
        })
    }
    // upload = async (req: Request, res: Response) => {

    //     form.parse(req, (err, fields, files) => {
    //         console.log({ err, fields, files });
    //         if (err) {
    //             return;
    //         }

    //     })
    // }

    upload = (options: FileUploadOptions): Promise<FileUploadResult> => {
        let form = new formidable.Formidable({
            uploadDir,
            multiples:true,
            filter: part => {
              return!!  options.fields.find(field=>part.name==field.name && part.mimetype?.startsWith(field.mimeTypePrefix) )
            },
            filename: (name, ext, part, form) => {
                let field = part.name
                let timestamp = Date.now()
                this.counter++
                let extname = part.mimetype?.split('/').pop() || 'bin'
                let filename = `${field}-${timestamp}-${this.counter}.${extname}`
                return filename

            },
        })
        return new Promise<FileUploadResult>((resolve, reject) => {
            form.parse(options.req, (err, fields, files) => {
                if (err) {
                    reject(
                        new HttpError(400, 'Failed to parse form data. ' + String(err)),
                    )
                    return
                }
                // console.log(fields, files);

                resolve({ fields, files })
            })
        })

    }



}