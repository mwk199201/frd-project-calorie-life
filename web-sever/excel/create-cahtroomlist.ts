import knex, { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("profile").del();
  await knex("user_read_message").del();
  await knex("chatroom_record").del();
  await knex("room").del();
  await knex("user").del();

  // const img = (i: number) => `https://picsum.photos/id/${i}/128/128`

  await knex("user").insert([
    {
      id: 1,
      username: "peter",
      hash_password: 12345,
      email: "ff1@gamil.com",
      phone_number: 23456781,
      admin: false,
      payment: false,
    },
    {
      id: 2,
      username: "mary",
      hash_password: 12349,
      email: "ff2@gamil.com",
      phone_number: 12345678,
      admin: false,
      payment: false,
    },
    {
      id: 3,
      username: "ken",
      hash_password: 123400,
      email: "f3f@gamil.com",
      phone_number: 12345670,
      admin: false,
      payment: false,
    },
    {
      id: 4,
      username: "ben",
      hash_password: 123443546,
      email: "ff4@gamil.com",
      phone_number: 12345679,
      admin: false,
      payment: false,
    },
    {
      id: 5,
      username: "luke",
      hash_password: 123446787,
      email: "ff5@gamil.com",
      phone_number: 98765432,
      admin: false,
      payment: false,
    },
  ]);

  await knex("room").insert([
    { id: 1, user1_id: 1, user2_id: 2 },
    { id: 2, user1_id: 1, user2_id: 3 },
    { id: 3, user1_id: 1, user2_id: 4 },
    { id: 4, user1_id: 1, user2_id: 5 },
    { id: 5, user1_id: 2, user2_id: 5 },
  ]);

  await knex("chatroom_record").insert([
    {
      id: 1,
      sender_id: 1,
      room_id: 1,
      message: "demo message1",
      image: null,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString(),
    },
    {
      id: 2,
      sender_id: 4,
      room_id: 2,
      message: "demo message2",
      image: null,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString(),
    },
    {
      id: 3,
      sender_id: 5,
      room_id: 3,
      message: "demo message3",
      image: null,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString(),
    },
    {
      id: 4,
      sender_id: 2,
      room_id: 1,
      message: "reply",
      image: null,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString(),
    },
  ]);

  await knex("user_read_message").insert([
    {
      id: 1,
      user_id: 1,
      room_id: 1,
      last_message_id: 1,
    },
    {
      id: 2,
      user_id: 1,
      room_id: 2,
      last_message_id: 2,
    },
  ]);

  await knex("profile").insert([
    {
      id: 1,
      user_id: 1,
      nickname: "man",
      height: 197,
      weight: 59,
      age: 35,
      icon_image: "123.png",
      aim: "keep fit",
      user_role: "user",
    },
    {
      id: 2,
      user_id: 2,
      nickname: "fd",
      height: 170,
      weight: 55,
      age: 20,
      icon_image: "234.png",
      aim: "keep fit",
      user_role: "user",
    },
    {
      id: 3,
      user_id: 3,
      nickname: "hfbdai",
      height: 115,
      weight: 60,
      age: 21,
      icon_image: "482.png",
      aim: "keep fit",
      user_role: "user",
    },
    {
      id: 4,
      user_id: 4,
      nickname: "havfc",
      height: 115,
      weight: 60,
      age: 21,
      icon_image: "482.png",
      aim: "keep fit",
      user_role: "user",
    },
    {
      id: 5,
      user_id: 5,
      nickname: "374679",
      height: 115,
      weight: 60,
      age: 21,
      icon_image: "482.png",
      aim: "keep fit",
      user_role: "user",
    },
  ]);

  await knex.raw(`ALTER SEQUENCE chatroom_record_id_seq RESTART WITH 5;`);
}
