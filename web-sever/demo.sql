CREATE USER calorielife WITH PASSWORD 'calorielife' SUPERUSER;

insert into "user" (username, hash_password, email, phone_number, admin, payment)
values ('mok','1234',null,null,'FALSE','FALSE');

insert into "user" (username, hash_password, email, phone_number, admin, payment)
values ('leung','1234',null,null,'FALSE','FALSE');
insert into "user" (username, hash_password, email, phone_number, admin, payment)
values ('may','1234',null,null,'FALSE','FALSE');
insert into "user" (username, hash_password, email, phone_number, admin, payment)
values ('beeno','1234',null,null,'TRUE','TRUE');


TRUNCATE TABLE 
"user",
"profile", 
comment_record,blog,
daily_food_record,
daily_exercise_record, 
blog_like,
daily_diet,
chatroom_record,
follow_list,
user_read_message,
quick_add_diet,
room,
blog_save,
blog_block,
blog_report
RESTART IDENTITY;



select 

username,
from blog join "user" on blog.poster_id = "user".id
ORDER BY blog.updated_at DESC;


select
  blog.id,
blog.content,
blog.updated_at,
profile.nickname,
blog.image
, count(*) as like_count
from blog
left join blog_like on blog_like.blog_id = blog.id
left join profile on blog.poster_id = profile.id
group by blog.id, profile.nickname
ORDER BY blog.updated_at DESC;


select blog.id, "user".username,json_agg(())

select
  blog.id
, count(*) as like_count
from blog
left join blog_like on blog_like.blog_id = blog.id
group by blog.id

select
blog_id,
  profile.nickname,
  profile.icon_image,
  comment_record.content,
  comment_record.updated_at
from comment_record
left join profile on profile.user_id = comment_record.commenter_id
where blog_id = 1
ORDER BY comment_record.updated_at DESC;

      select
            blog_id,
            profile.nickname,
            profile.icon_image,
            comment_record.content,
            comment_record.updated_at
            from comment_record
            left join profile on profile.user_id = comment_record.commenter_id
            where blog_id = 1
        ORDER BY comment_record.updated_at DESC;


          select
            comment_record.id,
            profile.nickname,
            profile.icon_image,
            comment_record.content,
            comment_record.updated_at
            from comment_record
            left join profile on profile.user_id = comment_record.commenter_id
            where blog_id = 6
        ORDER BY comment_record.updated_at ASC
        lIMIT 5 OFFSET 0
        


insert into "chatroom_record" (id,sender_id, receiver_id, message, created_at,updated_at)
values (1,1,2,'testing,testing',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

select 
       room.id,
       receiver_id,
       message,
       chatroom_record.created_at,
      profile.nickname,
      profile.icon_image,
      profile.user_role
      from chatroom_record
      left join profile on chatroom_record.receiver_id = profile.id
      full outer join room on chatroom_record.id = room.id
      group by chatroom_record.id,profile.nickname,profile.icon_image,profile.user_role,room.id
      ORDER BY chatroom_record.updated_at DESC;

      select 
      receiver_id,
      message,
      chatroom_record.created_at,
      profile.nickname,
      profile.icon_image,
      profile.user_role
      from chatroom_record
      left join profile on chatroom_record.receiver_id = profile.id
      group by chatroom_record.id,profile.nickname,profile.icon_image,profile.user_role,room.id
      ORDER BY chatroom_record.updated_at DESC;