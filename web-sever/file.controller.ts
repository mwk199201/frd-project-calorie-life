import aws from 'aws-sdk'
import formidable from 'formidable'
import { Request } from 'express'

export interface FileController {
  upload(options: FileUploadOptions): Promise<FileUploadResult>

  delete(filename: string): void
}

export interface FileUploadOptions {
  req: Request
  fields: { name: string, mimeTypePrefix: string }[]
  // mimeTypePrefix: string[]
}

export interface FileUploadResult {
  fields: Record<string, string[] | string>
  files: Record<string, formidable.File[] | formidable.File>
  s3Files?: aws.S3.ManagedUpload.SendData[]
}
