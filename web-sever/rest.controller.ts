import { Request, Response } from 'express'
import { HttpError } from './http.error'
import express from 'express'

// response to front end

export class RestController {
  router = express.Router()

  wrapControllerMethod(fn: (req: Request) => object | Promise<object>): express.RequestHandler {
    return async (req: Request, res: Response) => {
      try {
        let json = await fn(req)
        res.json(json)
      } catch (error) {
        if (error instanceof HttpError) {
          res.status(error.status).json({ error: error.message })
        } else {
          res.status(500).json({ error: String(error) })
        }
      }
    }
  }
}
