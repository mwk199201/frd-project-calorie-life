import express, { request } from "express";
import { Socket } from "socket.io";
import { ChatItemService } from "./ChatItem.service";
import { FileController } from "./file.controller";
import { getJWTPayload } from "./guards";
import { RestController } from "./rest.controller";
import { Server as socketio } from 'socket.io'


export class ChatItemController extends RestController {
  constructor(
    private chatitemService: ChatItemService,
    private fileController: FileController,
    private io: socketio,
  ) {
    super();
    this.router.get("/chatrooms", this.wrapControllerMethod(this.listChatItem));
    this.router.post(
      "/chatrooms/user/:user_id",
      this.wrapControllerMethod(this.getRoomId)
    );
    this.router.get(
      "/chatrooms/:room_id",
      this.wrapControllerMethod(this.getRoomMessages)
    );
    this.router.post(
      "/chatrooms/:room_id/message",
      this.wrapControllerMethod(this.sendChatItem)
    );
    this.router.delete(
      "/chatrooms/:room_id/:message_id",
      this.wrapControllerMethod(this.deleteChatItem)
    );
  }

  listChatItem = (req: express.Request) => {
    let payload = getJWTPayload(req);
    let user_id = payload.id;
    return this.chatitemService.listChatItem({ user_id });
  };

  getRoomId = async (req: express.Request) => {
    let payload = getJWTPayload(req);
    let user1_id = payload.id;
    let user2_id = +req.params.user_id;
    let id = await this.chatitemService.getRoomId({ user1_id, user2_id });
    // if(user1_id === user2_id){
    //   return []
    // }
    console.log(id,'is otheruser_id', user1_id ,'isuser',user2_id ,'is otheruser')
    return { ok: true, id };
  };

  getRoomMessages = async (req: express.Request) => {
    let payload = getJWTPayload(req);
    let user_id = payload.id;
    let room_id = +req.params.room_id;
    return await this.chatitemService.getRoomMessages({ user_id, room_id });
  };

  sendChatItem = async (req: express.Request) => {
    let payload = getJWTPayload(req);
    let room_id = +req.params.room_id;
    let user_id = payload.id;
    let input = await this.fileController.upload({
      req,
      fields: [
        { name: "image", mimeTypePrefix: "image/" },
      ],
    });
    // console.log(input);
    let content = Array.isArray(input.fields.content)
      ? input.fields.content
      : [input.fields.content];
    let image = Array.isArray(input.files.image)
      ? input.files.image
      : [input.files.image];
    const messages =
      await this.chatitemService.sendChatItem({ user_id, room_id, content, image });
    if (messages) {
      this.io.to(String(room_id)).emit('messages', messages)
    }
    return { ok: true }
  };


  deleteChatItem = async (req: express.Request) => {
    let payload = getJWTPayload(req);
    let user_id = payload.id;
    let room_id = +req.params.room_id;
    let message_id = +req.params.message_id;
    await this.chatitemService.deleteChatItem({
      user_id,
      room_id,
      message_id,
    });

    return { ok: true }

  };
}
