import { Request } from "express";
// import { BlogService } from "./blog.service";
import { FileController } from "./file.controller";
import { getBodyNumber, getBodyString, getJWTPayload } from "./guards";
// import { HttpError } from "./http.error";
import { RestController } from "./rest.controller";
// import { UpdateAvatarResult } from "./api.types";
import { ProfileService } from "./profile.service";


export class ProfileController extends RestController {
    constructor(private profileService: ProfileService,
        private fileController: FileController

    ) {
        super()

        this.router.get('/blog/saved', this.wrapControllerMethod(this.getSavedBlog))
        this.router.get('/blog/history', this.wrapControllerMethod(this.getBlogHistory))
        this.router.get('/blog/blocked', this.wrapControllerMethod(this.getBlockedBlog))
        this.router.post('/profile', this.wrapControllerMethod(this.postProfile))
        this.router.get('/profile', this.wrapControllerMethod(this.getProfile))
        this.router.post('/profile/iconImage', this.wrapControllerMethod(this.uploadIconImage))

    }


    getProfile = async (req: Request) => {
        let payload = getJWTPayload(req)
        let profile = await this.profileService.getProfile(payload.id)
        return profile
    }


    getSavedBlog = async (req: Request) => {
        let payload = getJWTPayload(req)
        let savedBlogs = await this.profileService.getSavedBlog(payload.id)
        return savedBlogs
    }



    getBlogHistory = async (req: Request) => {
        let payload = getJWTPayload(req)
        let blogHistory = await this.profileService.getBlogHistory(payload.id)
        return blogHistory
    }


    getBlockedBlog = async (req: Request) => {
        let payload = getJWTPayload(req)
        let blockedBlogs = await this.profileService.getBlockedBlog(payload.id)
        return blockedBlogs
    }

    postProfile = async (req: Request) => {
        let payload = getJWTPayload(req)
        let height = getBodyNumber(req, 'height')
        let weight = getBodyNumber(req, 'weight')
        let gender = getBodyString(req, 'gender')
        let age = getBodyNumber(req, 'age')
        let nickname = getBodyString(req, 'nickname')
        let activity = req.body.activity
        let aim = req.body.aim
        console.log(payload.id, nickname, height, weight, gender, age, activity, aim);

        return await this.profileService.postProfile(payload.id, nickname, height, weight, gender, age, activity, aim)
    }

    uploadIconImage = async (req: Request) => {
        let payload = getJWTPayload(req)
        let user_id = payload.id
        let input = await this.fileController.upload({
            req,
            fields: [{ name: "image", mimeTypePrefix: 'image/' }],
        })
        let image = input.files.image
        let file = Array.isArray(image) ? image[0] : image
        let filename = file?.newFilename
        // console.log("input", filename);

        return await this.profileService.uploadIconImage(user_id, filename)

    }
}
