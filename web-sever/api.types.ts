export type BaseResponse = {
  error?: string
}

export type LoginResponse = BaseResponse & {
  token: string
}

export type JWTPayload = {
  id: number
}

export type BlogItem = {
  id: number
  username: string
  icon: string
  content: string
  image: string | null
  like_count: number
  create_at: string
  commentList?: CommentItem[] | null
}
export type CommentItem = {
  id: number
  commenter_name: string
  commenter_icon: string
  commenter_content: string
  comment_time: string
}


export type ChatItem = {
  room_id: number,
  receiver_id: number,
  created_at: string,
  nickname: string,
  icon_image: string,
  user_role: string,
}

export type MessageInput = {
  room_id: number
  message?: string | string[]
  filename?: string
}

export type MessageNotice = {
  message_id: number
  room_id: number
  sender_id: number
  message?: string | string[]
  send_time: string
}

export type UpdateAvatarResult = {
  error?: string
  avatar_image: string
}

