
import { Knex } from 'knex'
import { comparePassword, hashPassword } from './hash';
import { HttpError } from './http.error';
import { JWTPayload } from './api.types';
import jwtSimple from 'jwt-simple'
import { env } from './env';
import { LoginResponse } from './api.types';
import { knex } from './db'

export class UserService {
    constructor(private knex: Knex) { }

    async getTokenWithPassword(input: {
        username: string
        password: string
    }): Promise<LoginResponse> {
        let user = await this.knex
            .select('id', 'hash_password')
            .from('user')
            .where({ username: input.username })
            .first()
        if (!user) {
            throw new HttpError(404, `user not found, wrong username?`)
        }
        let isPasswordMatched =
            await comparePassword({
                password: input.password,
                hash_password: user.hash_password,
            })
        if (!isPasswordMatched) {
            throw new HttpError(401, `wrong username or password?`)
        }
        return this.createToken(user.id)
    }

    createToken(id: number) {
        let payload: JWTPayload = { id }
        let token = jwtSimple.encode(payload, env.JWT_SECRET)
        return { token }
    }

    async registerWithPassword(input: {
        username: string
        password: string
    }) {
        let user = await this.knex
            .select('username')
            .from('user')
            .where({ username: input.username })
            .first()
        if (user) {

            throw new HttpError(404, `user already exist?`)
        }
        let hash_password = await hashPassword(input.password)

        let register = await this.knex
            .insert([
                {
                    username: input.username,
                    hash_password: hash_password,
                    admin: false,
                    payment: false
                }
            ]).into('user')
        console.log("register", register);
        return { register: true }

    }

    async getUsername(user_id: number) {
        // console.log(user_id);

        let username = await this.knex.select('username')
            .from('user')
            .where({ id: user_id })
            .first()
        // console.log('username', username)
        return username
    }

}



async function main() {
    let userService = new UserService(knex)
    let json = await userService.getTokenWithPassword({
        username: 'demo_user_1',
        password: 'demo_password',
    })
    // console.log(json)
}
if (process.argv[1] === __filename) {
    main()
        .catch(e => console.error(e))
        .finally(() => knex.destroy())
}

