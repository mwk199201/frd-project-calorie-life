import { config } from 'dotenv'
import { existsSync } from 'fs'
import populateEnv from 'populate-env'

// console.log('NODE_ENV:', process.env.NODE_ENV)


// config({ path: '.env.' + mode })
config({path:existsSync('.env')?'.env':'../.env'})

export let env = {
  DB_NAME: "",
  DB_USER: "",
  DB_PASSWORD: "",
  PORT: 8100,
  DB_HOST: "localhost",
  SESSION_SECRET: "",
  NODE_ENV: 'development',
  REACT_APP_ORIGIN:'',
  JWT_SECRET: '',
}

if (process.env.NODE_ENV === 'test'){
  env.DB_HOST = process.env.POSTGRES_HOST || ""
  env.DB_NAME = process.env.POSTGRES_NAME || ""
  env.DB_USER = process.env.POSTGRES_USER || ""
  env.DB_PASSWORD = process.env.POSTGRES_PASSWORD || ""
  env.SESSION_SECRET = process.env.SESSION_SECRET || ""
  env.REACT_APP_ORIGIN = process.env.REACT_APP_ORIGIN || ""
  env.JWT_SECRET = process.env.JWT_SECRET || ""
} 

// if(process.env.NODE_ENV === 'test') {
//   env.DB_HOST = process.env.POSTGRES_HOST
//   env.DB_NAME = process.env.POSTGRES_DB
//   env.DB_USER = process.env.POSTGRES_USER
//   env.DB_PASSWORD = process.env.POSTGRES_PASSWORD
// }

populateEnv(env, { mode: 'halt' })

// console.log('env:', env)
