"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatItemController = void 0;
const guards_1 = require("./guards");
const rest_controller_1 = require("./rest.controller");
class ChatItemController extends rest_controller_1.RestController {
    constructor(chatitemService, fileController, io) {
        super();
        this.chatitemService = chatitemService;
        this.fileController = fileController;
        this.io = io;
        this.listChatItem = (req) => {
            let payload = (0, guards_1.getJWTPayload)(req);
            let user_id = payload.id;
            return this.chatitemService.listChatItem({ user_id });
        };
        this.getRoomId = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let user1_id = payload.id;
            let user2_id = +req.params.user_id;
            let id = yield this.chatitemService.getRoomId({ user1_id, user2_id });
            return { ok: true, id };
        });
        this.getRoomMessages = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let user_id = payload.id;
            let room_id = +req.params.room_id;
            return yield this.chatitemService.getRoomMessages({ user_id, room_id });
        });
        this.sendChatItem = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let room_id = +req.params.room_id;
            let user_id = payload.id;
            let input = yield this.fileController.upload({
                req,
                fields: [
                    { name: "image", mimeTypePrefix: "image/" },
                ],
            });
            // console.log(input);
            let content = Array.isArray(input.fields.content)
                ? input.fields.content
                : [input.fields.content];
            let image = Array.isArray(input.files.image)
                ? input.files.image
                : [input.files.image];
            const messages = yield this.chatitemService.sendChatItem({ user_id, room_id, content, image });
            if (messages) {
                this.io.to(String(room_id)).emit('messages', messages);
            }
            return { ok: true };
        });
        this.deleteChatItem = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let user_id = payload.id;
            let room_id = +req.params.room_id;
            let message_id = +req.params.message_id;
            yield this.chatitemService.deleteChatItem({
                user_id,
                room_id,
                message_id,
            });
            return { ok: true };
        });
        this.router.get("/chatrooms", this.wrapControllerMethod(this.listChatItem));
        this.router.post("/chatrooms/user/:user_id", this.wrapControllerMethod(this.getRoomId));
        this.router.get("/chatrooms/:room_id", this.wrapControllerMethod(this.getRoomMessages));
        this.router.post("/chatrooms/:room_id/message", this.wrapControllerMethod(this.sendChatItem));
        this.router.delete("/chatrooms/:room_id/:message_id", this.wrapControllerMethod(this.deleteChatItem));
    }
}
exports.ChatItemController = ChatItemController;
