"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
const path_1 = __importDefault(require("path"));
const xlsx_1 = __importDefault(require("xlsx"));
const hash_1 = require("../hash");
let calories_path = path_1.default.resolve(__dirname, "../../../excel/calorieslife.xlsx");
let workbook = xlsx_1.default.readFile(calories_path);
let users = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.user, { raw: true });
let profiles = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.profile, { raw: true });
let blogs = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.blog, { raw: true });
let comment_records = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.comment_record, { raw: true });
let likes = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.like, { raw: true });
let rooms = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.room, { raw: true });
let chatroom_records = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.charoom_record, { raw: true });
let user_read_messages = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.user_read_message, { raw: true });
function seed(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        let id;
        let user_id_dict = {};
        let blog_key;
        let blog_id_dict = {};
        let last_message_id;
        let last_message_id_dict = {};
        for (let user of users) {
            // console.log(user);
            let row = yield knex
                .select('id')
                .from('user')
                .where({ username: user.username })
                .first();
            if (!row) {
                let rows = yield knex
                    .insert({
                    username: user.username,
                    hash_password: yield (0, hash_1.hashPassword)(user.password.toString()),
                    email: user.email,
                    phone_number: user.phone_number,
                    admin: user.admin,
                    payment: user.payment
                }).into('user')
                    .returning('id');
                id = rows[0].id;
            }
            else {
                id = row.id;
            }
            user_id_dict[user.username] = id;
        }
        let user_list = Object.keys(user_id_dict);
        // console.log(user_list);
        for (let profile of profiles) {
            // console.log(profile);
            let user_id = user_id_dict[profile.username];
            let nickname = profile.name;
            let height = profile.height;
            let weight = profile.weight;
            let age = profile.age;
            let icon_image = profile.icon_image;
            let aim = profile.aim;
            let role = profile.user_role;
            let activity = profile.activity;
            let gender = profile.gender;
            let row = yield knex
                .select('id')
                .from('profile')
                .where({ user_id: user_id })
                .first();
            if (!row) {
                let row = yield knex
                    .insert({
                    user_id: user_id,
                    nickname: nickname,
                    height: height,
                    weight: weight,
                    age: age,
                    icon_image: icon_image,
                    aim: aim,
                    user_role: role,
                    activity: activity,
                    gender: gender
                }).into('profile');
            }
        }
        for (let blog of blogs) {
            // console.log(blog);
            let user_id = user_id_dict[blog.username];
            let row = yield knex
                .select('id')
                .from('blog')
                .where({ content: blog.content })
                .first();
            if (!row) {
                let rows = yield knex
                    .insert({
                    poster_id: user_id,
                    content: blog.content,
                    image: blog.image,
                }).into('blog')
                    .returning('id');
                id = rows[0].id;
            }
            //  else {
            //     id = row.id
            // }
            // blog_id_dict[blog.blog_key] = id
        }
        // console.log('blog_id_dict', blog_id_dict);
        for (let comment_record of comment_records) {
            // console.log(comment_record);
            let user_id = user_id_dict[comment_record.commenter];
            let row = yield knex
                .select('id')
                .from('comment_record')
                .where({ content: comment_record.content })
                .first();
            if (!row) {
                let rows = yield knex
                    .insert({
                    commenter_id: user_id,
                    blog_id: comment_record.blog_key,
                    content: comment_record.content
                }).into('comment_record');
            }
        }
        for (let like of likes) {
            let user_id = user_id_dict[like.username];
            let row = yield knex
                .select('id')
                .from('blog_like')
                .where({ user_id: user_id })
                .andWhere({ blog_id: like.blog_key })
                .first();
            if (!row) {
                let rows = yield knex
                    .insert({
                    user_id: user_id,
                    blog_id: like.blog_key
                }).into('blog_like');
            }
        }
        for (let room of rooms) {
            let user1_id = user_id_dict[room.user1_name];
            let user2_id = user_id_dict[room.user2_name];
            let row = yield knex
                .select('id')
                .from('room')
                .where({ user1_id: user1_id })
                .andWhere({ user2_id: user2_id })
                .first();
            if (!row) {
                let rows = yield knex
                    .insert({
                    user1_id: user1_id,
                    user2_id: user2_id,
                }).into('room');
            }
        }
        for (let chatroom_record of chatroom_records) {
            let sender_id = user_id_dict[chatroom_record.sender_name];
            console.log("chatroom_record", chatroom_record);
            let row = yield knex
                .select('id')
                .from('chatroom_record')
                .where({ sender_id: sender_id })
                .first();
            if (!row) {
                let rows = yield knex
                    .insert({
                    sender_id: sender_id,
                    room_id: chatroom_record.room_id,
                    message: chatroom_record.message,
                    image: chatroom_record.image
                }).into('chatroom_record');
            }
        }
        for (let user_read_message of user_read_messages) {
            console.log(user_read_message);
            let user_id = user_id_dict[user_read_message.username];
            let row = yield knex
                .select('id')
                .from('user_read_message')
                .where({ user_id: user_id })
                .andWhere({ chatroom_record: user_read_message.room_id })
                .first();
            if (!row) {
                let rows = yield knex
                    .insert({
                    user_id: user_id,
                    chatroom_record: user_read_message.room_id,
                    message: user_read_message.last_message_id
                }).into('user_read_message');
            }
        }
    });
}
exports.seed = seed;
;
