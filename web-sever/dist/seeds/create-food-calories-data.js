"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
const path_1 = __importDefault(require("path"));
const xlsx_1 = __importDefault(require("xlsx"));
let food_path = path_1.default.resolve(__dirname, "../../../excel/food_nutrient.xlsx");
let workbook = xlsx_1.default.readFile(food_path);
let food_nutrient = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.food_nutrient, { raw: true });
let exercises = xlsx_1.default.utils.sheet_to_json(workbook.Sheets.exercise_calories, { raw: true });
// for (let exercise of exercises){
//     console.log(exercise)
// }
function seed(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        for (let nutrient of food_nutrient) {
            let row = yield knex
                .select('id')
                .from('food_calories_data')
                .where({ food_chi_name: nutrient.food_chi_name })
                .first();
            if (!row) {
                let rows = yield knex
                    .insert({
                    food_chi_name: nutrient.food_chi_name,
                    food_eng_name: nutrient.food_eng_name,
                    food_type: nutrient.food_type,
                    food_weight: nutrient.food_portion,
                    calories: nutrient.food_calories,
                    protein: nutrient.protein,
                    carbohydrate: nutrient.carbohydrate,
                    total_fat: nutrient.total_fat,
                    dietary_fibre: nutrient.dietary_fibre,
                    sugar: nutrient.sugars,
                    saturated_fat: nutrient.saturated_fat,
                    trans_fat: nutrient.trans_fat,
                    cholesterol: nutrient.cholesterol,
                    sodium: nutrient.sodium,
                    isselected: false
                }).into('food_calories_data');
            }
        }
        for (let exercise of exercises) {
            let row = yield knex
                .select('id')
                .from('exercise_calories_data')
                .where({ exercise_type: exercise.exercise_type })
                .first();
            if (!row) {
                let rows = yield knex
                    .insert({
                    exercise_type: exercise.exercise_type,
                    exercise_mins: exercise.exercise_mins,
                    exercise_METs: exercise.exercise_METs,
                    isselected: false
                }).into('exercise_calories_data');
            }
        }
    });
}
exports.seed = seed;
