"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatItemService = void 0;
class ChatItemService {
    constructor(knex, io) {
        this.knex = knex;
        this.io = io;
    }
    listChatItem(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield this.knex.raw(
            /* sql */ `
      with roomlist as (
        select
            room.id,
            (
                case
                    when user1_id = ? then user2_id
                    else user1_id
                end
            ) as other_user_id
        from
            room
        where
            user1_id = ?
            or user2_id = ?
    ),
    messagelist as (
        select
            max(chatroom_record.id) as message_id
        from
            chatroom_record
        where
            room_id in (
                select
                    roomlist.id
                from
                    roomlist
            )
        group by
            room_id
    )
    select
        chatroom_record.id as message_id,
        chatroom_record.created_at,
        roomlist.id as room_id,
        message,
        sender_id,
        other_user_id,
        nickname,
        icon_image,
        user_role
    from
        chatroom_record
        right outer join roomlist on roomlist.id = room_id
        inner join profile on profile.user_id = other_user_id
    where
        chatroom_record.id in (
            select
                message_id
            from
                messagelist
        ) OR chatroom_record.id is NULL
    ORDER BY
        chatroom_record.updated_at DESC;
      `, [filter.user_id, filter.user_id, filter.user_id]);
            // console.log(result.rows.length);
            let rooms = result["rows"];
            return rooms;
        });
    }
    getRoomId(input) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.knex.transaction((knex) => __awaiter(this, void 0, void 0, function* () {
                let user1_id = input.user1_id;
                let user2_id = input.user2_id;
                if (user1_id > user2_id) {
                    user1_id = input.user2_id;
                    user2_id = input.user1_id;
                }
                let row = yield knex
                    .select("id")
                    .from("room")
                    // .where("user1_id",user1_id) .andWhere('user2_id',user2_id)
                    .where({ user1_id, user2_id })
                    .first();
                if (row) {
                    return row.id;
                }
                // [row] = await knex("room").insert({ user1_id, user2_id }).returning("id");
                const room = yield knex("room")
                    .insert({ user1_id, user2_id })
                    .returning("id");
                return room[0].id;
            }));
        });
    }
    getOtherUserId(input, knex = this.knex) {
        return __awaiter(this, void 0, void 0, function* () {
            let room = yield knex
                .select("user1_id", "user2_id")
                .from("room")
                .where("room.id", input.room_id)
                .first();
            if (!room) {
                return false;
            }
            if (!(room.user1_id == input.user_id || room.user2_id == input.user_id)) {
                return false;
            }
            let other_user_id = room.user1_id == input.user_id ? room.user2_id : room.user1_id;
            return other_user_id;
        });
    }
    getRoomMessages(input) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.knex.transaction((knex) => __awaiter(this, void 0, void 0, function* () {
                let other_user_id = yield this.getOtherUserId(input, knex);
                let other_user = yield knex
                    .select("nickname", "user_id", "icon_image")
                    .from("profile")
                    .where("user_id", other_user_id)
                    .first();
                const messages = yield knex
                    .select("sender_id", "message", 'created_at')
                    .from("chatroom_record")
                    .where("room_id", input.room_id);
                return { other_user, messages };
            }));
        });
    }
    sendChatItem(input) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.knex.transaction((knex) => __awaiter(this, void 0, void 0, function* () {
                let other_user_id = yield this.getOtherUserId(input, knex);
                if (!other_user_id) {
                    return false;
                }
                let sender_id = input.user_id;
                const messages = input.content.map((_, i) => {
                    var _a;
                    return ({
                        room_id: input.room_id,
                        sender_id,
                        message: input.content[i],
                        image: ((_a = input.image[i]) === null || _a === void 0 ? void 0 : _a.newFilename) || null,
                    });
                });
                yield knex.insert(messages).into("chatroom_record");
                return messages;
            }));
        });
    }
    deleteChatItem(input) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(`theno is${input.user_id}& ${input.message_id}`);
            return this.knex.transaction((knex) => __awaiter(this, void 0, void 0, function* () {
                let other_user_id = yield this.getOtherUserId(input, knex);
                if (!other_user_id) {
                    return false;
                }
                yield knex('chatroom_record')
                    .where({ id: input.message_id, sender_id: input.user_id })
                    .del();
            }));
        });
    }
}
exports.ChatItemService = ChatItemService;
