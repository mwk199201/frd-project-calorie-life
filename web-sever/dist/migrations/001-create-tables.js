"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
function up(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!(yield knex.schema.hasTable('user'))) {
            yield knex.schema.createTable('user', table => {
                table.increments('id');
                table.string('username', 255).notNullable().unique();
                table.string('hash_password').nullable();
                table.string('email', 255).nullable();
                table.integer('phone_number').nullable();
                table.boolean('admin').notNullable();
                table.boolean('payment').notNullable();
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('profile'))) {
            yield knex.schema.createTable('profile', table => {
                table.increments('id');
                table.integer('user_id').unsigned().notNullable().references('user.id');
                table.string('name', 255).notNullable();
                table.integer('height').notNullable();
                table.integer('weight').notNullable();
                table.integer('age').notNullable();
                table.string('icon_image', 255).nullable();
                table.string('aim', 255).notNullable();
                // table.emun('admin','trainer','nutritionist','user')('role').notNullable()
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('daily_food_record'))) {
            yield knex.schema.createTable('daily_food_record', table => {
                table.increments('id');
                table.integer('user_id').unsigned().notNullable().references('user.id');
                table.integer('daily_calories_ingest').notNullable();
                table.integer('daily_calories_absorb').notNullable();
                table.integer('daily_exercises_ingest').notNullable();
                table.integer('water_absorb').notNullable();
                table.integer('recommend_diet_percentage').notNullable();
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('daily_diet'))) {
            yield knex.schema.createTable('daily_diet', table => {
                table.increments('id');
                table.integer('user_id').unsigned().notNullable().references('user.id');
                table.string('diet_type').notNullable();
                table.string('food_name', 255).notNullable();
                table.string('food_type', 255).notNullable();
                table.integer('calories').notNullable();
                table.float('protein').notNullable();
                table.float('carbohydrate').notNullable();
                table.float('total_fat').notNullable();
                table.float('sugar').notNullable();
                table.float('saturated_fat').notNullable();
                table.float('trans_fat').notNullable();
                table.float('cholesterol').notNullable();
                table.float('dietary').notNullable();
                table.float('sodium').notNullable();
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('daily_exercise_record'))) {
            yield knex.schema.createTable('daily_exercise_record', table => {
                table.increments('id');
                table.integer('user_id').unsigned().notNullable().references('user.id');
                table.string('exercise_type', 255).notNullable();
                table.integer('exercise_mins').notNullable();
                table.integer('exercise_calories').notNullable();
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('food_calories_data'))) {
            yield knex.schema.createTable('food_calories_data', table => {
                table.increments('id');
                table.string('food_chi_name', 255).notNullable();
                table.string('food_type', 255).notNullable();
                table.string('food_eng_name', 255).notNullable();
                table.integer('food_weight').notNullable();
                table.integer('calories').notNullable();
                table.float('protein').notNullable();
                table.float('carbohydrate').notNullable();
                table.float('total_fat').notNullable();
                table.float('sugar').notNullable();
                table.float('saturated_fat').notNullable();
                table.float('trans_fat').notNullable();
                table.float('cholesterol').notNullable();
                table.float('dietary_fibre').notNullable();
                table.float('sodium').notNullable();
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('exercise_calories_data'))) {
            yield knex.schema.createTable('exercise_calories_data', table => {
                table.increments('id');
                table.string('exercise_type', 255).notNullable();
                table.integer('exercise_mins').notNullable();
                table.float('exercise_METs').notNullable();
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('blog'))) {
            yield knex.schema.createTable('blog', table => {
                table.increments('id');
                table.integer('poster_id').unsigned().notNullable().references('user.id');
                table.text('content').notNullable();
                table.string('image', 255).nullable();
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('comment_record'))) {
            yield knex.schema.createTable('comment_record', table => {
                table.increments('id');
                table.integer('commenter_id').unsigned().notNullable().references('user.id');
                table.integer('blog_id').unsigned().notNullable().references('blog.id');
                table.string('content', 255).notNullable();
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('like'))) {
            yield knex.schema.createTable('like', table => {
                table.increments('id');
                table.integer('user_id').unsigned().notNullable().references('user.id');
                table.integer('blog_id').unsigned().notNullable().references('blog.id');
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('chatroom_record'))) {
            yield knex.schema.createTable('chatroom_record', table => {
                table.increments('id');
                table.integer('sender_id').unsigned().notNullable().references('user.id');
                table.integer('receiver_id').unsigned().notNullable().references('user.id');
                table.string('message', 255).notNullable();
                table.timestamps(false, true);
            });
        }
    });
}
exports.up = up;
function down(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield knex.schema.dropTableIfExists('chatroom_record');
            yield knex.schema.dropTableIfExists('like');
            yield knex.schema.dropTableIfExists('comment_record');
            yield knex.schema.dropTableIfExists('blog');
            yield knex.schema.dropTableIfExists('exercise_calories_data');
            yield knex.schema.dropTableIfExists('food_calories_data');
            yield knex.schema.dropTableIfExists('daily_exercise_record');
            yield knex.schema.dropTableIfExists('daily_diet');
            yield knex.schema.dropTableIfExists('daily_food_record');
            yield knex.schema.dropTableIfExists('profile');
            yield knex.schema.dropTableIfExists('user');
        }
        catch (_a) {
        }
    });
}
exports.down = down;
