"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
function up(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        yield knex.from('user_read_message ').delete();
        yield knex.from('chatroom_record').delete();
        yield knex.from('room').delete();
        yield knex.schema.alterTable('room', table => {
            table.integer('user1_id').unsigned().notNullable().references('user.id');
            table.integer('user2_id').unsigned().notNullable().references('user.id');
        });
        yield knex.schema.alterTable('chatroom_record', table => {
            table.integer('room_id').unsigned().notNullable().references('room.id');
            table.dropColumn('receiver_id');
        });
    });
}
exports.up = up;
function down(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        yield knex.schema.alterTable('chatroom_record', table => {
            table.integer('receiver_id').unsigned().notNullable().references('user.id');
            table.dropColumn('room_id');
        });
        yield knex.schema.alterTable('room', table => {
            table.dropColumn('user2_id');
            table.dropColumn('user1_id');
        });
    });
}
exports.down = down;
