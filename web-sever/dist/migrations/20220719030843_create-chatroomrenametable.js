"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
function up(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        `
     ALTER TABLE user_read_message RENAME COLUMN chatroom_record TO last_message_id;
     CREATE TYPE room_id AS foreign key ('room.id');
     ALTER TABLE profile ADD COLUMN room_id room_id NOT NULL;
    `;
    });
}
exports.up = up;
function down(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        `
    ALTER TABLE "user_read_message" RENAME COLUMN  last_message_id  TO chatroom_record;
    ALTER TABLE user_read_message DROP COLUMN room_id; DROP TYPE room_id;
   `;
    });
}
exports.down = down;
