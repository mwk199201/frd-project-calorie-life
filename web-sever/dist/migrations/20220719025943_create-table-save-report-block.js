"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
function up(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!(yield knex.schema.hasTable('blog_save'))) {
            yield knex.schema.createTable('blog_save', table => {
                table.increments('id');
                table.integer('user_id').unsigned().notNullable().references('user.id');
                table.integer('blog_id').unsigned().notNullable().references('blog.id');
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('blog_block'))) {
            yield knex.schema.createTable('blog_block', table => {
                table.increments('id');
                table.integer('user_id').unsigned().notNullable().references('user.id');
                table.integer('blog_id').unsigned().notNullable().references('blog.id');
                table.timestamps(false, true);
            });
        }
        if (!(yield knex.schema.hasTable('blog_report'))) {
            yield knex.schema.createTable('blog_report', table => {
                table.increments('id');
                table.integer('user_id').unsigned().notNullable().references('user.id');
                table.integer('blog_id').unsigned().notNullable().references('blog.id');
                table.string('statement', 255).notNullable();
                table.timestamps(false, true);
            });
        }
    });
}
exports.up = up;
function down(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        yield knex.schema.dropTableIfExists('blog_report');
        yield knex.schema.dropTableIfExists('blog_save');
        yield knex.schema.dropTableIfExists('blog_block');
    });
}
exports.down = down;
