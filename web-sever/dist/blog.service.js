"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogService = void 0;
class BlogService {
    constructor(knex) {
        this.knex = knex;
    }
    getBlogList(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield this.knex.raw(/* sql */ `
    select
      blog.id,
      blog.poster_id,
      blog.content,
      blog.updated_at,
      profile.nickname,
      profile.icon_image,
      blog.title,
      blog.image,
      (select count(*)from blog_like
        where blog_like.blog_id = blog.id
      )as like_count,
      (select count(*) 
       from blog_like 
       where blog_like.blog_id = blog.id 
         and blog_like.user_id = ?
      ) as has_like,
      (select count(*) 
       from blog_save 
       where blog_save.blog_id = blog.id 
         and blog_save.user_id = ?
      ) as has_save,
      (select count(*) 
       from blog_block 
       where blog_block.blog_id = blog.id 
         and blog_block.user_id = ?
      ) as has_block,
    (select count(*) 
     from blog_report 
     where blog_report.blog_id = blog.id 
       and blog_report.user_id = ?
    ) as has_report
    from blog
    left join blog_like on blog_like.blog_id = blog.id
    left join profile on blog.poster_id = profile.user_id
    left join blog_save on blog_save.blog_id = blog.id
    left join blog_block on blog_block.blog_id = blog.id
    left join blog_report on blog_report.blog_id = blog.id
    group by blog.id, profile.nickname, profile.icon_image
    ORDER BY blog.updated_at DESC;

  `, [user_id, user_id, user_id, user_id]);
            return { blogList: result.rows };
        });
    }
    getBlogListBySearch(user_id, value) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield this.knex.raw(/* sql */ `
    select
      blog.id,
      blog.poster_id,
      blog.content,
      blog.updated_at,
      profile.nickname,
      profile.icon_image,
      blog.title,
      blog.image,
      (select count(*)from blog_like
        where blog_like.blog_id = blog.id
      )as like_count,
      (select count(*) 
       from blog_like 
       where blog_like.blog_id = blog.id 
         and blog_like.user_id = ?
      ) as has_like,
      (select count(*) 
       from blog_save 
       where blog_save.blog_id = blog.id 
         and blog_save.user_id = ?
      ) as has_save,
      (select count(*) 
       from blog_block 
       where blog_block.blog_id = blog.id 
         and blog_block.user_id = ?
      ) as has_block,
    (select count(*) 
     from blog_report 
     where blog_report.blog_id = blog.id 
       and blog_report.user_id = ?
    ) as has_report
    from blog
    left join blog_like on blog_like.blog_id = blog.id
    left join profile on blog.poster_id = profile.user_id
    left join blog_save on blog_save.blog_id = blog.id
    left join blog_block on blog_block.blog_id = blog.id
    left join blog_report on blog_report.blog_id = blog.id
    where blog.title ILIKE ?
    group by blog.id, profile.nickname, profile.icon_image
    ORDER BY blog.updated_at DESC;

  `, [user_id, user_id, user_id, user_id, `%${value}%`]);
            return { blogList: result.rows };
        });
    }
    getComment(blog_id, counter) {
        return __awaiter(this, void 0, void 0, function* () {
            const limit = 5 * counter;
            // const offset = limit * counter
            // console.log('counter', counter, 'limit', limit,);
            let comment_records = yield this.knex.raw(/*sql*/ `
            select
            comment_record.id,
            profile.nickname,
            profile.icon_image,
            comment_record.content,
            comment_record.updated_at
            from comment_record
            left join profile on profile.user_id = comment_record.commenter_id
            where blog_id = ?
        ORDER BY comment_record.updated_at DESC
        LIMIT ? 
        `, [blog_id, limit]);
            // lIMIT = ${limit} OFFSET = ${limit ** counter}
            return comment_records;
        });
    }
    postComment(blog_id, content, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex.insert([
                {
                    content: content,
                    blog_id: blog_id,
                    commenter_id: user_id
                }
            ]).into('comment_record');
        });
    }
    addLike(blog_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex.insert([
                {
                    user_id: user_id,
                    blog_id: blog_id
                }
            ]).into('blog_like');
        });
    }
    deleteLike(blog_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex('blog_like')
                .where({
                'blog_id': `${blog_id}`,
                'user_id': `${user_id}`
            })
                .del();
        });
    }
    postBlog(user_id, content, title, image) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log(image);
            yield this.knex.insert([
                {
                    poster_id: user_id,
                    title: title,
                    content: content,
                    image: image
                }
            ]).into("blog");
        });
    }
    saveBlog(blog_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("save blog", user_id, blog_id);
            yield this.knex.insert([
                {
                    user_id: user_id,
                    blog_id: blog_id
                }
            ]).into('blog_save');
            let saved_blog_id = yield this.knex.select('blog_id')
                .from('blog_save').where('user_id', `${user_id}`);
            return saved_blog_id;
        });
    }
    cancelSaveBlog(blog_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex('blog_save')
                .where({
                'blog_id': `${blog_id}`,
                'user_id': `${user_id}`
            })
                .del();
        });
    }
    cancelBlockedBlog(blog_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex('blog_block')
                .where({
                'blog_id': `${blog_id}`,
                'user_id': `${user_id}`
            })
                .del();
            return { success: true };
        });
    }
    blockBlog(blog_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("block blog", user_id, blog_id);
            yield this.knex.insert([
                {
                    user_id: user_id,
                    blog_id: blog_id
                }
            ]).into('blog_block');
        });
    }
    reportBlog(blog_id, user_id, statement) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("report blog", user_id, blog_id);
            yield this.knex.insert([
                {
                    user_id: user_id,
                    blog_id: blog_id,
                    statement: statement
                }
            ]).into('blog_report');
        });
    }
    deleteBlog(blog_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(blog_id, user_id);
            yield this.knex('blog')
                .where({
                'id': `${blog_id}`,
                'poster_id': `${user_id}`
            })
                .del();
            return { success: true };
        });
    }
    blogHistory(blog_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex.insert([
                {
                    user_id: user_id,
                    blog_id: blog_id
                }
            ]).into('blog_history');
        });
    }
}
exports.BlogService = BlogService;
