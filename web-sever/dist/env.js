"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.env = void 0;
const dotenv_1 = require("dotenv");
const fs_1 = require("fs");
const populate_env_1 = __importDefault(require("populate-env"));
// console.log('NODE_ENV:', process.env.NODE_ENV)
// config({ path: '.env.' + mode })
(0, dotenv_1.config)({ path: (0, fs_1.existsSync)('.env') ? '.env' : '../.env' });
exports.env = {
    DB_NAME: "",
    DB_USER: "",
    DB_PASSWORD: "",
    PORT: 8100,
    DB_HOST: "localhost",
    SESSION_SECRET: "",
    NODE_ENV: 'development',
    REACT_APP_ORIGIN: '',
    JWT_SECRET: '',
};
if (process.env.NODE_ENV === 'test') {
    exports.env.DB_HOST = process.env.POSTGRES_HOST || "";
    exports.env.DB_NAME = process.env.POSTGRES_NAME || "";
    exports.env.DB_USER = process.env.POSTGRES_USER || "";
    exports.env.DB_PASSWORD = process.env.POSTGRES_PASSWORD || "";
    exports.env.SESSION_SECRET = process.env.SESSION_SECRET || "";
    exports.env.REACT_APP_ORIGIN = process.env.REACT_APP_ORIGIN || "";
    exports.env.JWT_SECRET = process.env.JWT_SECRET || "";
}
// if(process.env.NODE_ENV === 'test') {
//   env.DB_HOST = process.env.POSTGRES_HOST
//   env.DB_NAME = process.env.POSTGRES_DB
//   env.DB_USER = process.env.POSTGRES_USER
//   env.DB_PASSWORD = process.env.POSTGRES_PASSWORD
// }
(0, populate_env_1.default)(exports.env, { mode: 'halt' });
// console.log('env:', env)
