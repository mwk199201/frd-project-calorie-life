"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const rest_controller_1 = require("./rest.controller");
const guards_1 = require("./guards");
class UserController extends rest_controller_1.RestController {
    constructor(userService) {
        super();
        this.userService = userService;
        this.loginWithPassword = (req) => {
            return this.userService.getTokenWithPassword({
                username: (0, guards_1.getBodyString)(req, 'username'),
                password: (0, guards_1.getBodyString)(req, 'password'),
            });
        };
        this.registerWithPassword = (req) => {
            return this.userService.registerWithPassword({
                username: (0, guards_1.getBodyString)(req, 'username'),
                password: (0, guards_1.getBodyString)(req, 'password'),
            });
        };
        this.getUsername = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let username = yield this.userService.getUsername(payload.id);
            return username;
        });
        this.router.post('/login/password', this.wrapControllerMethod(this.loginWithPassword));
        this.router.post('/register/password', this.wrapControllerMethod(this.registerWithPassword));
        this.router.get('/username', this.wrapControllerMethod(this.getUsername));
    }
}
exports.UserController = UserController;
