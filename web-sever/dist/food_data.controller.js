"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoodDataController = void 0;
const guards_1 = require("./guards");
const rest_controller_1 = require("./rest.controller");
class FoodDataController extends rest_controller_1.RestController {
    constructor(foodDataService) {
        super();
        this.foodDataService = foodDataService;
        this.getFoodData = (req) => __awaiter(this, void 0, void 0, function* () {
            let searchItems = req.params.searchItem;
            let result = yield this.foodDataService.getFoodDataByName(searchItems);
            return result;
        });
        this.getWaterByDate = (req) => __awaiter(this, void 0, void 0, function* () {
            let date = req.params.date;
            let payload = (0, guards_1.getJWTPayload)(req);
            let result = yield this.foodDataService.getWaterByDate(date, payload.id);
            return result;
        });
        this.getExerciseData = (req) => __awaiter(this, void 0, void 0, function* () {
            let searchItems = req.params.searchItem;
            let result = yield this.foodDataService.getExerciseDataByName(searchItems);
            return result;
        });
        this.getFoodDataByID = (req) => __awaiter(this, void 0, void 0, function* () {
            let foodID = req.params.id;
            let result = yield this.foodDataService.getFoodDataByID(foodID);
            return result;
        });
        this.getFoodDataRecord = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            // let userID = req.params.id
            let day = req.params.dayValue;
            // console.log(day)
            let result = yield this.foodDataService.getFoodDataRecords(day, payload.id);
            // console.log(result);
            return result;
        });
        this.getQuickAddFoodRecord = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let day = req.params.dayValue;
            // console.log('day', day)
            let result = yield this.foodDataService.getQuickAddFoodDataRecord(day, payload.id);
            return result;
        });
        this.getQuickAddFoodDataByID = (req) => __awaiter(this, void 0, void 0, function* () {
            let id = req.params.id;
            let result = yield this.foodDataService.getQuickFoodDataByID(id);
            return result;
        });
        this.getRecentAddFoodDataByID = (req) => __awaiter(this, void 0, void 0, function* () {
            let id = req.params.id;
            let result = yield this.foodDataService.getRecentFoodDataByID(id);
            return result;
        });
        this.getExerciseRecord = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let day = req.params.dayValue;
            let result = yield this.foodDataService.getExerciseRecord(day, payload.id);
            return result;
        });
        this.addDietITem = (req) => __awaiter(this, void 0, void 0, function* () {
            // console.log(req.body)
            let payload = (0, guards_1.getJWTPayload)(req);
            // await this.foodDataService.addDiet(req.body,userId)
            let dietItem = yield this.foodDataService.addDiet(req.body, payload.id);
            return { ok: true, dietItem };
        });
        this.addWater = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let date = req.params.date;
            // await this.foodDataService.addDiet(req.body,userId)
            let water = yield this.foodDataService.checkWater(req.body.cupOfWater, payload.id, date);
            return { ok: true, water };
        });
        this.addQuickDietITem = (req) => __awaiter(this, void 0, void 0, function* () {
            // console.log(req.body)
            let payload = (0, guards_1.getJWTPayload)(req);
            // await this.foodDataService.addDiet(req.body,userId)
            let quickDietItem = yield this.foodDataService.quickAddDiet(req.body, payload.id);
            return { ok: true, quickDietItem };
        });
        this.addExerciseItem = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let exerciseItem = yield this.foodDataService.addExercise(req.body, payload.id);
            return { ok: true, exerciseItem };
        });
        this.updateQuickList = (req) => __awaiter(this, void 0, void 0, function* () {
            let id = +req.params.id;
            // console.log('quick',id)
            if (!id) {
                throw new Error('missing id');
            }
            let list = req.body;
            // console.log('list',list)
            if (!list) {
                throw new Error('missing list in request body');
            }
            yield this.foodDataService.updateQuickDietList(id, list);
            return { ok: true, list };
        });
        this.updateRecentList = (req) => __awaiter(this, void 0, void 0, function* () {
            let id = +req.params.id;
            // console.log('recent',id)
            if (!id) {
                throw new Error('missing id');
            }
            let list = req.body;
            // console.log('list',list)
            if (!list) {
                throw new Error('missing list in request body');
            }
            yield this.foodDataService.updateRecentDietList(id, list);
            return { ok: true, list };
        });
        this.deleteRecentFoodItem = (req) => __awaiter(this, void 0, void 0, function* () {
            let id = +req.params.id;
            yield this.foodDataService.deleteRecentItem(id);
            return { ok: 'delete' };
        });
        this.deleteQuickFoodItem = (req) => __awaiter(this, void 0, void 0, function* () {
            let id = +req.params.id;
            yield this.foodDataService.deleteQuickItem(id);
            return { ok: 'delete' };
        });
        this.deleteExerciseItem = (req) => __awaiter(this, void 0, void 0, function* () {
            let id = +req.params.id;
            yield this.foodDataService.deleteExerciseItem(id);
            return { ok: 'delete' };
        });
        this.router.get('/foodData/quickAddCaloriesRecord/:dayValue', this.wrapControllerMethod(this.getQuickAddFoodRecord));
        this.router.get('/foodDate/caloriesRecord/:dayValue', this.wrapControllerMethod(this.getFoodDataRecord));
        this.router.get('/foodData/:searchItem', this.wrapControllerMethod(this.getFoodData));
        this.router.get('/ExerciseData/:searchItem', this.wrapControllerMethod(this.getExerciseData));
        this.router.get('/exerciseDataRecord/:dayValue', this.wrapControllerMethod(this.getExerciseRecord));
        this.router.get('/foodData/id/:id', this.wrapControllerMethod(this.getFoodDataByID));
        this.router.get('/foodData/quickAddFoodDetail/:id', this.wrapControllerMethod(this.getQuickAddFoodDataByID));
        this.router.get('/foodData/recentAddFoodDetail/:id', this.wrapControllerMethod(this.getRecentAddFoodDataByID));
        this.router.get('/water/:date', this.wrapControllerMethod(this.getWaterByDate));
        this.router.post('/foodData/foodItem', this.wrapControllerMethod(this.addDietITem));
        this.router.post('/foodData/quickAddFoodItem', this.wrapControllerMethod(this.addQuickDietITem));
        this.router.post('/exerciseData/exercise', this.wrapControllerMethod(this.addExerciseItem));
        this.router.post('/addWater/:date', this.wrapControllerMethod(this.addWater));
        this.router.patch('/foodData/newQuickFoodItem/:id', this.wrapControllerMethod(this.updateQuickList));
        this.router.patch('/foodData/newRecentFoodItem/:id', this.wrapControllerMethod(this.updateRecentList));
        this.router.delete('/foodData/deleteRecentFoodItem/:id', this.wrapControllerMethod(this.deleteRecentFoodItem));
        this.router.delete('/foodData/deleteQuickFoodItem/:id', this.wrapControllerMethod(this.deleteQuickFoodItem));
        this.router.delete('/exerciseData/deleteExerciseItem/:id', this.wrapControllerMethod(this.deleteExerciseItem));
    }
}
exports.FoodDataController = FoodDataController;
