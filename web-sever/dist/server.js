"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importStar(require("express"));
const fs_1 = __importDefault(require("fs"));
// import { print } from 'listening-on'
const cors_1 = __importDefault(require("cors"));
const env_1 = require("./env");
const blog_service_1 = require("./blog.service");
const db_1 = require("./db");
const blog_controller_1 = require("./blog.controller");
const food_data_service_1 = require("./food_data.service");
const food_data_controller_1 = require("./food_data.controller");
const socket_io_1 = require("socket.io");
const http_1 = __importDefault(require("http"));
const local_file_controller_1 = require("./local-file.controller");
const ChatItem_controller_1 = require("./ChatItem.controller");
const ChatItem_service_1 = require("./ChatItem.service");
const user_service_1 = require("./user.service");
const user_controller_1 = require("./user.controller");
const profile_controller_1 = require("./profile.controller");
const profile_service_1 = require("./profile.service");
const socket = require("socket.io");
let app = (0, express_1.default)();
let server = http_1.default.createServer(app);
app.use((0, express_1.urlencoded)({ extended: false }));
app.use(express_1.default.json());
app.use((0, cors_1.default)({
    origin: env_1.env.NODE_ENV === "development" ? "*" : env_1.env.REACT_APP_ORIGIN,
}));
let io = new socket_io_1.Server(server, {
    cors: { origin: env_1.env.NODE_ENV === "development" ? "*" : env_1.env.REACT_APP_ORIGIN },
});
let rooms = new Set();
io.on("connection", function (socket) {
    console.log(`io server ${socket.id}`);
    socket.on("join_room", (room) => {
        socket.join(String(room));
        console.log(`users join room ${room}`);
    });
    // socket.on('sendMessage', (message, roomName) => {
    //     io.to(roomName).emit('receiver_message', message)
    //     console.log(`message send : ${message}`)
    // })
    socket.on("leave_room", (room) => {
        socket.leave(String(room));
        console.log(`users leave room ${room}`);
    });
    socket.on("open_cam", (room) => {
        for (;;) {
            let camRoom = Math.random().toString(36).replace("0.", "calorie-");
            if (rooms.has(camRoom)) {
                continue;
            }
            rooms.add(camRoom);
            io.to(String(room)).emit("start_cam", camRoom);
            console.log(`open cam on ${room}`);
            break;
        }
    });
    socket.on("off_cam", ({ room_id, camRoom }) => {
        rooms.delete(camRoom);
        io.to(String(room_id)).emit("off_cam");
        console.log(`close cam on ${room_id}`);
    });
    socket.on("disconnect", function () {
        console.log("left chatroom", socket.id);
    });
});
if (fs_1.default.existsSync("public")) {
    app.use(express_1.default.static("public"));
}
else {
    app.use(express_1.default.static("../public"));
}
// app.use(
//   cors({
//     origin: env.NODE_ENV === "production" ? env.REACT_APP_ORIGIN : "*",
//   })
// );
// app.use(urlencoded({ extended: false }));
app.get("/test", (req, res) => {
    res.json({ value: "Hello World" });
});
let foodDataService = new food_data_service_1.FoodDataService(db_1.knex);
let foodDataController = new food_data_controller_1.FoodDataController(foodDataService);
app.use(foodDataController.router);
let userService = new user_service_1.UserService(db_1.knex);
let userController = new user_controller_1.UserController(userService);
app.use(userController.router);
let fileController = 
//   env.NODE_ENV === 'production'
//     ? new S3FileController(:
new local_file_controller_1.LocalFileController();
app.use(fileController.router);
let profileService = new profile_service_1.ProfileService(db_1.knex);
let profileController = new profile_controller_1.ProfileController(profileService, fileController);
app.use(profileController.router);
let blogService = new blog_service_1.BlogService(db_1.knex);
let blogController = new blog_controller_1.BlogController(blogService, fileController);
app.use(blogController.router);
let chatItemService = new ChatItem_service_1.ChatItemService(db_1.knex, io);
let chatItemController = new ChatItem_controller_1.ChatItemController(chatItemService, fileController, io);
app.use(chatItemController.router);
let port = 8100;
server.listen(port, () => {
    //print(port);
    console.log(`Server started at ${port}`);
});
