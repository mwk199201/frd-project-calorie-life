"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileController = void 0;
const guards_1 = require("./guards");
// import { HttpError } from "./http.error";
const rest_controller_1 = require("./rest.controller");
class ProfileController extends rest_controller_1.RestController {
    constructor(profileService, fileController) {
        super();
        this.profileService = profileService;
        this.fileController = fileController;
        this.getProfile = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let profile = yield this.profileService.getProfile(payload.id);
            return profile;
        });
        this.getSavedBlog = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let savedBlogs = yield this.profileService.getSavedBlog(payload.id);
            return savedBlogs;
        });
        this.getBlogHistory = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blogHistory = yield this.profileService.getBlogHistory(payload.id);
            return blogHistory;
        });
        this.getBlockedBlog = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blockedBlogs = yield this.profileService.getBlockedBlog(payload.id);
            return blockedBlogs;
        });
        this.postProfile = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let height = (0, guards_1.getBodyNumber)(req, 'height');
            let weight = (0, guards_1.getBodyNumber)(req, 'weight');
            let gender = (0, guards_1.getBodyString)(req, 'gender');
            let age = (0, guards_1.getBodyNumber)(req, 'age');
            let nickname = (0, guards_1.getBodyString)(req, 'nickname');
            let activity = req.body.activity;
            let aim = req.body.aim;
            console.log(payload.id, nickname, height, weight, gender, age, activity, aim);
            return yield this.profileService.postProfile(payload.id, nickname, height, weight, gender, age, activity, aim);
        });
        this.uploadIconImage = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let user_id = payload.id;
            let input = yield this.fileController.upload({
                req,
                fields: [{ name: "image", mimeTypePrefix: 'image/' }],
            });
            let image = input.files.image;
            let file = Array.isArray(image) ? image[0] : image;
            let filename = file === null || file === void 0 ? void 0 : file.newFilename;
            // console.log("input", filename);
            return yield this.profileService.uploadIconImage(user_id, filename);
        });
        this.router.get('/blog/saved', this.wrapControllerMethod(this.getSavedBlog));
        this.router.get('/blog/history', this.wrapControllerMethod(this.getBlogHistory));
        this.router.get('/blog/blocked', this.wrapControllerMethod(this.getBlockedBlog));
        this.router.post('/profile', this.wrapControllerMethod(this.postProfile));
        this.router.get('/profile', this.wrapControllerMethod(this.getProfile));
        this.router.post('/profile/iconImage', this.wrapControllerMethod(this.uploadIconImage));
    }
}
exports.ProfileController = ProfileController;
