"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoodDataService = void 0;
class FoodDataService {
    constructor(knex) {
        this.knex = knex;
    }
    getFoodDataByName(searchItem) {
        return __awaiter(this, void 0, void 0, function* () {
            let foodData = yield this.knex.select('*').from('food_calories_data').where('food_eng_name', 'like', `%${searchItem}%`);
            return { foodData };
        });
    }
    getExerciseDataByName(searchItem) {
        return __awaiter(this, void 0, void 0, function* () {
            let exerciseData = yield this.knex.select('*').from('exercise_calories_data').where('exercise_type', 'like', `%${searchItem}%`);
            return { exerciseData };
        });
    }
    getFoodDataByID(foodID) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log(foodID)
            let foodData = yield this.knex.select('*').from('food_calories_data').where('id', foodID);
            return { foodData };
        });
    }
    getFoodDataRecords(date, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            let nextDay = new Date(date).getDate() + 1;
            var nextDate = new Date();
            nextDate.setDate(nextDay);
            let endDate = nextDate.toISOString().split('T')[0];
            // console.log(endDate)
            // console.log(date)
            let foodData = yield this.knex.select('*').from('daily_diet')
                .where({ user_id })
                .where('created_at', '>=', date)
                .where('created_at', '<', endDate);
            // console.log(foodData)
            return foodData;
        });
    }
    getQuickAddFoodDataRecord(date, id) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log('date', date)
            let nextDay = new Date(date).getDate() + 1;
            var nextDate = new Date();
            nextDate.setDate(nextDay);
            let endDate = nextDate.toISOString().split('T')[0];
            // console.log(endDate)
            let foodData = yield this.knex.select('*').from('quick_add_diet')
                .where('user_id', id)
                .where('created_at', '>=', date)
                .where('created_at', '<', endDate);
            // let foodData = await this.knex.raw(/*sql*/`
            // select id,user_id,food_name,diet_type,calories,protein,carbohydrate,fat
            // from quick_add_diet where created_at between ? and ?`,
            // [date,endOfDay])
            // console.log(foodData)
            // let foodData = await this.knex.select('*').from('quick_add_diet')
            // console.log(foodData)
            return foodData;
        });
    }
    getQuickFoodDataByID(foodID) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log('quickAddFoodID',foodID)
            let foodData = yield this.knex.select('*').from('quick_add_diet').where('id', foodID);
            return foodData;
        });
    }
    getRecentFoodDataByID(foodID) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log('quickAddFoodID',foodID)
            let foodData = yield this.knex.select('*').from('daily_diet').where('id', foodID);
            return foodData;
        });
    }
    getWaterByDate(date, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log('date', date)
            let nextDay = new Date(date).getDate() + 1;
            var nextDate = new Date();
            nextDate.setDate(nextDay);
            let endDate = nextDate.toISOString().split('T')[0];
            let water = yield this.knex.select('*').from('daily_water')
                .where({ user_id })
                .where('created_at', '>=', date)
                .where('created_at', '<', endDate);
            return water;
        });
    }
    getExerciseRecord(date, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log('quickAddFoodID',foodID)
            let nextDay = new Date(date).getDate() + 1;
            var nextDate = new Date();
            nextDate.setDate(nextDay);
            let endDate = nextDate.toISOString().split('T')[0];
            let exercise = yield this.knex.select('*').from('daily_exercise_record')
                .where({ user_id })
                .where('created_at', '>=', date)
                .where('created_at', '<', endDate);
            return exercise;
        });
    }
    addDiet(dietItem, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex.insert([
                {
                    user_id: userId,
                    food_name: dietItem.food_name,
                    diet_type: dietItem.diet_type,
                    food_type: dietItem.food_type,
                    calories: dietItem.calories,
                    protein: dietItem.protein,
                    carbohydrate: dietItem.carbohydrate,
                    total_fat: dietItem.total_fat,
                    sugar: dietItem.sugar,
                    saturated_fat: dietItem.saturated_fat,
                    trans_fat: dietItem.trans_fat,
                    cholesterol: dietItem.cholesterol,
                    dietary: dietItem.dietary_fiber,
                    sodium: dietItem.sodium,
                    food_weight: dietItem.food_weight,
                }
            ]).into('daily_diet');
        });
    }
    quickAddDiet(quickDietItem, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log({userId})
            yield this.knex.insert([
                {
                    user_id: userId,
                    food_name: quickDietItem.food_name,
                    diet_type: quickDietItem.diet_type,
                    calories: quickDietItem.calories,
                    protein: quickDietItem.protein,
                    carbohydrate: quickDietItem.carbohydrate,
                    fat: quickDietItem.fat
                }
            ]).into('quick_add_diet');
        });
    }
    addExercise(exerciseList, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex.insert([
                {
                    user_id: userId,
                    exercise_type: exerciseList.exercise_type,
                    exercise_mins: exerciseList.exercise_mins,
                    exercise_calories: exerciseList.exercise_calories,
                }
            ]).into('daily_exercise_record');
        });
    }
    checkWater(cupOfWater, userId, date) {
        return __awaiter(this, void 0, void 0, function* () {
            let nextDay = new Date(date).getDate() + 1;
            var nextDate = new Date();
            nextDate.setDate(nextDay);
            let endDate = nextDate.toISOString().split('T')[0];
            let row = yield this.knex.select('*').from('daily_water')
                .where('user_id', userId)
                .where('created_at', '>=', date)
                .where('created_at', '<', endDate)
                .first();
            // let id=row[0].id
            if (!row) {
                yield this.knex.insert([
                    {
                        user_id: userId,
                        cup_of_water: cupOfWater,
                    }
                ]).into('daily_water');
            }
            else {
                let id = row.id;
                yield this.knex('daily_water').update({
                    user_id: userId,
                    cup_of_water: cupOfWater,
                }).where({ id });
            }
        });
    }
    updateQuickDietList(id, dietList) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log({dietList})
            yield this.knex('quick_add_diet').update({
                calories: dietList.calories,
                protein: dietList.protein,
                carbohydrate: dietList.carbohydrate,
                fat: dietList.fat
            }).where({ id });
        });
    }
    updateRecentDietList(id, dietList) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log({dietList})
            yield this.knex('daily_diet').update({
                calories: dietList.calories,
                protein: dietList.protein,
                carbohydrate: dietList.carbohydrate,
                total_fat: dietList.total_fat,
                sugar: dietList.sugar,
                saturated_fat: dietList.saturated_fat,
                trans_fat: dietList.trans_fat,
                cholesterol: dietList.cholesterol,
                dietary: dietList.dietary,
                sodium: dietList.sodium,
                food_weight: dietList.food_weight
            }).where({ id });
        });
    }
    deleteRecentItem(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex('daily_diet').where({ id }).del();
        });
    }
    deleteQuickItem(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex('quick_add_diet').where({ id }).del();
        });
    }
    deleteExerciseItem(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex('daily_exercise_record').where({ id }).del();
        });
    }
}
exports.FoodDataService = FoodDataService;
// async function main() {
//     let foodData = new FoodDataService(knex)
//     let data = await foodData.getFoodData();
// // }
// if (process.argv[1] === __filename) {
//     main().catch(e => console.error(e))
//         .finally(() => knex.destroy())
// }
