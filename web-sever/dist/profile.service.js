"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileService = void 0;
class ProfileService {
    constructor(knex) {
        this.knex = knex;
    }
    getSavedBlog(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            let savedBlogs = yield this.knex.select('blog_id', 'blog.title', 'blog_save.created_at')
                .from('blog_save').where('user_id', `${user_id}`)
                .join('blog', 'blog.id', 'blog_save.blog_id');
            return savedBlogs;
        });
    }
    getBlockedBlog(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            let blockedBlogs = yield this.knex.select('blog_id', 'blog.title', 'blog_block.created_at')
                .from('blog_block').where('user_id', `${user_id}`)
                .join('blog', 'blog.id', 'blog_block.blog_id');
            return blockedBlogs;
        });
    }
    getBlogHistory(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            let blogHistory = yield this.knex.select('blog_id', 'blog.title', 'blog.created_at')
                .from('blog_history').where('user_id', `${user_id}`)
                .join('blog', 'blog.id', 'blog_history.blog_id');
            return blogHistory;
        });
    }
    getProfile(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            let profile = yield this.knex
                .select('nickname', 'height', 'weight', 'age', 'activity', 'aim', 'gender', 'user_role', 'icon_image')
                .from('profile').where('user_id', `${user_id}`);
            // console.log("profile", typeof profile, profile);
            if (profile.length == 0) {
                return false;
            }
            return profile;
        });
    }
    postProfile(user_id, nickname, height, weight, gender, age, activity, aim) {
        return __awaiter(this, void 0, void 0, function* () {
            let profile = yield this.knex.select('profile.user_id').from('profile')
                .where("profile.user_id", `${user_id}`)
                .first();
            if (!profile) {
                yield this.knex.insert([
                    {
                        user_id: user_id,
                        nickname: nickname,
                        height: height,
                        weight: weight,
                        gender: gender,
                        age: age,
                        activity: activity,
                        aim: aim,
                        user_role: 'user'
                    }
                ]).into('profile');
            }
            if (profile) {
                yield this.knex('profile')
                    .where({
                    user_id: user_id,
                }).update({
                    nickname: nickname,
                    height: height,
                    weight: weight,
                    gender: gender,
                    age: age,
                    activity: activity,
                    aim: aim,
                });
            }
            return { success: true };
        });
    }
    uploadIconImage(user_id, icon_image) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log(icon_image, user_id);
            // let iconImage = await this.knex.select('icon_image')
            //     .from('profile')
            //     .where({ user_id: 8 })
            //     .first()
            // if (iconImage.icon_image !== null){
            // }
            yield this.knex('profile')
                .where({ user_id: user_id })
                .update({
                icon_image: icon_image
            });
            return { ok: true };
        });
    }
}
exports.ProfileService = ProfileService;
