"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userService = void 0;
const db_1 = require("./db");
const user_service_1 = require("./user.service");
exports.userService = new user_service_1.UserService(db_1.knex);
