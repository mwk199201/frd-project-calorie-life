"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getParamInt = exports.getBodyNumber = exports.getBodyString = exports.getJWTPayload = void 0;
const permit_1 = require("permit");
const jwt_simple_1 = __importDefault(require("jwt-simple"));
const env_1 = require("./env");
const http_error_1 = require("./http.error");
const permit = new permit_1.Bearer({
    query: 'access_token',
});
function getJWTPayload(req) {
    let token;
    try {
        token = permit.check(req);
    }
    catch (error) {
        throw new http_error_1.HttpError(401, 'missing bearer token in request. ' + error);
    }
    let payload;
    try {
        payload = jwt_simple_1.default.decode(token, env_1.env.JWT_SECRET);
    }
    catch (error) {
        throw new http_error_1.HttpError(400, 'invalid JWT in bearer token. ' + error);
    }
    return payload;
}
exports.getJWTPayload = getJWTPayload;
function getBodyString(req, field) {
    let value = req.body[field];
    if (typeof value !== 'string') {
        throw new http_error_1.HttpError(400, `invalid body, expect '${field}' to be string`);
    }
    if (!value) {
        throw new http_error_1.HttpError(400, `invalid body, expect '${field}' to be non-empty string`);
    }
    return value;
}
exports.getBodyString = getBodyString;
function getBodyNumber(req, field) {
    let value = req.body[field];
    if (typeof value !== 'number') {
        throw new http_error_1.HttpError(400, `invalid body, expect '${field}' to be number`);
    }
    if (!value) {
        throw new http_error_1.HttpError(400, `invalid body, expect '${field}' to be non-empty number`);
    }
    return value;
}
exports.getBodyNumber = getBodyNumber;
function getParamInt(req, field) {
    let str = req.params[field];
    if (!str) {
        throw new http_error_1.HttpError(400, `missing int param '${field}'`);
    }
    let num = +str;
    if (!Number.isInteger(num)) {
        throw new http_error_1.HttpError(400, `invalid param '${field}', expect an integer`);
    }
    return num;
}
exports.getParamInt = getParamInt;
