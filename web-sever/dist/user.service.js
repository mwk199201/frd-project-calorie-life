"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const hash_1 = require("./hash");
const http_error_1 = require("./http.error");
const jwt_simple_1 = __importDefault(require("jwt-simple"));
const env_1 = require("./env");
const db_1 = require("./db");
class UserService {
    constructor(knex) {
        this.knex = knex;
    }
    getTokenWithPassword(input) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield this.knex
                .select('id', 'hash_password')
                .from('user')
                .where({ username: input.username })
                .first();
            if (!user) {
                throw new http_error_1.HttpError(404, `user not found, wrong username?`);
            }
            let isPasswordMatched = yield (0, hash_1.comparePassword)({
                password: input.password,
                hash_password: user.hash_password,
            });
            if (!isPasswordMatched) {
                throw new http_error_1.HttpError(401, `wrong username or password?`);
            }
            return this.createToken(user.id);
        });
    }
    createToken(id) {
        let payload = { id };
        let token = jwt_simple_1.default.encode(payload, env_1.env.JWT_SECRET);
        return { token };
    }
    registerWithPassword(input) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield this.knex
                .select('username')
                .from('user')
                .where({ username: input.username })
                .first();
            if (user) {
                throw new http_error_1.HttpError(404, `user already exist?`);
            }
            let hash_password = yield (0, hash_1.hashPassword)(input.password);
            let register = yield this.knex
                .insert([
                {
                    username: input.username,
                    hash_password: hash_password,
                    admin: false,
                    payment: false
                }
            ]).into('user');
            console.log("register", register);
            return { register: true };
        });
    }
    getUsername(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log(user_id);
            let username = yield this.knex.select('username')
                .from('user')
                .where({ id: user_id })
                .first();
            // console.log('username', username)
            return username;
        });
    }
}
exports.UserService = UserService;
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        let userService = new UserService(db_1.knex);
        let json = yield userService.getTokenWithPassword({
            username: 'demo_user_1',
            password: 'demo_password',
        });
        // console.log(json)
    });
}
if (process.argv[1] === __filename) {
    main()
        .catch(e => console.error(e))
        .finally(() => db_1.knex.destroy());
}
