"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogController = void 0;
const guards_1 = require("./guards");
// import { HttpError } from "./http.error";
const rest_controller_1 = require("./rest.controller");
// import { UpdateAvatarResult } from "./api.types";
class BlogController extends rest_controller_1.RestController {
    constructor(blogService, fileController) {
        super();
        this.blogService = blogService;
        this.fileController = fileController;
        this.getBlogList = (req) => {
            let payload = (0, guards_1.getJWTPayload)(req);
            return this.blogService.getBlogList(payload.id);
        };
        this.getBlogListBySearch = (req) => {
            let payload = (0, guards_1.getJWTPayload)(req);
            let value = req.body.value;
            return this.blogService.getBlogListBySearch(payload.id, value);
        };
        this.getComment = (req) => __awaiter(this, void 0, void 0, function* () {
            let comment_records = yield this.blogService.getComment(req.body.id, req.body.counter);
            return { comment_records };
        });
        this.postComment = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blog_id = +req.params.blog_id;
            yield this.blogService.postComment(blog_id, req.body.commentContent, payload.id);
            let comment_records = yield this.blogService.getComment(req.body.blog_id, 1);
            return { ok: true, comment_records };
        });
        this.addLike = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blog_id = +req.params.blog_id;
            yield this.blogService.addLike(blog_id, payload.id);
            return { ok: true };
        });
        this.deleteLike = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blog_id = +req.params.blog_id;
            // console.log(blog_id);
            yield this.blogService.deleteLike(blog_id, payload.id);
            return { ok: false };
        });
        this.postBlog = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let user_id = payload.id;
            // let content = req.body.content
            // let image = req.body?.image
            let input = yield this.fileController.upload({
                req,
                fields: [{ name: "image", mimeTypePrefix: 'image/' }],
            });
            let image = input.files.image;
            let file = Array.isArray(image) ? image[0] : image;
            let filename = file === null || file === void 0 ? void 0 : file.newFilename;
            let content = input.fields.content;
            let title = input.fields.title;
            yield this.blogService.postBlog(user_id, content, title, filename);
            // console.log('image', image);
            // console.log('content', content);
            // console.log('newBlog', req.body);
            return { ok: true };
        });
        this.saveBlog = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blog_id = +req.params.blog_id;
            return yield this.blogService.saveBlog(blog_id, payload.id);
        });
        this.cancelSaveBlog = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blog_id = +req.params.blog_id;
            // console.log(blog_id);
            yield this.blogService.cancelSaveBlog(blog_id, payload.id);
            return { ok: false };
        });
        this.blockBlog = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blog_id = +req.params.blog_id;
            return yield this.blogService.blockBlog(blog_id, payload.id);
        });
        this.cancelBlockedBlog = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blog_id = +req.params.blog_id;
            // console.log(blog_id);
            yield this.blogService.cancelBlockedBlog(blog_id, payload.id);
            return { ok: true };
        });
        this.reportBlog = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blog_id = +req.params.blog_id;
            let statement = req.body.statement;
            // console.log(statement);
            yield this.blogService.reportBlog(blog_id, payload.id, statement);
            return { ok: true };
        });
        this.deleteBlog = (req) => __awaiter(this, void 0, void 0, function* () {
            let payload = (0, guards_1.getJWTPayload)(req);
            let blog_id = +req.params.blog_id;
            return yield this.blogService.deleteBlog(blog_id, payload.id);
        });
        this.router.get('/blogs', this.wrapControllerMethod(this.getBlogList));
        this.router.post('/blogs/search', this.wrapControllerMethod(this.getBlogListBySearch));
        this.router.post('/blog/:blog_id', this.wrapControllerMethod(this.getComment));
        this.router.post('/blog/post/:blog_id', this.wrapControllerMethod(this.postComment));
        this.router.post('/blog/:blog_id/like', this.wrapControllerMethod(this.addLike));
        this.router.delete('/blog/:blog_id/like', this.wrapControllerMethod(this.deleteLike));
        this.router.post('/blog', this.wrapControllerMethod(this.postBlog));
        this.router.post('/blog/:blog_id/block', this.wrapControllerMethod(this.blockBlog));
        this.router.post('/blog/:blog_id/save', this.wrapControllerMethod(this.saveBlog));
        this.router.delete('/blog/:blog_id/save', this.wrapControllerMethod(this.cancelSaveBlog));
        this.router.delete('/blog/:blog_id/block', this.wrapControllerMethod(this.cancelBlockedBlog));
        this.router.post('/blog/:blog_id/report', this.wrapControllerMethod(this.reportBlog));
        this.router.delete('/blog/del/:blog_id', this.wrapControllerMethod(this.deleteBlog));
    }
}
exports.BlogController = BlogController;
