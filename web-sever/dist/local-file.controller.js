"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocalFileController = void 0;
const formidable_1 = __importDefault(require("formidable"));
const fs_1 = require("fs");
const rest_controller_1 = require("./rest.controller");
const express_1 = __importDefault(require("express"));
const http_error_1 = require("./http.error");
const path_1 = require("path");
const uploadDir = 'uploads';
(0, fs_1.mkdirSync)(uploadDir, { recursive: true });
class LocalFileController extends rest_controller_1.RestController {
    constructor() {
        super();
        this.counter = 0;
        this.delete = (filename) => {
            let file = (0, path_1.join)(uploadDir, filename);
            (0, fs_1.access)(file, error => {
                if (error) {
                    console.error('failed to check file access right:', file);
                    return;
                }
                (0, fs_1.unlink)(file, error => {
                    if (error) {
                        console.error('failed to delete file:', file);
                    }
                });
            });
        };
        // upload = async (req: Request, res: Response) => {
        //     form.parse(req, (err, fields, files) => {
        //         console.log({ err, fields, files });
        //         if (err) {
        //             return;
        //         }
        //     })
        // }
        this.upload = (options) => {
            let form = new formidable_1.default.Formidable({
                uploadDir,
                multiples: true,
                filter: part => {
                    return !!options.fields.find(field => { var _a; return part.name == field.name && ((_a = part.mimetype) === null || _a === void 0 ? void 0 : _a.startsWith(field.mimeTypePrefix)); });
                },
                filename: (name, ext, part, form) => {
                    var _a;
                    let field = part.name;
                    let timestamp = Date.now();
                    this.counter++;
                    let extname = ((_a = part.mimetype) === null || _a === void 0 ? void 0 : _a.split('/').pop()) || 'bin';
                    let filename = `${field}-${timestamp}-${this.counter}.${extname}`;
                    return filename;
                },
            });
            return new Promise((resolve, reject) => {
                form.parse(options.req, (err, fields, files) => {
                    if (err) {
                        reject(new http_error_1.HttpError(400, 'Failed to parse form data. ' + String(err)));
                        return;
                    }
                    // console.log(fields, files);
                    resolve({ fields, files });
                });
            });
        };
        this.router.use('/uploads', express_1.default.static(uploadDir));
    }
}
exports.LocalFileController = LocalFileController;
