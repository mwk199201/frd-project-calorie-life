import express from 'express'
import { FoodDataService } from "./food_data.service";
import { getJWTPayload } from './guards';
import { RestController } from "./rest.controller";

export class FoodDataController extends RestController{

    constructor(private foodDataService:FoodDataService){
        super()
        this.router.get('/foodData/quickAddCaloriesRecord/:dayValue',this.wrapControllerMethod(this.getQuickAddFoodRecord))
        this.router.get('/foodDate/caloriesRecord/:dayValue',this.wrapControllerMethod(this.getFoodDataRecord))
        this.router.get('/foodData/:searchItem',this.wrapControllerMethod(this.getFoodData))
        this.router.get('/ExerciseData/:searchItem',this.wrapControllerMethod(this.getExerciseData))
        this.router.get('/exerciseDataRecord/:dayValue',this.wrapControllerMethod(this.getExerciseRecord))
        this.router.get('/foodData/id/:id',this.wrapControllerMethod(this.getFoodDataByID))  
        this.router.get('/foodData/quickAddFoodDetail/:id',this.wrapControllerMethod(this.getQuickAddFoodDataByID))  
        this.router.get('/foodData/recentAddFoodDetail/:id',this.wrapControllerMethod(this.getRecentAddFoodDataByID))  
        this.router.get('/water/:date',this.wrapControllerMethod(this.getWaterByDate))  
        this.router.post('/foodData/foodItem',this.wrapControllerMethod(this.addDietITem))
        this.router.post('/foodData/quickAddFoodItem',this.wrapControllerMethod(this.addQuickDietITem))
        this.router.post('/exerciseData/exercise',this.wrapControllerMethod(this.addExerciseItem))
        this.router.post('/addWater/:date',this.wrapControllerMethod(this.addWater))
        this.router.patch('/foodData/newQuickFoodItem/:id',this.wrapControllerMethod(this.updateQuickList))
        this.router.patch('/foodData/newRecentFoodItem/:id',this.wrapControllerMethod(this.updateRecentList))
        this.router.delete('/foodData/deleteRecentFoodItem/:id',this.wrapControllerMethod(this.deleteRecentFoodItem))
        this.router.delete('/foodData/deleteQuickFoodItem/:id',this.wrapControllerMethod(this.deleteQuickFoodItem))
        this.router.delete('/exerciseData/deleteExerciseItem/:id',this.wrapControllerMethod(this.deleteExerciseItem))
    }

    getFoodData=async(req:express.Request)=>{
      let searchItems = req.params.searchItem
      let result = await this.foodDataService.getFoodDataByName(searchItems)
      return result
    }

    getWaterByDate=async(req:express.Request)=>{
      let date=req.params.date
      let payload = getJWTPayload(req)
      let result = await this.foodDataService.getWaterByDate(date,payload.id)
      return result
    }

    getExerciseData=async(req:express.Request)=>{
      let searchItems = req.params.searchItem
      let result = await this.foodDataService.getExerciseDataByName(searchItems)
      return result
    }

    getFoodDataByID=async(req:express.Request)=>{
      let foodID = req.params.id
      let result = await this.foodDataService.getFoodDataByID(foodID)
      return result;
    }

    getFoodDataRecord=async(req:express.Request)=>{
      let payload = getJWTPayload(req)
      // let userID = req.params.id
      let day= req.params.dayValue
      // console.log(day)
      let result = await this.foodDataService.getFoodDataRecords(day,payload.id)
      // console.log(result);
      return result;
    }

    getQuickAddFoodRecord=async(req:express.Request)=>{
      let payload = getJWTPayload(req)
      let day= req.params.dayValue
      // console.log('day', day)
      let result=await this.foodDataService.getQuickAddFoodDataRecord(day,payload.id)
      return result
    }

    getQuickAddFoodDataByID=async(req:express.Request)=>{
      let id=req.params.id
      let result= await this.foodDataService.getQuickFoodDataByID(id)
      return result
    }

    getRecentAddFoodDataByID=async(req:express.Request)=>{
      let id=req.params.id
      let result= await this.foodDataService.getRecentFoodDataByID(id)
      return result
    }

    getExerciseRecord=async(req:express.Request)=>{
      let payload = getJWTPayload(req)
      let day= req.params.dayValue
      let result= await this.foodDataService.getExerciseRecord(day,payload.id)
      return result
    }

    addDietITem= async (req:any)=>{
      // console.log(req.body)
      let payload = getJWTPayload(req)
      // await this.foodDataService.addDiet(req.body,userId)
      let dietItem = await this.foodDataService.addDiet(req.body,payload.id)
        return { ok: true, dietItem }
    }

    addWater= async (req:any)=>{
      let payload = getJWTPayload(req)
      let date=req.params.date
      // await this.foodDataService.addDiet(req.body,userId)
      let water = await this.foodDataService.checkWater(req.body.cupOfWater,payload.id,date)
        return { ok: true, water }
    }

    addQuickDietITem= async (req:any)=>{
      // console.log(req.body)
      let payload= getJWTPayload(req)
      // await this.foodDataService.addDiet(req.body,userId)
      let quickDietItem = await this.foodDataService.quickAddDiet(req.body,payload.id)
        return { ok: true, quickDietItem }
    }

    addExerciseItem= async (req:any)=>{
      let payload = getJWTPayload(req)
      
      let exerciseItem = await this.foodDataService.addExercise(req.body,payload.id)
        return { ok: true,exerciseItem }
    }

    updateQuickList= async (req:express.Request)=>{
      let id = +req.params.id
      // console.log('quick',id)
      if(!id){
        throw new Error('missing id')
      }
      let list=req.body
      // console.log('list',list)
      if(!list){
        throw new Error('missing list in request body')
      }
      await this.foodDataService.updateQuickDietList(id,list)
    return { ok: true ,list}
    }

    updateRecentList= async (req:express.Request)=>{
      let id = +req.params.id
      // console.log('recent',id)
      if(!id){
        throw new Error('missing id')
      }
      let list=req.body
      // console.log('list',list)
      if(!list){
        throw new Error('missing list in request body')
      }
      await this.foodDataService.updateRecentDietList(id,list)
    return { ok: true ,list}
    }

    deleteRecentFoodItem=async (req:express.Request) => {
      let id = +req.params.id
      await this.foodDataService.deleteRecentItem(id)
      return { ok: 'delete' }
    }

    deleteQuickFoodItem=async (req:express.Request) => {
      let id = +req.params.id
      await this.foodDataService.deleteQuickItem(id)
      return { ok: 'delete' }
    }

    deleteExerciseItem=async (req:express.Request) => {
      let id = +req.params.id
      await this.foodDataService.deleteExerciseItem(id)
      return { ok: 'delete' }
    }
}