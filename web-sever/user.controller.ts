import { RestController } from "./rest.controller";
import { UserService } from "./user.service";
import express from 'express'
import { getBodyString, getJWTPayload } from "./guards";


export class UserController extends RestController {
    constructor(private userService: UserService) {
        super();
        this.router.post(
            '/login/password',
            this.wrapControllerMethod(this.loginWithPassword),
        )
        this.router.post(
            '/register/password',
            this.wrapControllerMethod(this.registerWithPassword),
        )
        this.router.get('/username', this.wrapControllerMethod(this.getUsername))

    }
    loginWithPassword = (req: express.Request) => {
        return this.userService.getTokenWithPassword({
            username: getBodyString(req, 'username'),
            password: getBodyString(req, 'password'),
        })
    }
    registerWithPassword = (req: express.Request) => {

        return this.userService.registerWithPassword({
            username: getBodyString(req, 'username'),
            password: getBodyString(req, 'password'),
        })
    }

    getUsername = async (req: express.Request) => {
        let payload = getJWTPayload(req)
        let username = await this.userService.getUsername(payload.id)
        return username
    }
}