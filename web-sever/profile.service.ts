import { Knex } from "knex";



export class ProfileService {
    constructor(private knex: Knex) { }


    async getSavedBlog(user_id: number) {
        let savedBlogs = await this.knex.select('blog_id', 'blog.title', 'blog_save.created_at')
            .from('blog_save').where('user_id', `${user_id}`)
            .join('blog', 'blog.id', 'blog_save.blog_id')

        return savedBlogs
    }
    async getBlockedBlog(user_id: number) {
        let blockedBlogs = await this.knex.select('blog_id', 'blog.title', 'blog_block.created_at')
            .from('blog_block').where('user_id', `${user_id}`)
            .join('blog', 'blog.id', 'blog_block.blog_id')

        return blockedBlogs
    }


    async getBlogHistory(user_id: number) {

    
        let blogHistory = await this.knex.select('blog_id', 'blog.title', 'blog_history.created_at')
            .from('blog_history').where('user_id', `${user_id}`)
            .join('blog', 'blog.id', 'blog_history.blog_id')
        return blogHistory
    }

    async getProfile(user_id: number) {
        let profile = await this.knex
            .select('nickname', 'height', 'weight', 'age', 'activity', 'aim', 'gender', 'user_role', 'icon_image')
            .from('profile').where('user_id', `${user_id}`)

        // console.log("profile", typeof profile, profile);
        if (profile.length == 0) {
            return false
        }
        return profile
    }

    async postProfile(user_id: number,
        nickname: string,
        height: number,
        weight: number,
        gender: string,
        age: number,
        activity: string,
        aim: string) {
        let profile = await this.knex.select('profile.user_id').from('profile')
            .where("profile.user_id", `${user_id}`)
            .first()
        if (!profile) {
            await this.knex.insert([
                {
                    user_id: user_id,
                    nickname: nickname,
                    height: height,
                    weight: weight,
                    gender: gender,
                    age: age,
                    activity: activity,
                    aim: aim,
                    user_role: 'user'
                }
            ]).into('profile')
        }
        if (profile) {
            await this.knex('profile')
                .where({
                    user_id: user_id,
                }).update({
                    nickname: nickname,
                    height: height,
                    weight: weight,
                    gender: gender,
                    age: age,
                    activity: activity,
                    aim: aim,
                })
        }



        return { success: true }
    }

    async uploadIconImage(user_id: number, icon_image: string) {
        // console.log(icon_image, user_id);

        // let iconImage = await this.knex.select('icon_image')
        //     .from('profile')
        //     .where({ user_id: 8 })
        //     .first()

        // if (iconImage.icon_image !== null){

        // }


        await this.knex('profile')
            .where({ user_id: user_id })
            .update({
                icon_image: icon_image
            })

        return { ok: true }
    }

}
