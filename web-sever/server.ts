import express, { urlencoded } from 'express'
import fs from 'fs'
// import { print } from 'listening-on'
import cors from 'cors'
import { env } from './env'
import { BlogService } from './blog.service'
import { knex } from './db'
import { BlogController } from './blog.controller'
import { FoodDataService } from './food_data.service'
import { FoodDataController } from './food_data.controller'
import { Server as socketio } from 'socket.io'
import http from 'http'
import { LocalFileController } from './local-file.controller'
import { ChatItemController } from './ChatItem.controller'
import { ChatItemService } from './ChatItem.service'
import { UserService } from './user.service'
import { UserController } from './user.controller'
import { ProfileController } from './profile.controller'
import { ProfileService } from './profile.service'


const socket = require("socket.io");

let app = express();
let server = http.createServer(app);

app.use(urlencoded({ extended: false }));
app.use(express.json());

app.use(
  cors({
    origin: env.NODE_ENV === "development" ? "*" : env.REACT_APP_ORIGIN,
  })
);

let io = new socketio(server, {
  cors: { origin: env.NODE_ENV === "development" ? "*" : env.REACT_APP_ORIGIN },
});

let rooms = new Set();

io.on("connection", function (socket) {
  console.log(`io server ${socket.id}`);

  socket.on("join_room", (room) => {
    socket.join(String(room));
    console.log(`users join room ${room}`);
  });

  // socket.on('sendMessage', (message, roomName) => {
  //     io.to(roomName).emit('receiver_message', message)
  //     console.log(`message send : ${message}`)
  // })

  socket.on("leave_room", (room) => {
    socket.leave(String(room));
    console.log(`users leave room ${room}`);
  });

  socket.on("open_cam", (room) => {
    for (; ;) {
      let camRoom = Math.random().toString(36).replace("0.", "calorie-");
      if (rooms.has(camRoom)) {
        continue;
      }
      rooms.add(camRoom);
      io.to(String(room)).emit("start_cam", camRoom);
      console.log(`open cam on ${room}`);
      break;
    }
  });

  socket.on("off_cam", ({ room_id, camRoom }) => {
    rooms.delete(camRoom);
    io.to(String(room_id)).emit("off_cam");
    console.log(`close cam on ${room_id}`);
  });

  socket.on("disconnect", function () {
    console.log("left chatroom", socket.id);
  });
});

if (fs.existsSync("public")) {
  app.use(express.static("public"));
} else {
  app.use(express.static("../public"));
}

// app.use(
//   cors({
//     origin: env.NODE_ENV === "production" ? env.REACT_APP_ORIGIN : "*",
//   })
// );

// app.use(urlencoded({ extended: false }));

app.get("/test", (req, res) => {
  res.json({ value: "Hello World" });
});

let foodDataService = new FoodDataService(knex);
let foodDataController = new FoodDataController(foodDataService);
app.use(foodDataController.router);

let userService = new UserService(knex);
let userController = new UserController(userService);
app.use(userController.router);

let fileController =
  //   env.NODE_ENV === 'production'
  //     ? new S3FileController(:
  new LocalFileController();
app.use(fileController.router);

let profileService = new ProfileService(knex);
let profileController = new ProfileController(profileService, fileController);

app.use(profileController.router);

let blogService = new BlogService(knex);
let blogController = new BlogController(blogService, fileController);

app.use(blogController.router);

let chatItemService = new ChatItemService(knex, io);
let chatItemController = new ChatItemController(
  chatItemService,
  fileController,
  io
);

app.use(chatItemController.router);

let port = 8100;
server.listen(port, () => {
  //print(port);
  console.log(`Server started at ${port}`);
});
