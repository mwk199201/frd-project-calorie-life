import express from 'express'
import { Bearer } from 'permit'
import jwtSimple from 'jwt-simple'
import { env } from './env'
import { HttpError } from './http.error'
import { JWTPayload } from './api.types'

const permit = new Bearer({
  query: 'access_token',
})


export function getJWTPayload(req: express.Request): JWTPayload {
  let token: string
  try {
    token = permit.check(req)
  } catch (error) {
    throw new HttpError(401, 'missing bearer token in request. ' + error)
  }

  let payload: JWTPayload
  try {
    payload = jwtSimple.decode(token, env.JWT_SECRET)
  } catch (error) {
    throw new HttpError(400, 'invalid JWT in bearer token. ' + error)
  }

  return payload
}


export function getBodyString(req: express.Request, field: string): string {
  let value = req.body[field]
  if (typeof value !== 'string') {
    throw new HttpError(400, `invalid body, expect '${field}' to be string`)
  }
  if (!value) {
    throw new HttpError(
      400,
      `invalid body, expect '${field}' to be non-empty string`,
    )
  }
  return value
}

export function getBodyNumber(req: express.Request, field: string): number {
  let value = req.body[field]
  if (typeof value !== 'number') {
    throw new HttpError(400, `invalid body, expect '${field}' to be number`)
  }
  if (!value) {
    throw new HttpError(
      400,
      `invalid body, expect '${field}' to be non-empty number`,
    )
  }
  return value
}


export function getParamInt(req: express.Request, field: string): number {
  let str = req.params[field]
  if (!str) {
    throw new HttpError(400, `missing int param '${field}'`)
  }
  let num = +str
  if (!Number.isInteger(num)) {
    throw new HttpError(400, `invalid param '${field}', expect an integer`)
  }
  return num
}
