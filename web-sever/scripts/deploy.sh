set -e
# npm run build
# rsync -SavP -rv dist 'haveanice:~/frd-project-calorie-life/web-sever/'
rsync -SavP -rv public 'haveanice:~/frd-project-calorie-life/web-sever/'
# ssh haveanice '. ~/.nvm/nvm.sh && cd ~/frd-project-calorie-life/web-sever/dist && pnpm i'
# ssh haveanice 'forever restart dist/server.js'