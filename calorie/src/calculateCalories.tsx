export function calculateCalories(age:number,weight:number , height:number,activity:number ,gender:string,aim:string) {
    let totalCalories
    if(gender === 'male' && activity === 1) {
        totalCalories = Math.floor( 1.2 * (66.5 + (13.75 *(weight)) + (5.003 *(height)) - (6.755 *(age))));
      } else if(gender === 'male' && activity === 2) {
        totalCalories = Math.floor(1.375 * (66.5 + (13.75 *(weight)) + (5.003 *(height)) - (6.755 *(age))));
      } else if (gender === 'male' && activity === 3) {
        totalCalories = Math.floor(1.55 * (66.5 + (13.75 *(weight)) + (5.003 *(height)) - (6.755 *(age))));
      } else if(gender === 'male' && activity === 4) {
        totalCalories = Math.floor(1.725 * (66.5 + (13.75 *(weight)) + (5.003 *(height)) - (6.755 *(age))));
      } else if(gender === 'male' && activity === 5) {
        totalCalories = Math.floor(1.9 * (66.5 + (13.75 *(weight)) + (5.003 *(height)) - (6.755 *(age))))
        ;
      } else if(gender === 'female' && activity === 1) {
        totalCalories = Math.floor(1.2 * (655 + (9.563 *(weight)) + (1.850 *(height)) - (4.676 *(age))));
      } else if(gender === 'female' && activity === 2) {
        totalCalories = Math.floor(1.375 * (655 + (9.563 *(weight)) + (1.850 *(height)) - (4.676 *(age))));
      } else if(gender === 'female' && activity === 3) {
        totalCalories = Math.floor(1.55 * (655 + (9.563 *(weight)) + (1.850 *(height)) - (4.676 *(age))));
      } else if(gender === 'female' && activity === 4) {
        totalCalories = Math.floor(1.725* (655 + (9.563 *(weight)) + (1.850 *(height)) - (4.676 *(age))));
      } else {
        totalCalories = Math.floor(1.9 * (655 + (9.563 *(weight)) + (1.850 *(height)) - (4.676 *(age))));
      } 
    if(aim==='BuildMuscle'){
      totalCalories=Math.floor(totalCalories*1.2)
    }else if(aim==='LoseWeight'){
      totalCalories=Math.floor(totalCalories*0.8)
    }
      return totalCalories
}