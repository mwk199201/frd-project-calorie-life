import { IonContent} from "@ionic/react";
import styles from "./WaveHeader.module.css";

const WaveHeader: React.FC = () => {
  return (
    <IonContent>
      <div className={styles.waveContainer}>
        <div className={styles.wave}></div>
      </div>
    </IonContent>
  );
};

export default WaveHeader;
