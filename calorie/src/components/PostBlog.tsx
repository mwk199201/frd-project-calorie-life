import {
  IonButton,
  IonChip,
  IonContent,
  IonHeader,
  IonIcon,
  IonLabel,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
// import { text } from "express";
import {  personAddOutline } from "ionicons/icons";
// import { useState } from "react";
// import ExploreContainer from "./ExploreContainer";
import ProfileItem from "./profile_compenont/ProfileItem";
import Logo from "./icon.png";

const PostBlog: React.FC = () => {
  return (
    <IonPage>
      <IonContent fullscreen>
        <IonHeader>
          <IonToolbar color="orange">
            <IonTitle>Profile</IonTitle>
          </IonToolbar>
        </IonHeader>
        <div className="user-header">
          <div className="user-photo">
            <img src={Logo} />
            <button type="submit">
              <IonIcon icon={personAddOutline} />
            </button>
          </div>
          <div className="usernames">
            userName
            <br />
            <span> Status:</span>
            <IonChip>
              <IonLabel>member/Trainer/Processing...</IonLabel>
            </IonChip>
          </div>
          <br />

          <div className="function-button">
            <IonButton color="primary" className="click-btn">
              Apply{" "}
            </IonButton>
            <IonButton color="primary" className="click-btn">
              Follow{" "}
            </IonButton>
            <IonButton color="primary" className="click-btn">
              Chat{" "}
            </IonButton>
          </div>
        </div>
        <div className="line"></div>

        <ProfileItem />

        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large"></IonTitle>
          </IonToolbar>
        </IonHeader>
      </IonContent>
      <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
    </IonPage>
  );
};

export default PostBlog;
