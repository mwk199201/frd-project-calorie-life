import {  IonIcon, IonLabel } from "@ionic/react";
import { useLocation } from "react-router";

export default function TabButtonContent({
  tab,
  path,
  color,
  content,
  icon,
}: {
  tab: string;
  path: string;
  color: string;
  content: string;
  icon: any;
}) {
  let location = useLocation();

  const active = location.pathname.includes(path);

  return (
    <>
      <IonIcon color={active ? "orange" : color} icon={icon} />
      <IonLabel color={active ? "orange" : color}>{content}</IonLabel>
    </>
  );
}
