import { IonAvatar, IonImg } from "@ionic/react";
import moment from "moment";
import styles from "./Comment.module.css";

export default function Comment(props: any) {
  return (
    <div className={styles.commentBox}>
      <IonAvatar className={styles.commentIcon}>
        <IonImg src={props.commenter_icon}></IonImg>
      </IonAvatar>
      <div className={styles.contentBox}>
        <div className={styles.commentContent}>
          <div className={styles.commentUsername}>{props.commenter_name}</div>

          {props.commenter_content}
        </div>
        <div className={styles.commentTime}>
          {moment(props.comment_time).fromNow()}
        </div>
      </div>
    </div>
  );
}
