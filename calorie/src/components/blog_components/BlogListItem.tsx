import {
  IonCard,
  IonCardSubtitle,
  IonCardContent,
  IonItem,
  IonIcon,
  IonLabel,
  IonButton,
  IonImg,
  IonAvatar,
  IonList,
  IonInput,
  IonSelect,
  IonSelectOption,
  IonButtons,
  IonContent,
  IonHeader,
  IonModal,
  IonPage,
  IonTitle,
  IonToolbar,
  IonCardTitle,
} from "@ionic/react";
import { BlogItem, RootState } from "../../redux/state";
import styles from "./BlogListItem.module.css";
import moment from "moment";
import {
  bookmark,
  chatboxEllipsesOutline,
  chatbubble,
  chatbubbleOutline,
  heart,
  heartOutline,
  shareSocial,
} from "ionicons/icons";
import { useDispatch, useSelector } from "react-redux";
import { useRef, useState } from "react";
import Comment from "./Comment";
import { RootThunkDispatch } from "../../redux/dispatch";
import {
  delPostThunk,
  loadBlogCommentsThunk,
  loadBlogListThunk,
  postCommentThunk,
} from "../../redux/blogList/thunk";

import defaultIcon from "../../default-user-icon-13.jpeg";
import { APIOrigin, del, post } from "../../api";
import { useHistory, useParams } from "react-router-dom";
import { AuthState } from "../../redux/auth/state";
import {
  addLikeAction,
  delLikeAction,
  updateBlockedBlogs,
  updateSavedBlogs,
} from "../../redux/blogList/action";
// import { reset } from "fetch-mock";

const options = [
  {
    code: "Nudity or pornography",
    en: "Nudity or pornography",
    zh: "裸露或色情",
  },
  {
    code: "Violence",
    en: "Violence",
    zh: "暴力",
  },
  {
    code: "Harassment",
    en: "Harassment",
    zh: "騷擾",
  },
  {
    code: "Suicide or self-harm",
    en: "Suicide or self-harm",
    zh: "自殺或自殘",
  },
  {
    code: "False news",
    en: "False news",
    zh: "虛假新聞",
  },
  {
    code: "Spam",
    en: "Spam",
    zh: "垃圾訊息",
  },
  {
    code: "Unauthorized sales",
    en: "Unauthorized sales",
    zh: "未經授權銷售",
  },
  {
    code: "Hate speech",
    en: "Hate speech",
    zh: "仇恨言論",
  },
  {
    code: "Terrorism",
    en: "Terrorism",
    zh: "恐怖主義",
  },
  {
    code: "Copyright infringement",
    en: "Copyright infringement",
    zh: "侵犯版權",
  },
  {
    code: "Other",
    en: "Other (please specify)",
    zh: "其他 (請列明)",
  },
];

type Props = BlogItem;

export default function BlogListItem(props: Props) {
  let history = useHistory();
  let authUser_id = useSelector((state: RootState) =>
    state.auth.status === "authenticated" ? state.auth.user_id : null
  );

  const timePassed = moment(props.updated_at).fromNow();
  const blogItem = useSelector(
    (state: RootState) => state.blogList.items
  )?.filter((v) => v.id === props.id);
  const [isShowComment, setIsShowComment] = useState(false);
  const [textValue, setTextValue] = useState("");
  const [likeCount, SetLikeCount] = useState(+props.like_count);
  const [isLike, setIsLike] = useState(getHasLike(props));
  const [counter, setCounter] = useState(2);
  const [noMoreComments, setIsNoMoreComments] = useState(false);
  const [selectReport, setIsSelectReport] = useState(false);
  const isSave = props.has_save === "1" ? true : false;
  const [isBlock, setIsBlock] = useState(getHasBlock(props));
  const [hasReport, setHasReport] = useState(getHasReport(props));
  const ref = useRef(null);

  const dispatch = useDispatch<RootThunkDispatch>();
  function showComment(blog_id: number) {
    let counter = 1;
    dispatch(loadBlogCommentsThunk(blog_id, counter));
    setIsShowComment(!isShowComment);
  }
  function showMoreComment(blog_id: number) {
    CheckNoComment();
    setCounter(counter + 1);
    dispatch(loadBlogCommentsThunk(blog_id, counter));
  }
  function CheckNoComment() {
    if (Array.isArray(blogItem) && blogItem.length >= 1) {
      const length = blogItem[0].commentList!.length;
      if (length % 5 !== 0 || length === 0) {
        return setIsNoMoreComments(true);
      }
    }
    return setIsNoMoreComments(false);
  }

  function postComment(blog_id: number, commentContent: string) {
    dispatch(postCommentThunk(blog_id, commentContent));
    setTextValue("");
  }
  // function getBoolean(props: string){
  //   return +props > 0
  // }
  function getHasLike(props: Props) {
    return +props.has_like > 0;
  }
  function getHasBlock(props: Props) {
    if (+props.has_block > 0) {
      return true;
    } else {
      return false;
    }
  }
  function getHasReport(props: Props) {
    if (+props.has_report > 0) {
      return true;
    } else {
      return false;
    }
  }

  function unLikeBtn() {
    const fetchData = async () => {
      let blog_id = props.id;
      const result = await del(`/blog/${blog_id}/like`);
      if (result.ok === false) {
        dispatch(delLikeAction(blog_id, "0"));
      }
      setIsLike(false);
      SetLikeCount(likeCount - 1);
    };
    fetchData();
  }
  function postLikeBtn() {
    const fetchData = async () => {
      let blog_id = props.id;
      const result = await post(`/blog/${blog_id}/like`);
      if (result.ok === true) {
        dispatch(addLikeAction(blog_id, "1"));
      }
      setIsLike(true);
      SetLikeCount(likeCount + 1);
    };
    fetchData();
  }
  function saveBlog() {
    const fetchData = async () => {
      let blog_id = props.id;
      await post(`/blog/${blog_id}/save`);
    };
    fetchData();
  }
  function cancelSave() {
    const fetchData = async () => {
      let blog_id = props.id;
      let unsave = await del(`/blog/${blog_id}/save`);
    };
    fetchData();
  }
  function blockBlog() {
    const fetchData = async () => {
      let blog_id = props.id;
      const result = await post(`/blog/${blog_id}/block`);
    };
    fetchData();
  }

  const reportBlog = async (value: string) => {
    try {
      let blog_id = props.id;
      const formObject = {
        id: blog_id,
        statement: value,
      };

      const res = await fetch(`${APIOrigin}/blog/${blog_id}/report`, {
        method: "POST",
        headers: {
          "content-type": "application/json; charset=utf-8",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(formObject),
      });
      const result = await res.json();
      return result;
    } catch (error) {
      return { error: String(error) };
    }
  };

  const { REACT_APP_API_ORIGIN } = process.env;
  let blog_image = `${REACT_APP_API_ORIGIN}/uploads/` + props.image;
  let poster_icon_image = `${REACT_APP_API_ORIGIN}/uploads/` + props.icon_image;
  const cardContent = props.content;

  function showIcon(icon: any) {
    if (!icon) {
      return defaultIcon;
    }
    return `${REACT_APP_API_ORIGIN}/uploads/` + icon;
  }
  function actionForThePost(value: string) {
    if (value === "save") {
      saveBlog();
      dispatch(updateSavedBlogs(props.id, "1"));
    } else if (value === "report") {
      setIsSelectReport(true);
      setTimeout(() => {
        (ref.current as any).open();
      }, 1000);
    } else if (value === "block") {
      blockBlog();
      setIsBlock(true);
      dispatch(updateBlockedBlogs(props.id, "1"));
    } else if (value === "delete") {
      dispatch(delPostThunk(props.id));
    } else if (value === "") {
      return;
    } else if (value === "unSave") {
      cancelSave();
      dispatch(updateSavedBlogs(props.id, "0"));
    } else {
      setHasReport(true);
      reportBlog(value);
    }
  }

  const [OtherUser, seOtherUser] = useState<{
    user2_id: number;
  }>();

  let target_id = +props.poster_id;
  let params = useParams<{ user_id: string }>();
  const user_id = params.user_id;

  const openNewRoom = async () => {
    const res = await fetch(`${APIOrigin}/chatrooms/user/${target_id}`, {
      method: "POST",
      redirect: "follow",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    const result = await res.json();
    if (result.ok) {
      history.push(`/tab/chatroom/user/${target_id}`);
      seOtherUser(result.user2_id);
      console.log(
        `traget_id is ${target_id} and otheruser_id is ${authUser_id}`
      );
    }
  };

  return (
    <>
      {hasReport || isBlock ? null : (
        <IonCard className={styles.card}>
          {isSave ? (
            <ion-tabs>
              <IonIcon size="large" color="success" icon={bookmark}></IonIcon>
              <IonCardTitle color="primary" className="ion-padding">
                Title : {props.title}
              </IonCardTitle>
            </ion-tabs>
          ) : (
            <ion-tabs>
              <IonCardTitle color="primary" className="ion-padding">
                Title : {props.title}
              </IonCardTitle>
            </ion-tabs>
          )}
          <IonCardSubtitle color="nothing">
            <IonItem>
              <IonAvatar>
                {props.icon_image === null ? (
                  <img src={defaultIcon} />
                ) : (
                  <IonImg src={poster_icon_image}></IonImg>
                )}
              </IonAvatar>
              <div className="ion-padding">
                <IonLabel>{props.nickname}</IonLabel>
                <IonCardSubtitle className={styles.timeShow}>
                  {timePassed}
                </IonCardSubtitle>
              </div>
              {isSave ? (
                <IonItem slot="end" className={styles.selectorItem}>
                  <IonSelect
                    ref={ref}
                    placeholder=""
                    onIonChange={(e) => {
                      actionForThePost(e.detail.value);
                      //(ref.current as any).open();
                    }}
                    value=""
                  >
                    {selectReport === false ? (
                      <>
                        <IonSelectOption value="unSave">
                          {"Cancel save"}
                        </IonSelectOption>
                        <IonSelectOption value="block">
                          {"Block post"}
                        </IonSelectOption>
                        <IonSelectOption value="report">
                          {"Report post"}
                        </IonSelectOption>
                        {authUser_id === props.poster_id ? (
                          <IonSelectOption value="delete">
                            {"Delete post"}
                          </IonSelectOption>
                        ) : null}
                      </>
                    ) : (
                      <>
                        {options.map((item, i) => {
                          return (
                            <IonSelectOption
                              key={i}
                              value={item.en}
                            ></IonSelectOption>
                          );
                        })}
                      </>
                    )}
                  </IonSelect>
                </IonItem>
              ) : (
                <IonItem slot="end" className={styles.selectorItem}>
                  <IonSelect
                    ref={ref}
                    placeholder=""
                    onIonChange={(e) => {
                      actionForThePost(e.detail.value);
                      //(ref.current as any).open();
                    }}
                    value=""
                  >
                    {selectReport === false ? (
                      <>
                        <IonSelectOption value="save">
                          {"Save post"}
                        </IonSelectOption>
                        <IonSelectOption value="block">
                          {"Block post"}
                        </IonSelectOption>
                        <IonSelectOption value="report">
                          {"Report post"}
                        </IonSelectOption>

                        {authUser_id === props.poster_id ? (
                          <IonSelectOption value="delete">
                            {"Delete post"}
                          </IonSelectOption>
                        ) : null}
                      </>
                    ) : (
                      <>
                        {options.map((item, i) => {
                          return (
                            <IonSelectOption key={i} value={item.en}>
                              {item.en}
                            </IonSelectOption>
                          );
                        })}
                      </>
                    )}
                  </IonSelect>
                </IonItem>
              )}
            </IonItem>
          </IonCardSubtitle>
          {cardContent.split("\n").map((d) => (
            <div
              className="ion-padding"
              style={{ paddingTop: "2px", paddingBottom: "2px" }}
            >{`${d}`}</div>
          ))}
          {/* <IonCardContent>{`${cardContent}`}</IonCardContent> */}

          {props.image == null ? <></> : <IonImg src={blog_image}></IonImg>}

          <div className="ion-text-end">
            <IonButton
              slot="end"
              size="small"
              color={"light"}
              onClick={() => showComment(props.id)}
            >
              {isShowComment ? (
                <IonIcon color="primary" icon={chatbubble} />
              ) : (
                <IonIcon color="medium" icon={chatbubbleOutline} />
              )}{" "}
              comment
            </IonButton>
            {isLike ? (
              <IonButton
                size="small"
                color={"light"}
                onClick={() => {
                  unLikeBtn();
                }}
              >
                <IonIcon color="danger" icon={heart} />
                {likeCount}
              </IonButton>
            ) : (
              <IonButton
                size="small"
                color={"light"}
                onClick={() => {
                  postLikeBtn();
                }}
              >
                <IonIcon color="medium" icon={heartOutline} />
                {likeCount}
              </IonButton>
            )}
            {/* {props.like_count} */}
            {}
            <IonButton
              slot="end"
              color={"light"}
              size="small"
              onClick={() => {
                openNewRoom();
              }}
              hidden={authUser_id === props.poster_id}
            >
              {
                <div>
                  <IonIcon color="tertiary" icon={chatboxEllipsesOutline} />
                  chat
                </div>
              }
            </IonButton>
          </div>
          {isShowComment ? (
            <IonList>
              {/* {commentList.commentListStatus === "loading" && <div>Loading...</div>} */}
              <IonItem class="ion-padding">
                <IonInput
                  value={textValue}
                  class="ion-padding"
                  placeholder="say something"
                  onIonChange={(e) => setTextValue(e.detail.value!)}
                ></IonInput>
                <IonButton
                  onClick={() => {
                    postComment(props.id, textValue);
                    // setIsPostComment(!isPostComment);
                  }}
                >
                  POST
                </IonButton>
              </IonItem>
              {Array.isArray(blogItem) &&
                blogItem.length >= 1 &&
                blogItem[0].commentList!.map((v: any, i) => (
                  <Comment
                    key={i}
                    commenter_name={v.nickname}
                    comment_time={v.updated_at}
                    commenter_icon={showIcon(v.icon_image)}
                    commenter_content={v.content}
                  />
                ))}
              {noMoreComments ? (
                <IonButton
                  slot="end"
                  size="small"
                  color={"pig"}
                  onClick={() => showMoreComment(props.id)}
                >
                  No more comments
                </IonButton>
              ) : (
                <IonButton
                  slot="start"
                  size="small"
                  color={"light"}
                  onClick={() => showMoreComment(props.id)}
                >
                  Show more comments
                </IonButton>
              )}
              <IonButton
                slot="end"
                size="small"
                color={"pig"}
                onClick={() => setIsShowComment(!isShowComment)}
              >
                Close
              </IonButton>
            </IonList>
          ) : null}
        </IonCard>
      )}
    </>
  );
}
