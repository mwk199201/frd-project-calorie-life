import {
  IonContent,
  AccordionGroupCustomEvent,
  IonAccordion,
  IonAccordionGroup,
  IonItem,
  IonLabel,
  IonButton,
  useIonRouter,
  IonIcon,
  IonTitle,
  IonCard,
  IonCardTitle,
  IonCardContent,
  IonCardSubtitle,
  useIonAlert,
} from "@ionic/react";
import { closeCircle } from "ionicons/icons";
import moment from "moment";
import { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { APIOrigin, del } from "../../api";
import { useHasLogin } from "../../hooks/use-has-login";
import { logoutAction } from "../../redux/auth/action";
import { autoLoginThunk, logoutThunk } from "../../redux/auth/thunk";
import {
  updateBlockedBlogs,
  updateSavedBlogs,
} from "../../redux/blogList/action";
import { loadBlogListThunk } from "../../redux/blogList/thunk";
import { RootThunkDispatch } from "../../redux/dispatch";
import { profileNotReadyAction } from "../../redux/profile/action";
import { loadProfileThunk } from "../../redux/profile/thunk";
import { RootState } from "../../redux/state";
import { routes } from "../../Routes";
// import Logo from "./icon.png";
import styles from "./profile_compenont.module.css";

// type savedBlogList =
//   | { status: "loading" }
//   | {
//       status: "ready";
//       savedBlogItem: savedBlogItem[];
//     };

type savedBlogItem = {
  // status: "ready";
  blog_id: number;
  title: string;
  created_at: string;
};
type blockedBlogItem = {
  // status: "ready";
  blog_id: number;
  title: string;
  created_at: string;
};

type HistoryItem = {
  blog_id: number;
  title: string;
  created_at: string;
};

function ProfileItem() {
  // const [password, setPassword] = useState<string>();
  const listenerOut = useRef<null | HTMLParagraphElement>(null);
  const values = ["first", "second", "third"];
  const [savedBlogs, setSavedBlogs] = useState<savedBlogItem[]>([]);
  const [blockedBlogs, setBlockedBlogs] = useState<blockedBlogItem[]>([]);
  const [getSaved, setGetSave] = useState(false);
  const [BlogsHistory, setBlogHistory] = useState<HistoryItem[]>([]);
  const [getHistory, setHistory] = useState(false);

  // const blockedBlogs = useSelector((state: RootState) =>
  //   state.blogList.status === "ready"
  //     ? state.blogList.items?.filter((item) => item.has_block === "1")
  //     : null
  // );

  const accordionGroupChange = (ev: AccordionGroupCustomEvent) => {
    const nativeEl = listenerOut.current;
    if (!nativeEl) {
      return;
    }

    const collapsedItems = values.filter((value) => value !== ev.detail.value);
    const selectedValue = ev.detail.value;

    nativeEl.innerText = `
            Expanded: ${selectedValue === undefined ? "None" : ev.detail.value}
            Collapsed: ${collapsedItems.join(", ")}
          `;
  };
  const dispatch = useDispatch<RootThunkDispatch>();

  function logout() {
    dispatch(logoutAction());

    // router.push(routes.login);
  }
  let history = useHistory();
  const hasLogin = useHasLogin();
  const router = useIonRouter();
  if (!hasLogin) {
    router.push(routes.login);
  }

  const getSavedBlog = async () => {
    // setGetSave(true);
    try {
      const res = await fetch(`${APIOrigin}/blog/saved`, {
        method: "GET",
        headers: {
          "content-type": "application/json; charset=utf-8",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      const savedBlogs = await res.json();
      console.log(savedBlogs);

      return setSavedBlogs(savedBlogs);
    } catch (error) {
      return { error: String(error) };
    }
  };
  const getBlockedBlog = async () => {
    try {
      const res = await fetch(`${APIOrigin}/blog/blocked`, {
        method: "GET",
        headers: {
          "content-type": "application/json; charset=utf-8",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      const blockedBlogs = await res.json();
      console.log("blockedBlogs", blockedBlogs);

      return setBlockedBlogs(blockedBlogs);
    } catch (error) {
      return { error: String(error) };
    }
  };

  const getBlogHistory = async () => {
    // setGetSave(true);
    try {
      const res = await fetch(`${APIOrigin}/blog/history`, {
        headers: {
          "content-type": "application/json; charset=utf-8",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      const BlogsHistory = await res.json();
      console.log(`history is ${BlogsHistory}`);

      return setBlogHistory(BlogsHistory);
    } catch (error) {
      return { error: String(error) };
    }
  };

  const [presentAlert] = useIonAlert();
  const [handlerMessage, setHandlerMessage] = useState("");
  const [roleMessage, setRoleMessage] = useState("");
  function cancelSave(blog_id: number) {
    const fetchData = async () => {
      let unsave = await del(`/blog/${blog_id}/save`);
      console.log("unsave", unsave);
      setGetSave(false);
    };
    fetchData();
  }
  function cancelBlocked(blog_id: number) {
    const fetchData = async () => {
      let unBlock = await del(`/blog/${blog_id}/block`);
      console.log("unBlock", unBlock);
    };
    fetchData();
  }

  return (
    <IonAccordionGroup onIonChange={accordionGroupChange}>
      <IonAccordion
        value="first"
        onClick={() => {
          getBlogHistory();
        }}
      >
        <IonItem slot="header" color="light">
          <IonLabel>History</IonLabel>
        </IonItem>
        <div className="ion-padding" slot="content">
          {BlogsHistory.map((item: HistoryItem) => {
            return (
              <IonItem key={item.blog_id}>
                <IonLabel>
                  <IonLabel
                    className="ion-padding"
                    style={{ paddingTop: "2px", paddingBottom: "2px" }}
                  >
                    Title : {item.title}
                  </IonLabel>
                  <IonLabel
                    className="ion-padding"
                    style={{ paddingTop: "2px", paddingBottom: "2px" }}
                  >
                    Post at : {moment(item.created_at).fromNow()}
                  </IonLabel>
                </IonLabel>
              </IonItem>
            );
          })}
          {/* <div className={styles.border}>Blog title post at</div> */}
        </div>
      </IonAccordion>
      <IonAccordion
        value="saved"
        onClick={() => {
          getSavedBlog();
        }}
      >
        <IonItem slot="header" color="light">
          <IonLabel>Saved</IonLabel>
        </IonItem>

        <div className="ion-padding" slot="content">
          {savedBlogs.map((item: any) => {
            // console.log(item);
            return (
              <IonItem key={item.blog_id}>
                <IonButton
                  onClick={() =>
                    presentAlert({
                      header: "Confirm to cancel saved blog?",
                      buttons: [
                        {
                          text: "Cancel",
                          role: "cancel",
                          handler: () => {
                            setHandlerMessage("Alert canceled");
                          },
                        },
                        {
                          text: "OK",
                          role: "confirm",
                          handler: () => {
                            setHandlerMessage("Alert confirmed");
                            cancelSave(item.blog_id);
                            const newSavedBlogs = savedBlogs.filter(
                              (post) => post.blog_id !== item.blog_id
                            );
                            setSavedBlogs(newSavedBlogs);
                            dispatch(loadBlogListThunk());
                            dispatch(updateSavedBlogs(item.blog_id, "0"));
                          },
                        },
                      ],
                      onDidDismiss: (e: CustomEvent) =>
                        setRoleMessage(`Dismissed with role: ${e.detail.role}`),
                    })
                  }
                >
                  {" "}
                  <IonIcon icon={closeCircle}></IonIcon>
                </IonButton>

                <IonLabel>
                  <IonLabel
                    className="ion-padding"
                    style={{ paddingTop: "2px", paddingBottom: "2px" }}
                  >
                    Title : {item.title}
                  </IonLabel>
                  <IonLabel
                    className="ion-padding"
                    style={{ paddingTop: "2px", paddingBottom: "2px" }}
                  >
                    Saved at : {moment(item.created_at).fromNow()}
                  </IonLabel>
                </IonLabel>
              </IonItem>
            );
          })}
        </div>
      </IonAccordion>
      <IonAccordion
        value="blocked"
        onClick={() => {
          getBlockedBlog();
        }}
      >
        <IonItem slot="header" color="light">
          <IonLabel>Blocked</IonLabel>
        </IonItem>
        <div className="ion-padding" slot="content">
          {blockedBlogs?.map((item) => {
            return (
              <IonItem key={item.blog_id}>
                <IonButton
                  onClick={() =>
                    presentAlert({
                      header: "Confirm to cancel blocked blog?",
                      buttons: [
                        {
                          text: "Cancel",
                          role: "cancel",
                          handler: () => {
                            setHandlerMessage("Alert canceled");
                          },
                        },
                        {
                          text: "OK",
                          role: "confirm",
                          handler: () => {
                            setHandlerMessage("Alert confirmed");
                            cancelBlocked(item.blog_id);
                            const newBlockedBlogs = blockedBlogs.filter(
                              (post) => post.blog_id !== item.blog_id
                            );

                            setBlockedBlogs(newBlockedBlogs);
                            // dispatch(loadBlogListThunk());
                            dispatch(updateBlockedBlogs(item.blog_id, "0"));
                          },
                        },
                      ],
                      onDidDismiss: (e: CustomEvent) =>
                        setRoleMessage(`Dismissed with role: ${e.detail.role}`),
                    })
                  }
                >
                  {" "}
                  <IonIcon icon={closeCircle}></IonIcon>
                </IonButton>
                <IonLabel>
                  <IonLabel
                    className="ion-padding"
                    style={{ paddingTop: "2px", paddingBottom: "2px" }}
                  >
                    Title : {item.title}
                  </IonLabel>
                  <IonLabel
                    className="ion-padding"
                    style={{ paddingTop: "2px", paddingBottom: "2px" }}
                  >
                    Saved at : {moment(item.created_at).fromNow()}
                  </IonLabel>
                </IonLabel>
              </IonItem>
            );
          })}
        </div>
      </IonAccordion>
      <IonAccordion value="Setting">
        <IonItem slot="header" color="light">
          <IonLabel>Setting</IonLabel>
        </IonItem>
        <div className="ion-padding" slot="content">
          <IonItem
            className={styles.border}
            onClick={() => history.push(routes.setPersonalInfoPage)}
          >
            {" "}
            Personal Information
          </IonItem>
        </div>
        <div
          className="ion-padding"
          // style={{ paddingBottom: "20px" }}
          slot="content"
        >
          <IonButton
            onClick={() => {
              logout();
            }}
          >
            Logout
          </IonButton>
        </div>
      </IonAccordion>
    </IonAccordionGroup>
  );
}

export default ProfileItem;
