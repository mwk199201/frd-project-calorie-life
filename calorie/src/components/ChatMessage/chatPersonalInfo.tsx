import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent, IonListHeader, IonList, IonSearchbar, IonItem, IonAvatar, IonLabel, IonChip, IonIcon } from '@ionic/react';
import styles from 'chatPersonalInfo.module.css'
import moment from 'moment';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ChatListItem } from '../../redux/chatroom/state';
import { loadChatListThunk } from '../../redux/chatroom/thunk';
import { RootThunkDispatch } from '../../redux/dispatch';
import { RootState } from '../../redux/state';
import defaultIcon from "../../default-user-icon-13.jpeg";
import { APIOrigin } from '../../api';
import { useParams } from 'react-router';

type personaItem = {
    nickname: string;
    user_id: number;
    icon_image:string;
    user_role:string;
  };


const chatPersonalInfo = ()=>{
    const user_id = useSelector((state: RootState) =>
    state.auth.status === "authenticated" ? state.auth.user_id : null
  );
  const token = useSelector((state: RootState) =>
    state.auth.status === "authenticated" ? state.auth.token : null
  );
    const [messages, setMessages] = useState<personaItem[]>([]);
  const { REACT_APP_API_ORIGIN } = process.env;
  const [otherUser, setOtherUser] = useState<{
    nickname: string;
    user_id: number;
    icon_image:string
  }>();

  let params = useParams<{ room_id: string }>();
  const room_id = params.room_id;
  useEffect(() => {
    fetch(`${APIOrigin}/chatrooms/${room_id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((json) => {
        setMessages(json.messages);
        setOtherUser(json.other_user);
      });
  }, [room_id, token]);

    return(
        <IonPage>

        <IonHeader>
          <IonToolbar color="orange">
            <IonTitle>Personal Info</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
           { messages.map((item,idx)=>(
            <div key={idx}>
                 <div className={styles.userHeader}>
                   <div className={styles.userPhoto}>
                   {!item.icon_image
                    ? <img src={defaultIcon} />
                    : <img  src={`${REACT_APP_API_ORIGIN}/uploads/${item.icon_image}`}/>
                    }
                   </div>
                   <div className={styles.usernames}>
                       nickname:{item.nickname}
                   </div>
                   <br />
                   <IonChip color="primary" outline={true}>
                       <IonLabel>Role:{item.user_role}</IonLabel>
                   </IonChip>
                   <br />
               </div><div className={styles.line}></div>
               </div>
            ))}
       
        </IonContent>
      </IonPage>
    )
}

export default chatPersonalInfo;