import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonButton,
  IonBackButton,
  IonCard,
  IonCardContent,
  IonItem,
  IonFooter,
  IonIcon,
  IonInput,
  IonModal,
  IonButtons,
  IonThumbnail,
  IonActionSheet,
  IonLabel,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import styles from "./ChatMessage.module.css";
import { useParams } from "react-router";
import { useSocket } from "../../context/SocketContent";
import { APIOrigin } from "../../api";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/state";
import {
  caretForwardCircle,
  clipboardOutline,
  heart,
  imagesOutline,
  send,
  share,
  stopCircleOutline,
  trash,
  videocamOutline,
} from "ionicons/icons";
import { selectImage } from "@beenotung/tslib/file";
import { compressMobilePhoto, dataURItoBlob } from "@beenotung/tslib/image";
import moment from "moment";
import defaultIcon from "../../default-user-icon-13.jpeg";
import { json } from "express";

type ChatMessage = {
  id: number;
  message: string;
  image: string;
  sender_id: number;
  created_at: string;
};

type MessageDraft = {
  message: string;
  file: File;
  previewUrl: string;
};

const ChatMessage: React.FC = () => {
  const [camRoom, setCamRoom] = useState<string>(``);
  const [showActionSheet, setShowActionSheet] = useState(false);
  const [messages, setMessages] = useState<ChatMessage[]>([]);
  const [otherUser, setOtherUser] = useState<{
    nickname: string;
    user_id: number;
    icon_image: string;
  }>();
  const io = useSocket();
  const user_id = useSelector((state: RootState) =>
    state.auth.status === "authenticated" ? state.auth.user_id : null
  );
  const token = useSelector((state: RootState) =>
    state.auth.status === "authenticated" ? state.auth.token : null
  );
  const [text, setText] = useState<string>("");

  const [selectItemIdx, setSelectItemIdx] = useState<number>();

  let params = useParams<{ room_id: string }>();
  const room_id = params.room_id;
  const [draftMessages, setDraftMessages] = useState<MessageDraft[]>([]);
  const [activeDraftMessage, setActiveDraftMessage] =
    useState<MessageDraft | null>(null);
  const openScanner = async () => {
    let files = await selectImage({
      multiple: true,
    });
    let message = text;
    for (let file of files) {
      let previewUrl = await compressMobilePhoto({ image: file });
      let blob = dataURItoBlob(previewUrl);
      file = new File([blob], file.name, {
        lastModified: file.lastModified,
        type: blob.type,
      });
      let draftMessage = { message, file, previewUrl };
      setDraftMessages((messages) => [...messages, draftMessage]);
      setActiveDraftMessage((message) => message || draftMessage);
      message = "";
    }
  };

  const updateDraft = (draftMessage: MessageDraft) => {
    setActiveDraftMessage(draftMessage);
    setDraftMessages((messages) =>
      messages.map((message) =>
        message === activeDraftMessage ? draftMessage : message
      )
    );
  };

  const sendMessage = async () => {
    let formData = new FormData();
    if (draftMessages.length === 0) {
      formData.append("content", text);
    } else {
      draftMessages.forEach((message) => {
        formData.append("content", message.message);
        formData.append("image", message.file);
      });
    }

    const res = await fetch(`${APIOrigin}/chatrooms/${room_id}/message`, {
      method: "POST",
      body: formData,
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    const json = await res.json();

    clearPhoto();
    setText("");
  };

  const deleteMessage = async (message_id: number) => {
    const res = await fetch(`${APIOrigin}/chatrooms/${room_id}/${message_id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    const result = await res.json();
    if (result.ok) {
      let arr = messages.filter((v) => v.id != message_id);
      setMessages(arr);
    }

    // const newMessageList = messages.slice(0);
    // newMessageList.splice(message_id, 1);
    // setMessages(newMessageList);
    // setMessages(messages);
  };

  useEffect(() => {
    fetch(`${APIOrigin}/chatrooms/${room_id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((json) => {
        setMessages(json.messages);
        setOtherUser(json.other_user);
        // setMessages(messages);
        clearPhoto();
      });
  }, [room_id, token]);

  useEffect(() => {
    io.emit("join_room", room_id);
    function onMessage(message: ChatMessage[]) {
      setMessages((messages) => [...messages, ...message]);
    }
    function stopCamRoom() {
      setCamRoom("");
    }

    io.on("messages", onMessage);
    io.on("start_cam", setCamRoom);
    io.on("off_cam", stopCamRoom);
    return () => {
      io.emit("leave_room", room_id);
      io.emit("off_cam", { room_id, camRoom });
      io.off("messages", onMessage);
      io.off("start_cam", setCamRoom);
      io.off("off_cam", stopCamRoom);
    };
  }, [room_id, io]);

  const clearPhoto = () => {
    setDraftMessages([]);
    setActiveDraftMessage(null);
  };

  function openCam() {
    io.emit("open_cam", room_id);
  }

  function closeCam() {
    io.emit("off_cam", { room_id, camRoom });
  }
  // const personalPage =()=>{

  // }

  const { REACT_APP_API_ORIGIN } = process.env;

  return (
    <>
      <>
        <>
          <IonHeader>
            <IonToolbar color="orange">
              <IonButtons slot="start">
                {camRoom ? (
                  <IonButtons>
                    {" "}
                    <IonButton onClick={closeCam}>
                      <IonIcon icon={stopCircleOutline}></IonIcon>
                    </IonButton>
                  </IonButtons>
                ) : (
                  <IonBackButton defaultHref="/chatroom" />
                )}
              </IonButtons>
              <IonTitle>
                <div className={styles.left}>
                  {!otherUser?.icon_image ? (
                    <img src={defaultIcon} />
                  ) : (
                    <img
                      src={`${REACT_APP_API_ORIGIN}/uploads/${otherUser?.icon_image}`}
                    />
                  )}
                  {otherUser?.nickname}
                </div>
              </IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonContent fullscreen className={styles.background}>
            {messages.map((item, idx) => (
              <div key={idx}>
                <div
                  className={
                    item.sender_id == otherUser?.user_id
                      ? styles.allMessageContainer1
                      : styles.allMessageContainer
                  }
                >
                  <div
                    className={
                      item.sender_id == otherUser?.user_id
                        ? styles.messageContainer1
                        : styles.messageContainer
                    }
                  >
                    <div
                      onClick={() => {
                        setShowActionSheet(true);
                        setSelectItemIdx(item.id);
                      }}
                    >
                      <div
                        className={
                          item.sender_id == otherUser?.user_id
                            ? styles.name1
                            : styles.name
                        }
                      >
                        {item.sender_id == otherUser?.user_id
                          ? otherUser.nickname
                          : "me"}
                      </div>
                      <br />
                      <div className={styles.messagesend}>
                        {item.image && (
                          <img
                            src={`${REACT_APP_API_ORIGIN}/uploads/${item.image}`}
                          />
                        )}
                        {item.message && <div>{item.message}</div>}

                        <span
                          className={
                            item.sender_id == otherUser?.user_id
                              ? styles.time1
                              : styles.time
                          }
                        >
                          {moment(item.created_at).fromNow()}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </IonContent>

          {camRoom ? (
            <div className={styles.camContainer}>
              <iframe
                className={styles.cam}
                src={"https://meet.jit.si/" + camRoom}
              />
            </div>
          ) : null}
        </>
        <IonFooter hidden={!!camRoom}>
          <IonItem className={styles.bottom}>
            <IonInput
              type="text"
              placeholder="Enter a message"
              value={text!}
              onIonChange={(e) => setText(e.detail.value || "")}
            ></IonInput>
            <IonButtons slot="end">
              <IonButton onClick={openCam}>
                <IonIcon icon={videocamOutline} />
              </IonButton>
              <IonButton onClick={openScanner}>
                <IonIcon icon={imagesOutline} />
              </IonButton>
              <IonButton onClick={sendMessage}>
                <IonIcon icon={send}></IonIcon>
              </IonButton>
            </IonButtons>
          </IonItem>
        </IonFooter>
        <IonModal
          isOpen={draftMessages.length > 0}
          onDidDismiss={clearPhoto}
          backdropDismiss={false}
        >
          <IonHeader>
            <IonToolbar>
              <IonButtons slot="start">
                <IonButton onClick={clearPhoto}>Close</IonButton>
              </IonButtons>

              <IonTitle slot="end">Photo Message</IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonContent className="ion-padding">
            <div className="ion-text-center">
              <img src={activeDraftMessage?.previewUrl || ""} />
            </div>
          </IonContent>
          <IonFooter>
            <IonItem className={styles.bottom}>
              {activeDraftMessage ? (
                <IonInput
                  type="text"
                  placeholder="Enter a message"
                  value={activeDraftMessage?.message || ""}
                  onIonChange={(e) =>
                    updateDraft({
                      ...activeDraftMessage,
                      message: e.detail.value || "",
                    })
                  }
                ></IonInput>
              ) : null}

              <IonButtons slot="end">
                <IonButton></IonButton>
                <IonButton onClick={openScanner}>
                  <IonIcon icon={imagesOutline} />
                </IonButton>
                <IonButton onClick={sendMessage}>
                  <IonIcon icon={send}></IonIcon>
                </IonButton>
              </IonButtons>
            </IonItem>
            <div className={styles.thumbnailsContainer}>
              {draftMessages.map((message, i) => (
                <div
                  className={
                    message == activeDraftMessage
                      ? styles.thumbnailContainerActive
                      : styles.thumbnailContainer
                  }
                  key={i}
                  onClick={() => setActiveDraftMessage(message)}
                >
                  <IonThumbnail>
                    <img src={message.previewUrl} />
                  </IonThumbnail>
                </div>
              ))}
            </div>
          </IonFooter>
        </IonModal>
        <IonActionSheet
          isOpen={showActionSheet}
          onDidDismiss={() => setShowActionSheet(false)}
          cssClass="my-custom-class"
          buttons={[
            {
              text: "Delete",
              role: "destructive",
              icon: trash,
              handler: () => {
                if (selectItemIdx != null) {
                  deleteMessage(selectItemIdx);
                }
                // setMessages(json.messages);
              },
            },
          ]}
        ></IonActionSheet>
      </>
    </>
  );
};

export default ChatMessage;

// function newMessagehandler(text: string) {
//   if (!user_id || !otherUser) {
//     return;
//   }
//   const newMsgList = messages.slice(0);
//   newMsgList.push({
//     message: text,
//     sender_id: user_id,
//   });
//   setMessages(newMsgList);
// }

///method 1
{
  /* <div className={styles.background}>
          {messages.map((item, idx) => (
            <IonCard key={idx} >
              <IonCardContent
                onClick={() => {
                  setShowActionSheet(true);
                  setSelectItemIdx(idx);
                }}
              >
                <IonLabel slot="end">
                  {item.sender_id == otherUser?.user_id
                    ? otherUser.nickname
                    : "me"}
                </IonLabel>
                <br />
                <div>
                  {item.message && <div>{item.message}</div>}
                  {item.image && (
                    <img
                      src={`${REACT_APP_API_ORIGIN}/uploads/${item.image}`}
                    />
                  )}
                </div>

                <br />
                <span className={styles.time}>
                  {moment(item.created_at).fromNow()}
                </span>
              </IonCardContent>
            </IonCard>
          ))}
        </div> */
}

///method 3

{
  /* <div className={styles.background}>
          {messages.map((item, idx) => (
            
            <div key={idx}>
              {
                item.sender_id &&
                <div className={styles.allMessageContainer}>
                <div className={styles.messageContainer}>
                  <div
                    onClick={() => {
                      setShowActionSheet(true);
                      setSelectItemIdx(idx);
                    }}
                  >
                    <div className={styles.name}>
                      {item.sender_id == otherUser?.user_id
                        ? otherUser.nickname
                        : "me"}
                    
                    </div>
                    <br />
                    <div>
                      {item.image && (
                        <img
                          src={`${REACT_APP_API_ORIGIN}/uploads/${item.image}`}
                        />
                      )}
                      {item.message && <div className={styles.messagesend}>{item.message}</div>}
                          <span className={styles.time}>
                      {moment(item.created_at).fromNow()}
                    </span>
                    </div>
                  </div>
                </div>
              </div>
 } :{ otherUser?.user_id &&<div className={styles.allMessageContainer1}>
             <div className={styles.messageContainer1}>
               <div
                 onClick={() => {
                   setShowActionSheet(true);
                   setSelectItemIdx(idx);
                 }}
               >
                 <div className={styles.name1}>
                   {item.sender_id == otherUser?.user_id
                     ? otherUser.nickname
                     : "me"}
                     {/* {otherUser?.nickname} */
}
{
  /* </div>
                 <br />
                 <div>
                   {item.image && (
                     <img
                       src={`${REACT_APP_API_ORIGIN}/uploads/${item.image}`}
                     />
                   )}
                   {item.message && <div className={styles.messagesend}>{item.message}</div>}
                       <span className={styles.time1}>
                   {moment(item.created_at).fromNow()}
                 </span>
                 </div>
               </div>
             </div>
           </div>
             }
             </div>
            ))}
          </div>
               */
}
