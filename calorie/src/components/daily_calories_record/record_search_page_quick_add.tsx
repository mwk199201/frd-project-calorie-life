import { IonItem, IonList, useIonAlert } from "@ionic/react";
import { useEffect, useState } from "react";
import styles from "./record_search_page_quick_add.module.css";

export type QuickAddItem = {
  id: number;
  foodName: string;
  calories: number;
  carbohydrates: number;
  protein: number;
  fat: number;
};

function QuickAdd(prop: {
  selectedFood: (foodArray: QuickAddItem[]) => void;
  deleteFoodId: number | undefined;
}) {
  const [quickAddFoods, setQuickAddFoods] = useState<QuickAddItem[]>([]);
  const [descriptionFood, setDescriptionFood] = useState("Any Food");
  const [foodCalories, setFoodCalories] = useState(0);
  const [foodCarbohydrate, setFoodCarbohydrate] = useState(0);
  const [foodProtein, setFoodProtein] = useState(0);
  const [foodFat, setFoodFat] = useState(0);
  const [presentAlert] = useIonAlert();
  const [quickAddId, setQuickAddId] = useState(1);

  useEffect(() => {
    setQuickAddFoods(
      quickAddFoods.filter((food) => food.id != prop.deleteFoodId)
    );
  }, [prop.deleteFoodId]);

  function quickAddFoodEvent() {
    if (foodCalories == 0) {
      presentAlert({
        message: "Please enter food calories",
        buttons: ["OK"],
      });
      return;
    }
    let quickAddFood: QuickAddItem = {
      id: quickAddId,
      foodName: descriptionFood,
      calories: foodCalories,
      carbohydrates: foodCarbohydrate,
      protein: foodProtein,
      fat: foodFat,
    };
    quickAddFoods.push(quickAddFood);
    setQuickAddFoods(quickAddFoods);
    prop.selectedFood(quickAddFoods);
    // console.log(quickAddFoods)
    setQuickAddId(quickAddId + 1);
  }

  return (
    <div>
      <IonList>
        <IonItem className={styles.content}>
          <div style={{ flexGrow: 1 }}>Description</div>
          <input
            style={{ backgroundColor: "white" }}
            type="text"
            placeholder="ex. Today diet"
            onChange={(e) =>
              e.target.value
                ? setDescriptionFood(e.target.value)
                : setDescriptionFood("Any Food")
            }
          ></input>
        </IonItem>
        <IonItem className={styles.content}>
          <div style={{ flexGrow: 1 }}>Calories</div>
          <input
            style={{ backgroundColor: "white" }}
            type="number"
            placeholder="0 cal"
            onChange={(e) =>
              e.target.value
                ? setFoodCalories(+e.target.value)
                : setFoodCalories(0)
            }
          ></input>
        </IonItem>
        <IonItem className={styles.content}>
          <div style={{ flexGrow: 1 }}>Carbohydrates</div>
          <input
            type="number"
            style={{ width: "20vw", backgroundColor: "white" }}
            placeholder="0 g"
            onChange={(e) =>
              e.target.value
                ? setFoodCarbohydrate(+e.target.value)
                : setFoodCarbohydrate(0)
            }
          ></input>
        </IonItem>
        <IonItem className={styles.content}>
          <div style={{ flexGrow: 1 }}>Protein</div>
          <input
            style={{ backgroundColor: "white" }}
            type="number"
            placeholder="0 g"
            onChange={(e) =>
              e.target.value
                ? setFoodProtein(+e.target.value)
                : setFoodProtein(0)
            }
          ></input>
        </IonItem>
        <IonItem className={styles.content}>
          <div style={{ flexGrow: 1 }}>Fat</div>
          <input
            style={{ backgroundColor: "white" }}
            type="number"
            placeholder="0 g"
            onChange={(e) =>
              e.target.value ? setFoodFat(+e.target.value) : setFoodFat(0)
            }
          ></input>
        </IonItem>
      </IonList>
      <div>
        <button
          className={styles.addButton}
          onClick={() => quickAddFoodEvent()}
        >
          ADD
        </button>
      </div>
    </div>
  );
}

export default QuickAdd;
