import { IonItem, IonList, useIonAlert } from "@ionic/react";
import  { useEffect, useState } from "react";
import { Exercise } from "../../pages/record_page/exerciseSeachPage";
import styles from './record_search_page_quick_add.module.css'



function ExerciseQuickAdd(prop:{ quickAddList:((list:Exercise[]) =>void) ,deleteFoodId:number|undefined}){
    
    const [descriptionExercise,setDescriptionExercise]=useState('Any Exercise')
    const [exerciseCalories,setExerciseCalories] = useState(0)
    const [exerciseMins,setExerciseMins] = useState(0)
    const [presentAlert] = useIonAlert();
    const [quickAddId,setQuickAddId]=useState(1)
    const [quickList,setQuickList]=useState<Exercise[]>([])

    useEffect(() =>{
        setQuickList(quickList.filter((exercise)=>exercise.quickId!=prop.deleteFoodId))
    },[prop.deleteFoodId])


    function quickAddFoodEvent(){
        if(exerciseCalories===0){
            presentAlert({
                message: 'Please enter exercise calories',
                buttons: ['OK'],
            })
            return
        }
        let quickAddExercise:Exercise={
            quickId:quickAddId,
            description:descriptionExercise,
            calories:exerciseCalories,
            exercsieMins:exerciseMins
        }
        quickList.push(quickAddExercise)
        setQuickList(quickList)
        prop.quickAddList(quickList)
        setQuickAddId(quickAddId+1)
    }

    return(
        <div>
        <IonList>
        <IonItem className={styles.content}>
            <div style={{flexGrow:1}}>Description</div>
            <input type="text" placeholder="ex. Exercise Type" onChange={(e)=>e.target.value?setDescriptionExercise(e.target.value):setDescriptionExercise('Any Exercise')}></input>
        </IonItem>
        <IonItem className={styles.content}>
            <div style={{flexGrow:1}}>Calories</div>
            <input type="number" placeholder="0 cal" onChange={(e)=>e.target.value?setExerciseCalories(+e.target.value):setExerciseCalories(0)}></input>
        </IonItem>
        <IonItem className={styles.content}>
            <div style={{flexGrow:1}}>Exercise Time</div>
            <input type="number" placeholder="0 mins" onChange={(e)=>e.target.value?setExerciseMins(+e.target.value):setExerciseMins(0)}></input>
        </IonItem>
    
        </IonList>
        <div>
            <button className={styles.addButton } onClick={()=>quickAddFoodEvent()}>ADD</button>
        </div>
        </div>
    )
}

export default ExerciseQuickAdd