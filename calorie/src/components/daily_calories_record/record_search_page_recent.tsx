import { IonIcon, IonItem } from "@ionic/react";

import { caretForward } from "ionicons/icons";
import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { routes } from "../../Routes";
import { useGet } from "../../hooks/useGet";
import styles from "./record_search_page_recent.module.css";
import { useDispatch, useSelector } from "react-redux";
import { RootThunkDispatch } from "../../redux/dispatch";
import { loadFoodDataThunk } from "../../redux/calories_record/record_thunk";
import { RootState } from "../../redux/state";

export type FoodDataList = {
  calories: number;
  carbohydrate: number;
  cholesterol: number;
  created_at: string;
  dietary_fibre: number;
  food_chi_name: string;
  food_eng_name: string;
  food_type: string;
  food_weight: number;
  id: number;
  protein: number;
  saturated_fat: number;
  sodium: number;
  sugar: number;
  total_fat: number;
  trans_fat: number;
  updated_at: string;
  isselected: boolean;
};

function Recent(prop: {
  searchItem: string;
  add_diet: string;
  selectedFood: (foodArray: FoodDataList[]) => void;
  deleteFoodId: number | undefined;
  isPressing:boolean
}) {
  const dispatch = useDispatch<RootThunkDispatch>();
  let [selectedFoodArray, setSelectedFoodArray] = useState<FoodDataList[]>([]);
  let locateParams = useLocation();
  let newSearchItem: any[];
  let params;
  let paramsSearch: string | undefined = undefined;
  let paramsSearchFoodID: string | number;
  let paramsSearchFoodValue: number;
  let paramsSearchFoodCalories: number;

  let result =localStorage.getItem('searchInputValue')
// console.log(result)
  let { json, reload } = useGet<{
    foodData: FoodDataList[];
    error: string;
  }>("/foodData/" + result, {
    error: "",
    foodData: [],
  });
 
  // if(paramsSearch){
  //   json.foodData.map((food) =>{
  //     if (food.id==paramsSearchFoodID){
  //       food.food_weight=+food.food_weight*paramsSearchFoodValue
  //       food.calories=+food.calories*paramsSearchFoodValue
  //       console.log(food.food_weight)
  //     }
  //   })
  // }

 

  const foodData = useSelector((state: RootState) => state.calorieRecord.data);
 
  useEffect(() => {
    if(!json.error && json.foodData.length>0){
      setSelectedFoodArray(json.foodData);
      console.log(json)
    }
    else{
      setSelectedFoodArray([])
    }
    
  }, [json,prop.searchItem]);

  useEffect(() => {
  
      params = locateParams.pathname.replace("/tab/Record_search_page/", "");
      console.log(json)
      paramsSearch = locateParams.search;
      // console.log(paramsSearch);
      paramsSearchFoodID = locateParams.search.split("?")[1];
      paramsSearchFoodValue = +locateParams.search.split("?")[2];
      paramsSearchFoodCalories = +locateParams.search.split("?")[3];

    
      
      newSearchItem = json.foodData.slice();
      
      newSearchItem = newSearchItem.map((food) => {
        if (food.id === paramsSearchFoodID) {
          food.food_weight = paramsSearchFoodValue;
          // food.calories=+food.calories*(paramsSearchFoodValue/100)
          food.calories = paramsSearchFoodCalories.toFixed(2);
          food.isselected = true;
        }
        setSelectedFoodArray(newSearchItem);
      });
    
  }, [prop.searchItem]);

  useEffect(() => {
  
    newSearchItem = json.foodData.map((food) => food);
    newSearchItem = json.foodData
      .filter((food) => food.isselected)
      .map((food) => {
        const value = food.food_weight / 100;
        food.carbohydrate = +(food.carbohydrate * value).toFixed(2);
        food.cholesterol = +(food.cholesterol * value).toFixed(2);
        food.dietary_fibre = +(food.dietary_fibre * value).toFixed(2);
        food.protein = +(food.protein * value).toFixed(2);
        food.saturated_fat = +(food.saturated_fat * value).toFixed(2);
        food.sodium = +(food.sodium * value).toFixed(2);
        food.sugar = +(food.sugar * value).toFixed(2);
        food.total_fat = +(food.total_fat * value).toFixed(2);
        food.trans_fat = +(food.trans_fat * value).toFixed(2);
        return food;
      });
    prop.selectedFood(newSearchItem);
  }, [selectedFoodArray]);

  useEffect(() => {
    if (prop.deleteFoodId != undefined) {
      selectedFoodArray = selectedFoodArray.map((food) => {
        {
          if (food.id === prop.deleteFoodId) food.isselected = false;
        }
        return food;
      });
    }
    setSelectedFoodArray(selectedFoodArray);
  }, [prop.deleteFoodId]);

  // useEffect(() => {
  //   newSearchItem=json.foodData.slice()

  // },[selectedFoodArray])

  //  function returnJsonData(foodID:number,foodName:string,foodWeight:number,foodCalories:number){
  //     return (
  //         <Link to={routes.food_detail_page({id:foodID,add_diet:prop.add_diet})} className={styles.link}>
  //         <IonItem key={foodID}>
  //             <div className={styles.searchItem}>
  //                 <input type="checkBox"></input>

  //                 <div style={{marginLeft:'1rem'}}>
  //                     <div style={{maxWidth:'60vw'}}>{foodName}</div>
  //                     <div className={styles.foodWeight}>{+foodWeight}g</div>
  //                 </div>
  //                 <div className={styles.foodName}>
  //                     <div style={{marginRight:'1rem'}}>{+foodCalories}</div>
  //                     <IonIcon style={{fontSize: 'larger'}} icon={caretForward}></IonIcon>
  //                 </div>
  //             </div>
  //         </IonItem>
  //         </Link>
  //     )
  //  }
  function changeIsSelected(foodId: number) {
    newSearchItem = json.foodData.slice();
    newSearchItem = newSearchItem.map((food) => {
      if (food.id === foodId) {
        food.isselected = !food.isselected;
      }
      setSelectedFoodArray(newSearchItem);
      // console.log(newSearchItem)
      // prop.selectedFood(newSearchItem)
    });
  }

  return (
    <div>
      {json.error !== undefined ? (
        <p>{json.error}</p>
      ) : (
        <div>
          {selectedFoodArray.map((food) => (
            <IonItem key={food.id}>
              <input
                type="checkBox"
                checked={food.isselected}
                onChange={() => changeIsSelected(food.id)}
              ></input>
              <Link
                to={routes.food_detail_page({
                  id: food.id,
                  add_diet: prop.add_diet,
                })}
                className={styles.link}
              >
                {/* <IonItem key={food.id}> */}
                <div className={styles.searchItem}>
                  <div style={{ marginLeft: "1rem" }}>
                    <div style={{ maxWidth: "60vw" }}>{food.food_eng_name}</div>
                    <div className={styles.foodWeight}>{food.food_weight}g</div>
                  </div>
                  <div className={styles.foodName}>
                    <div style={{ marginRight: "1.5rem" }}>{food.calories}</div>
                    <IonIcon
                      style={{ fontSize: "larger" }}
                      icon={caretForward}
                    ></IonIcon>
                  </div>
                </div>
                {/* </IonItem> */}
              </Link>
            </IonItem>
          ))}
      
        </div>
        // !paramsSearch?<div>
        //   {json.foodData.map((food) => (returnJsonData(food.id,food.food_eng_name,food.food_weight,food.calories)))}
        // </div>:<div>{json.foodData.map((food) =>food.id==paramsSearchFoodID?returnJsonData(food.id,food.food_eng_name,(+food.food_weight*paramsSearchFoodValue),+food.calories*paramsSearchFoodValue):
        // returnJsonData(food.id,food.food_eng_name,food.food_weight,food.calories)
        // )}</div>
      )}
          {json.foodData.length===0 && prop.isPressing?
          <div>can not find your search , you may need to quick add your food data</div>:null}
    </div>
  );
}

export default Recent;
