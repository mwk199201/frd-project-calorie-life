import { IonAvatar, IonIcon, IonItem, IonItemOption, IonItemOptions, IonItemSliding, IonList, useIonAlert } from "@ionic/react";
import { add, happy, sad, trash } from "ionicons/icons";
import { Link } from "react-router-dom";
import { routes } from "../../Routes";
import styles from "./AddDietButton.module.css";
import {
  ExerciseList,
  FoodDataList,
  QuickAddItem,
} from "../../redux/calories_record/record_state";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { RootThunkDispatch } from "../../redux/dispatch";
import { deleteExerciseItem, deleteQuickItem, deleteRecentItem } from "../../redux/calories_record/record_thunk";


export type AddDiet = {
  id: number;
  dietType: string;
  dietImg: string;
  dietList: FoodDataList[];
  quickdietList: QuickAddItem[];
  exerciseList?: ExerciseList[];
  recommendCalories: number;
  recentDietTotalCal: number;
  quickDietTotalCal: number;
};

// type DailyDiet={
//   id:number;
//   user_id:number;
//   diet_type:string;
//   food_name:string;
//   food_type :string;
//   calories   :number;
//   protein :number;
//   carbohydrate :number;
//   total_fat :number;
//   sugar:number;
//   saturated_fat:number;
//   trans_fat:number;
//   cholesterol:number;
//   dietary:number;
//   sodium:number;
//   created_at:string
//   updated_at:string
// }

function AddDietButton(prop: { diet: AddDiet; className: string ;deleteItem:((id:number)=>void); date:string }) {
  let today = new Date().toISOString().split('T')[0]
  const dispatch = useDispatch<RootThunkDispatch>();
  const add_diet = prop.diet.dietType;
  const [isPressing,setIsPressing] = useState(false)
  const [presentAlert] = useIonAlert();
  const [deleteItemTitles, setDeleteItemTitles] = useState(
    {
      title:'food_name',
      calories:0,
      foodId:0,
      type:'recent or quick'
  }); 
  // const [roleMessage, setRoleMessage] = useState('');

  // const [breakfastCal,setBreakfastCal]=useState(0)
  // const [lunchCal,setLunchCal]=useState(0)
  // const [dinnerCal,setDinnerCal]=useState(0)
  // const [snackCal,setSnackCal]=useState(0)

  // useEffect(() => {dispatch(loadCalorieRecordThunk())},[])
  // dispatch(loadCalorieRecordThunk())
  // const dietList=useSelector((state:RootState)=>state.calorieRecord.items)

  // useEffect(() => {
  //   if(prop.diet.dietType==='Add Breakfast'){
  //     prop.diet.dietList.map((diet)=>{breakfastCal+diet.calories})
  //     // set
  //   }
  //   if(prop.diet.dietType==='Add Lunch'){
  //     setLunchList(prop.diet.dietList)
  //   }
  //   if(prop.diet.dietType==='Add Dinner'){
  //     setDinnerList(prop.diet.dietList)
  //   }
  //   if(prop.diet.dietType==='Add Snack'){
  //     setSnackList(prop.diet.dietList)
  //   }

  // },[prop.diet.dietList])

  useEffect(()=>{
    if(isPressing){
      if(today===prop.date){
     
        presentAlert({
          header: 'Do you want to delete this item?',
          subHeader:`${deleteItemTitles.title}(${deleteItemTitles.calories})Cal`,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => { setIsPressing(false); }
            },
            {
              text: 'OK',
              role: 'confirm',
              handler: () => {prop.deleteItem(deleteItemTitles.foodId)
                if(deleteItemTitles.type==='recent'){
                  dispatch(deleteRecentItem(deleteItemTitles.foodId))
                }
                if(deleteItemTitles.type==='quick'){
                dispatch(deleteQuickItem(deleteItemTitles.foodId))
                }
                if(deleteItemTitles.type==='exercise'){
                dispatch(deleteExerciseItem(deleteItemTitles.foodId))
                }
                setIsPressing(false)
               }
            }
          ],
          // onDidDismiss: (e: CustomEvent) => setRoleMessage(`Dismissed with role: ${e.detail.role}`)
        })

   
    }
  }
  },[isPressing, deleteItemTitles.calories,deleteItemTitles.foodId,deleteItemTitles.title, deleteItemTitles.type, dispatch, presentAlert,prop])

  function pressing(title:string, calories:number, foodId:number , type:string){
    setDeleteItemTitles({title,calories,foodId,type})
    setIsPressing(true)
  }

  let recommendCondition =
    prop.diet.dietList.length > 0 || prop.diet.quickdietList.length > 0 ? (
      <div className={styles.recommend}>
        {prop.diet.recentDietTotalCal + prop.diet.quickDietTotalCal} Cal
      </div>
    ) : (
      <div className={styles.recommend}>
        Recommended:{prop.diet.recommendCalories}
        cal
      </div>
    );

  let addBtn =
      prop.diet.dietType!=='Exercise'?
        prop.diet.dietList.length > 0 || prop.diet.quickdietList.length > 0 ? (
      <div>
        {prop.diet.recommendCalories -
          (prop.diet.recentDietTotalCal + prop.diet.quickDietTotalCal) >
        0 ? (
          <div>
            <IonIcon icon={happy} className={styles.happyIcon}></IonIcon>
            <div style={{ marginBottom: "0.5rem", fontSize: "medium" }}>
              {prop.diet.recommendCalories -
                (prop.diet.recentDietTotalCal +
                  prop.diet.quickDietTotalCal)}{" "}
              Cal
              <span style={{ color: "lightGreen" }}> UNDER</span>
            </div>
          </div>
        ) : (
          <div>
            <IonIcon icon={sad} className={styles.happyIcon}></IonIcon>
            <div style={{ marginBottom: "0.5rem", fontSize: "medium" }}>
              {prop.diet.recommendCalories -
                (prop.diet.recentDietTotalCal +
                  prop.diet.quickDietTotalCal)}{" "}
              Cal
              <span style={{ color: "red" }}> OVER</span>
            </div>
          </div>
        )}
      </div>
    ) : (
      <div>
        {today===prop.date?
        <Link
          to={routes.record_search_page({ add_diet: add_diet })}
          style={{ backgroundColor: "white" }}
        >
          <IonIcon className={styles.icon} icon={add} />
        </Link>:null}
      </div>
    ): <div>
      {today===prop.date?
    <Link
      to={routes.exerciseSearchPage}
      style={{ backgroundColor: "white" }}
    >
      <IonIcon className={styles.icon} icon={add} />
    </Link>:null
}
  </div>



  return (
    // <button>
    
    <IonList
      className={prop.className}
      style={
        prop.diet.dietList.length > 0
          ? { marginBottom: "1rem" }
          : { marginBottom: "null" }
      }
      class="ion-no-padding"
    >
      <button className={styles.addList}>
        
        <IonItem key={prop.diet.id} className={styles.container}>
          <div>
            <IonAvatar
              className={
                prop.diet.dietList.length > 0
                  ? styles.dietImg
                  : styles.normalDietImg
              }
            >
              <img alt={prop.diet.dietType} src={prop.diet.dietImg}></img>
            </IonAvatar>
          </div>
          <div className={styles.title}>
            <div>{prop.diet.dietType}</div>
            {/* <div className={styles.recommend}>
              Recommended:{prop.diet.recommendCalories}
              cal
            </div> */}
            {recommendCondition}
          </div>
          {/* <div>
            <Link to={routes.record_search_page({add_diet:add_diet})}  style={{backgroundColor:"white"}} >
            <IonIcon  className={styles.icon} icon={add} />
            </Link>
          </div> */}
          {addBtn}
        </IonItem>
      </button>
      <button className={styles.selectedFoodContainer} >
        {prop.diet.dietList.map((item) => (
          <div  key={item.id+'recent'}  style={{textDecoration:'none'}}>
           <IonItemSliding>

          <IonItem
          routerLink={routes.recentFoodDetailPage({id:item.id,date:prop.date})}

           key={item.id+'recent'} 
           className={styles.selectedFood} 
          //  onTouchStart={()=>pressing(item.food_name,item.calories,item.id,'recent')} 
          //  onTouchEnd={()=>setIsPressing(false)}
   
           >
            <div className={styles.selectedFoodName}>
              <div>{item.food_name}</div>
              <div className={styles.selectedFoodAmounting}>
                {item.food_weight}
                {"g"}
              </div>
            </div>
            <div>{item.calories} Cal</div>
          </IonItem>

          <IonItemOptions side="end">
          < IonItemOption className={styles.side} onClick={() => pressing(item.food_name,item.calories,item.id,'recent')}>
            <IonIcon icon={trash}></IonIcon>
          </IonItemOption>
            </IonItemOptions>
        </IonItemSliding>  
          </div>
        ))}
        {prop.diet.quickdietList.map((item,i) => (

          <div key={i+item.id+'quick'}  style={{textDecoration:'none'}}>
            <IonItemSliding>
          <IonItem
          routerLink={routes.quickFoodDetailPage({id:item.id,date:prop.date})} 
          key={i+item.id+'quick'} className={styles.selectedFood} 
          // onTouchStart={()=>pressing(item.food_name,item.calories,item.id,
          // 'quick')} onTouchEnd={()=>setIsPressing(false)}
          >
            <div className={styles.selectedFoodName}>
              <div>{item.food_name}</div>
 
            </div>
            <div className={styles.selectedFoodCal}>{item.calories} Cal</div>
          </IonItem>

          <IonItemOptions side="end">
          < IonItemOption className={styles.side} onClick={() => pressing(item.food_name,item.calories,item.id,'quick')}>
            <IonIcon icon={trash}></IonIcon>
          </IonItemOption>
            </IonItemOptions>
        </IonItemSliding>  
           </div>
        ))}
        {prop.diet.exerciseList!==undefined &&prop.diet.exerciseList.length>0? prop.diet.exerciseList.map((item,i) => (
          <div key={i+item.id+'exercise'}  style={{textDecoration:'none'}}>
          <IonItemSliding>
          <IonItem
          // routerLink={routes.quickFoodDetailPage({id:item.id})} 
          key={i+item.id+'exercise'} className={styles.selectedFood}
          //  onTouchStart={()=>pressing(item.exercise_type,
          //  item.exercise_calories,item.id,'exercise')} onTouchEnd={()=>setIsPressing(false)}
           >
            <div className={styles.selectedFoodName}>
              <div>{item.exercise_type}</div>
 
            </div>
            <div className={styles.selectedFoodCal}>{item.exercise_calories} Cal</div>
          </IonItem>
          <IonItemOptions side="end">
          < IonItemOption className={styles.side} onClick={() => pressing(item.exercise_type,item.exercise_calories,item.id,'exercise')}>
            <IonIcon icon={trash}></IonIcon>
          </IonItemOption>
            </IonItemOptions>
        </IonItemSliding>  
           </div>
        )):null}
      </button>
      
      {prop.diet.dietList.length > 0 || prop.diet.quickdietList.length > 0 ?
      today===prop.date? (
        <Link
          to={routes.record_search_page({ add_diet: add_diet })}
          style={{ textDecoration: "none" }}
        >
          <IonItem key="add Food">
            <button className={styles.addFoodButton}>Add food</button>
          </IonItem>
        </Link>
      ) : null:null}
      {/* {prop.diet.exerciseList?prop.diet.exerciseList.length > 0 ? prop.diet.exerciseList.map((item,i)=>{
         <Link key={i+item.id+'quick'} to={routes.quickFoodDetailPage({id:item.id})} style={{textDecoration:'none'}}>
         <IonItem key={i+item.id+'exercise'} className={styles.selectedFood} onTouchStart={()=>pressing(item.exercise_type,item.exercise_calories,item.id,'exercise')} onTouchEnd={()=>setIsPressing(false)}>
           <div className={styles.selectedFoodName}>
             <div>{item.exercise_type}</div>
           </div>
           <div className={styles.selectedFoodCal}>{item.exercise_calories} Cal</div>
         </IonItem>
          </Link>
      }):null:null} */}
      
    </IonList>
    // </button>
  );
}

export default AddDietButton;

