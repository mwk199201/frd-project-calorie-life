import { IonIcon, IonItem } from "@ionic/react";

import { caretForward } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useGet } from "../../hooks/useGet";
import styles from "./record_search_page_recent.module.css";
import {  RecentExercise } from "../../pages/record_page/exerciseSeachPage";



function ExerciseRecent(prop:{searchItem: string ,selectedItem:((item:RecentExercise[])=>void), deleteFoodId: number | undefined, isPressing:boolean}) {
  let [selectedExerciseArray, setSelectedExerciseArray] = useState<RecentExercise[]>([]);
  let [userWeights, setUserWeights] = useState(70)
  let newSearchItem: any[];
    
  const { json, reload } = useGet<{
    exerciseData: RecentExercise[];
    error: string;
  }>("/ExerciseData/" + prop.searchItem, {
    error: "",
    exerciseData: [],
  });
   

//   console.log(json.exerciseData[0].id)
  useEffect(() => {
    setSelectedExerciseArray(json.exerciseData);
  }, [json]);

 useEffect(()=>{
    newSearchItem = json.exerciseData.map((exercise) => exercise);
    newSearchItem = json.exerciseData
    .filter((exercise) => exercise.isselected)
    prop.selectedItem(newSearchItem);
 },[selectedExerciseArray])

  function changeIsSelected(exerciseId: number) {
    newSearchItem = json.exerciseData.slice();
    newSearchItem = newSearchItem.map((exercise) => {
      if (exercise.id === exerciseId) {
        exercise.isselected = !exercise.isselected;
      }
      setSelectedExerciseArray(newSearchItem);
    });
  }

  useEffect(() => {
    if (prop.deleteFoodId != undefined) {
        selectedExerciseArray = selectedExerciseArray.map((exercise) => {
        {
          if (exercise.id === prop.deleteFoodId) exercise.isselected = false;
        }
        return exercise;
      });
    }
    setSelectedExerciseArray(selectedExerciseArray);
  }, [prop.deleteFoodId]);
  

  return (
    <div>
    {json.error !== undefined ? (
      <p>{json.error}</p>
    ) : (
      <div>
        {selectedExerciseArray.map((exercise) => (
          <IonItem key={exercise.id}>
            <input
              type="checkBox"
              checked={exercise.isselected}
              onChange={() => changeIsSelected(exercise.id)}
            ></input>
            <div           
              className={styles.link}
            >
              {/* <IonItem key={food.id}> */}
              <div className={styles.searchItem}>
                <div style={{ marginLeft: "1rem" }}>
                  <div style={{ maxWidth: "60vw" }}>{exercise.exercise_type}</div>
                  <div className={styles.foodWeight}>{exercise.exercise_mins} mins</div>
                </div>
                <div className={styles.foodName}>
                  <div style={{ marginRight: "1.5rem" }}>{(userWeights*(exercise.exercise_mins/60)*exercise.exercise_METs).toFixed(1)} Cal</div>
                  <IonIcon
                    style={{ fontSize: "larger" }}
                    icon={caretForward}
                  ></IonIcon>
                </div>
              </div>
              {/* </IonItem> */}
            </div>
          </IonItem>
        ))}
      </div>

  )}
   {json.exerciseData.length===0 && prop.isPressing?
          <div>can not find your search , you may need to quick add your exercise data</div>:null}
   </div>
  )
}

export default ExerciseRecent;
