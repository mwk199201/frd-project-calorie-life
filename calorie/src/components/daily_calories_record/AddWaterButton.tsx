import { IonItem } from "@ionic/react";
import  { useEffect, useState } from "react";
import styles from "./AddWaterButton.module.css";
import pushWater from "./Glass-push-water.svg";
import water from "./Glass-water.svg";
import noWater from "./Glass-no-water.svg";
import { useDispatch } from "react-redux";
import { RootThunkDispatch } from "../../redux/dispatch";
import { addWaterThunk } from "../../redux/calories_record/record_thunk";

function AddWaterButton(prop: { date: string; cupOfWater: number }) {
  const dispatch = useDispatch<RootThunkDispatch>();
  let today = new Date().toISOString().split("T")[0];
  const [drunkCup, setDrunkCup] = useState(prop.cupOfWater);
  const displayCup = drunkCup >= 11 ? 11 : drunkCup < 8 ? 8 : drunkCup + 1;
  let cupNodes =prop.date===today? new Array(displayCup).fill(0).map((_, i) => (
  
    
    <img key={i} alt='water'
      onClick={() => {
          if (i + 1 <= drunkCup) {
            setDrunkCup(i);
            dispatch(addWaterThunk(1,i,prop.date))
          } else {
            setDrunkCup(i + 1);
            dispatch(addWaterThunk(1,i+1,prop.date))
          }
      }}
      title={i + "," + drunkCup}
      
      src={ i + 1 <= drunkCup ? water : i === drunkCup ? pushWater : noWater}
    ></img>
  )):
  new Array(displayCup).fill(0).map((item, i) => (
  
    <img alt="avatar" key={i}
     
      title={i + "," + drunkCup}
      
      src={ i + 1 <= drunkCup ? water : noWater}
    ></img>
  ))

  useEffect(() => {
    setDrunkCup(prop.cupOfWater);
  }, [prop.cupOfWater]);

  // useEffect(() => {
  //   setDrunkCup(prop.cupOfWater)
  // },[prop.cupOfWater])
  // countWater[0]=<img src={pushWater}></img>
  // const [cupOfWater,setCupOfWater] =useState(countWater)
  // function addWater(i:number){
  //     let newCupOfWater=cupOfWater.slice()
  //     if(newCupOfWater[i].props.src=='/static/media/Glass-push-water.886e9d2511893c752ad14f6399fd0eef.svg'
  //     ){
  //         newCupOfWater[i]= <img src={water}></img>
  //         if(i<10){
  //             newCupOfWater[i+1]= <img src={pushWater}></img>
  //         }
  //         setCup(cup+1)
  //         setCupOfWater(newCupOfWater)
  //     }
  //     else if(newCupOfWater[i].props.src=='/static/media/Glass-std.2e2f051026758835b9e82a371e7f9f57.svg'){
  //         newCupOfWater[i]= <img src={pushWater}></img>
  //         for(let x=0;x<newCupOfWater.length ;x++){
  //                     if(x>8){
  //                         newCupOfWater=newCupOfWater.slice(0,7)
  //                         setCupOfWater(newCupOfWater)
  //                     }
  //                     if(x>i){
  //                         newCupOfWater[x]= <img src={noWater}></img>
  //                     }
  //                 setCupOfWater(newCupOfWater)
  //                 setCup(i)
  //             }
  //     }else {
  //         return
  //     }
  //        }
  return (
    <div className={styles.container}>
      <IonItem key="water">
        <div className={styles.flex}>
          <div className={styles.showWater}>Water</div>
          <div className={styles.showCup}> {drunkCup} cups</div>
        </div>
      </IonItem>
      <IonItem key="cup ">
        <div className={styles.showCupOfWater}>{cupNodes}</div>
      </IonItem>
      {/* {cupOfWater.map((item,i)=>
            <button onClick={()=>addWater(i)}>{item}</button>
)} */}
    </div>
  );
}
export default AddWaterButton;
