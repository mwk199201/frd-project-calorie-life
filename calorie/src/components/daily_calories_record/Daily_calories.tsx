import { IonAvatar, IonIcon } from "@ionic/react";
import { caretBack, caretForward } from "ionicons/icons";
import React, { useEffect, useState } from "react";
import styles from "./Daily_calories.module.css";
import icon from "./people.svg";
import { Dialog } from "@reach/dialog";
import "@reach/dialog/styles.css";
import "react-calendar/dist/Calendar.css";
import { Link } from "react-router-dom";
import { routes } from "../../Routes";
import { ExerciseList } from "../../redux/calories_record/record_state";
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';




export type DailyRecordCalculate = {
  budget: number;
  consumed: number;
  exercise: number;
  over: number;
};

function Daily_calories_record(prop: {
  className: string;
  recentRecordCalculate: number;
  quickRecordCalculate: number;
  dayValue: (date: string) => void;
  userBudget:number;
  exercise:ExerciseList[]
}) {
  const [value, onChange] = useState(new Date());
  const userId = 1;
  const [car,setCar] =useState("200") ;
  const rootStyle = document.documentElement.style;
  rootStyle.setProperty("--car", car);
  const [day, setDay] = useState(new Date().getDate());
  // const [dayValue,setDayValue]=useState('')
  const [showDialog, setShowDialog] = React.useState(false);
  const open = () => setShowDialog(true);
  const close = () => setShowDialog(false);
  // const [value, onChange] = useState(new Date());
  const [recentConsumed, setRecentConsumed] = useState<number>(0);
  const [quickConsumed, setQuickConsumed] = useState<number>(0);
  const [dailyCaloriesValues, setDailyCaloriesValues] = useState<number>(prop.userBudget);
  const [exerciseCalories,setExerciseCalories] = useState(prop.exercise.reduce((acc,item)=>acc+item.exercise_calories,0))
 


  var today = new Date();
  today.setDate(day);
  // var nextday = new Date().getDate();
  // const date=new Date(today.getFullYear(), today.getMonth()+1, 0).getDate();
  let date = today.toDateString();

//   let dayResult=localStorage.getItem('dayValue');

// console.log(dayResult?.split(' ')[2])

//   useEffect(()=>{
//     if(dayResult){
//       let day = dayResult.split(' ')[2]
//       setDay(+day)
//     }
//   },[])

  // useEffect(() => {
  //   localStorage.setItem('dayValue',date)

  // },[day])
  
  useEffect(() => {
    // setDayValue(today.toISOString().split('T')[0])
    prop.dayValue(today.toISOString().split("T")[0]);
    // console.log(today)
  }, [today,prop]);

  useEffect(() => {
    // let newArray=[prop.recordCalculate].slice()
    let calories=prop.exercise.reduce((acc,item)=>acc+item.exercise_calories,0)
    setRecentConsumed(prop.recentRecordCalculate);
    setDailyCaloriesValues(prop.userBudget - (prop.recentRecordCalculate + prop.quickRecordCalculate)+calories);
  }, [prop.recentRecordCalculate,prop.quickRecordCalculate,prop.userBudget,prop.exercise]);

  useEffect(() => {
    // let newArray=[prop.recordCalculate].slice()
    let calories=prop.exercise.reduce((acc,item)=>acc+item.exercise_calories,0)
    setQuickConsumed(prop.quickRecordCalculate);
    setDailyCaloriesValues(prop.userBudget - (prop.recentRecordCalculate + prop.quickRecordCalculate)+calories);
  }, [prop.quickRecordCalculate,prop.recentRecordCalculate,prop.userBudget,prop.exercise]);

  useEffect(() => {
    setDailyCaloriesValues(prop.userBudget-recentConsumed-quickConsumed+exerciseCalories)
    // setDailyCaloriesValues(dailyCaloriesValues - (recentConsumed + quickConsumed))
  },[prop.userBudget,quickConsumed,recentConsumed,exerciseCalories])


  useEffect(() => {
    if(dailyCaloriesValues>prop.userBudget){
      setCar('0')
    }else if(dailyCaloriesValues>0){
      setCar(((1-(dailyCaloriesValues+exerciseCalories)/prop.userBudget)*472).toString())
    }else{
      setCar('472')
    }
    
  },[dailyCaloriesValues,prop.userBudget,prop.exercise,exerciseCalories])

  useEffect(() => {
    let calories=prop.exercise.reduce((acc,item)=>acc+item.exercise_calories,0)
    setDailyCaloriesValues(prop.userBudget- (prop.recentRecordCalculate + prop.quickRecordCalculate)+calories) 
    setQuickConsumed(prop.quickRecordCalculate);
    setRecentConsumed(prop.recentRecordCalculate);
    
  },[day,prop.quickRecordCalculate, prop.recentRecordCalculate,prop.userBudget,prop.exercise])

  useEffect(() => { 
    if(prop.exercise!==undefined && prop.exercise.length > 0) {
      let calories=prop.exercise.reduce((acc,item)=>acc+item.exercise_calories,0)
      // console.log({calories})
      if(calories>0){
        setExerciseCalories(prop.exercise.reduce((acc,item)=>acc+item.exercise_calories,0))
      }
      
    }else{
      setExerciseCalories(0)
    }
  },[prop.exercise])

  function preDay() {
    setDay(day - 1);
    // today.setDate(day)
    // date=today.toDateString()
  }
  function nextDay() {
    setDay(day + 1);
  }
  
  // let calories=prop.exercise.reduce((acc,item)=>acc+item.exercise_calories,0)
  // console.log({calories})
  return (
    // <ion-grid>
    <div className={styles.container}>
      <div>
        <IonAvatar>
          <img alt='icon' src={icon}></img>
        </IonAvatar>
      </div>

      <div className={styles.date}>
        <button onClick={preDay}>
          <IonIcon className={styles.chevron} icon={caretBack} />
        </button>

        <button onClick={open}>
          
          {date}
        </button>
        <Dialog isOpen={showDialog} onDismiss={close} aria-label="canledar">
          <button className="close-button" onClick={close}>
            {/* <VisuallyHidden>Close</VisuallyHidden> */}
            <span aria-hidden>×</span>
          </button>
          {/* <Calendar onChange={onChange} value={value} /> */}
        </Dialog>

        <button>
          <IonIcon
            onClick={nextDay}
            className={styles.chevron}
            icon={caretForward}
            hidden={date === new Date().toDateString()}
          />
        </button>
      </div>

      <div className={styles.calories_bar}>
        <div className={styles.outer}>
          <div className={styles.inner}>
            <div className={styles.calories_box}>
              <span id={styles.calories}>{dailyCaloriesValues}</span>
              <div>
                {/* <IonButton>more</IonButton> */}
                <Link
                  to={routes.record_more_detail_page({
                    id: userId,
                    date: today.toISOString().split("T")[0],
                  })}
                  className={styles.moreDetailButton}
                >
                  <button>more</button>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          width="160px"
          height="160px"
        >
          <defs>
            <linearGradient id="GradientColor">
              <stop offset="0%" stopColor="#e91e63" />
              <stop offset="100%" stopColor="#673ab7" />
            </linearGradient>
          </defs>
          <circle cx="80" cy="80" r="70" strokeLinecap="round" />
        </svg>
      </div>
      <div className={styles.calculate}>
        <div className={styles.recordBox}>
          {dailyCaloriesValues  > 0 ? (
            <div>{dailyCaloriesValues }</div>
          ) : (
            <div>0</div>
          )}
          <div className={styles.word}>budget</div>
        </div>
        <div className={styles.recordBox}>
          <div>{recentConsumed + quickConsumed}</div>
          <div className={styles.word}>consumed</div>
        </div>
        <div className={styles.recordBox}>
            {exerciseCalories}
          <div className={styles.word}>exercise</div>
        </div>
        <div className={styles.recordBox}>
          {dailyCaloriesValues< 0 ? (
            <div>{ recentConsumed + quickConsumed-prop.userBudget}</div>
          ) : (
            <div>0</div>
          )}
          <div className={styles.word}>over</div>
        </div>
      </div>
    </div>
    // </ion-grid>
  );
}

export default Daily_calories_record;
