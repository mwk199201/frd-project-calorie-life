import { connect } from 'socket.io-client'
// import { storages } from './redux/auth/storage'


const { REACT_APP_API_ORIGIN } = process.env

if (!REACT_APP_API_ORIGIN) {
  let message = 'Missing environment variable REACT_APP_API_ORIGIN'
  document.body.innerText = message
  throw new Error(message)
}

export const APIOrigin: string = REACT_APP_API_ORIGIN

export async function del(url: string) {
  try {

    const res = await fetch(`${APIOrigin}${url}`, {
      method: "DELETE",
      headers: {
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      }

    });
    const result = await res.json();
    return result
  } catch (error) {
    return { error: String(error) }
  }
}

export async function post(url: string, body?: any) {
  try {

    const res = await fetch(`${APIOrigin}${url}`, {

      method: "POST",
      headers: {
        "Authorization": `Bearer ${localStorage.getItem('token')}`,
        "content-type": "application/json; charset=utf-8",
      },
      ...(body && { body: JSON.stringify(body) })
    });
    const result = await res.json();
    return result
  } catch (error) {
    return { error: String(error) }
  }
}

export async function get(url: string) {
  try {
    const res = await fetch(`${APIOrigin}${url}`, {
      method: "GET",
      headers: {
        "Authorization": `Bearer ${localStorage.getItem('token')}`,

      },
    })
    const result = await res.json()
    return result
  } catch (error) {
    return { error: String(error) }
  }
}

export function connectSocketIO() {
  return connect(APIOrigin)
}

