import {
  IonTabs,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
} from "@ionic/react";
import { flame, book, chatbubbles, settings } from "ionicons/icons";
import { Route, Redirect } from "react-router-dom";
import ChatMessage from "./components/ChatMessage/ChatMessage";
import TabButtonContent from "./components/TabButtonContent";
import { useHasLogin } from "./hooks/use-has-login";
import BlogListPage from "./pages/BlogListPage";
import ChatList from "./pages/ChatRoom/ChatList";
// import LoginPage from "./pages/LoginPage";

import SettingPage from "./pages/profile/SettingPage";
import RecordPage_moreDetailPage from "./pages/record_page/daily_record_more_detailPage";
import FoodDetailPage from "./pages/record_page/food_detail_page";
import Daily_record_page from "./pages/record_page/RecordPage";
import Record_search_page from "./pages/record_page/record_search_page";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
// import NotFoundPage from "./pages/NotFoundPage";
// import { useSuccessRegister } from "./hooks/use-success-register";
import PersonalInfo from "./PersonalInfo";
import QuickDetailPage from "./pages/record_page/quickAddDetailPage";
import RecentAddDetailPage from "./pages/record_page/recentAddDetailPage";
import ExerciseSearchPage from "./pages/record_page/exerciseSeachPage";
import { useEffect } from "react";
// import { get } from "./api";
import { useHasProfile } from "./hooks/use-has-profile";
import { loadProfileThunk } from "./redux/profile/thunk";
import { useDispatch } from "react-redux";
import { RootThunkDispatch } from "./redux/dispatch";
import SetPersonalInfoPage from "./pages/SetPersonalInfoPage";
import NotFoundPage from "./pages/NotFoundPage";

export const routes = {
  record_search_page: (options: { add_diet: string }) =>
    `/tab/Record_search_page/${options.add_diet}`,
  record_more_detail_page: (options: { id: string | number; date: string }) =>
    `/tab/Record_more_detail_page/${options.id}/${options.date}`,
  food_detail_page: (options: { id: string | number; add_diet: string }) =>
    `/tab/Food_detail_page/${options.add_diet}/${options.id}/`,
  quickFoodDetailPage: (options: { id: string | number; date: string }) =>
    `/tab/Quick_food_detail_page/${options.id}/${options.date}`,
  recentFoodDetailPage: (options: { id: string | number; date: string }) =>
    `/tab/Recent_food_detail_page/${options.id}/${options.date}`,

  exerciseSearchPage: "/tab/ExerciseSearchPage",
  recordSearch: "/Record_search_page/:add_diet",
  recordDetail: "/Record_more_detail_page/:id",
  login: "/login",
  // profile: "/profile",
  register: "/register",
  /*tab*/
  blog: "/tab/BlogListPage",
  record: "/tab/Daily_record_page",
  chatroom: "/tab/chatroom",
  profile: "/tab/profile",
  foodDetail: "/Food_detail_page/:id",
  chatMessage: "/chatroom/:room_id",
  newMessage: "/chatroom/user/:user_id",
  personalInfo: "/personalInfo/:user_id",
  setPersonalInfoPage: "/tab/SetPersonalInfoPage",
};

function ProtectedRoute(props: {
  exact?: boolean;
  path: string;
  component: React.FC;
}) {
  const dispatch = useDispatch<RootThunkDispatch>();
  useEffect(() => {
    dispatch(loadProfileThunk());
  }, []);
  const hasLogin = useHasLogin();
  const hasProfile = useHasProfile();

  const component = !hasLogin
    ? LoginPage
    : !hasProfile
    ? PersonalInfo
    : props.component;

  return (
    <Route exact={props.exact} path={props.path} component={component}></Route>
  );
}

export const tabButtons = [
  {
    tab: "Record",
    path: "/tab/Daily_record_page",
    color: "medium",
    currentColor: "danger",
    content: "Record",
    icon: flame,
  },
  {
    tab: "BlogList",
    path: "/tab/BlogListPage",
    color: "medium",
    currentColor: "orange",
    content: "Blog",
    icon: book,
  },
  // {
  //   tab: "tab3",
  //   href: "/tab/tab3",
  //   color: "medium",
  //   currentColor: "success",
  //   content: "Camera",
  //   icon: camera,
  // },
  {
    tab: "chatroom",
    path: "/tab/chatroom",
    color: "medium",
    currentColor: "primary",
    content: "Chatting",
    icon: chatbubbles,
  },
  {
    tab: "tab5",
    path: "/tab/profile",
    color: "medium",
    currentColor: "dark",
    content: "Settings",
    icon: settings,
  },
];

export default function Routes() {
  return (
    <>
      <Route path="/">
        <Redirect to={routes.record} />
      </Route>
      <Route path={routes.register} component={RegisterPage} />
      <Route path={routes.login} component={LoginPage} />
      {/* <ProtectedRoute path={routes.chatMessage} component={ChatMessage} /> */}
      <ProtectedRoute path={routes.profile} component={SettingPage} />

      <ProtectedRoute path={routes.personalInfo} component={PersonalInfo} />
      <Route path="/tab">
        <IonTabs>
          <IonRouterOutlet>
            <ProtectedRoute
              path={routes.record}
              component={Daily_record_page}
            />
            <ProtectedRoute path={routes.blog} component={BlogListPage} />
            <ProtectedRoute path={routes.chatroom} component={ChatList} />
            {/* <ProtectedRoute path={routes.chatMessage} component={ChatMessage} />
            <ProtectedRoute path={routes.newMessage} component={ChatMessage} /> */}
            <Route path={routes.profile} component={SettingPage} />
            <ProtectedRoute
              path={routes.setPersonalInfoPage}
              component={SetPersonalInfoPage}
            />
          </IonRouterOutlet>

          <IonTabBar
            className="tabBar"
            color="--ion-color-light-rgba"
            slot="bottom"
          >
            {tabButtons.map((button, i) => {
              return (
                <IonTabButton href={button.path} key={i} tab={button.tab}>
                  <TabButtonContent
                    path={button.path}
                    key={button.path}
                    tab={button.tab}
                    color={button.color}
                    icon={button.icon}
                    content={button.content}
                  />
                </IonTabButton>
              );
            })}
          </IonTabBar>
        </IonTabs>

        <ProtectedRoute
          path="/tab/Food_detail_page/:id"
          component={FoodDetailPage}
        />

        <ProtectedRoute
          component={Record_search_page}
          path={
            "/tab/Record_search_page/:add_diet"
          } /*path={routes.record_search_page({add_diet:add_diet})}*/
        />

        <ProtectedRoute
          path="/tab/Record_more_detail_page/:id"
          component={RecordPage_moreDetailPage}
        />

        <ProtectedRoute
          path="/tab/Quick_food_detail_page/:id"
          component={QuickDetailPage}
        />

        <ProtectedRoute
          path="/tab/Recent_food_detail_page/:id"
          component={RecentAddDetailPage}
        />

        <ProtectedRoute
          path="/tab/ExerciseSearchPage"
          component={ExerciseSearchPage}
        />
      </Route>
      {/* <Route component={NotFoundPage} /> */}

      <ProtectedRoute path={routes.chatMessage} component={ChatMessage} />
      <ProtectedRoute path={routes.newMessage} component={ChatMessage} />
    </>
  );
}
