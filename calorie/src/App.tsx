// @ts-nocheck
// import { Redirect, Route } from "react-router-dom";
import { IonApp, IonRouterOutlet, setupIonicReact } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
// import { book, camera, chatbubbles, flame, settings } from "ionicons/icons";
// import Daily_record_page from "./pages/record_page/RecordPage";
// import Tab3 from "./pages/Tab3";
import "./App.css";
/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";
// import { useLocation } from "react-router-dom";
/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
// import ChatRoom from "./pages/ChatRoom";
// import Profile from "./pages/profile/ProfilePage";

// import BlogListPage from "./pages/BlogListPage";
// import Record_search_page from "./pages/record_page/record_search_page";
// import RecordPage_moreDetailPage from "./pages/record_page/daily_record_more_detailPage";
// import ChatMessage from "./components/ChatMessage/ChatMessage";
// import ChatList from "./pages/ChatRoom/ChatList";
// import FoodDetailPage from "./pages/record_page/food_detail_page";
// import { useEffect } from "react";
// import { useSocket } from "./context/SocketContent";
// import TabButtonContent from "./components/TabButtonContent";
// import { useHasLogin } from "./hooks/use-has-login";
import Routes from "./Routes";

setupIonicReact();

const App: React.FC = () => {
  return (
    <IonApp className="body">
      <IonReactRouter>
        <Routes />
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
