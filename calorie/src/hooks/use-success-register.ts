import { useSelector } from "react-redux";
import { RootState } from "../redux/state";

export function useSuccessRegister() {
    const useSuccessRegister: boolean = useSelector(
        (state: RootState) => state.auth.status === 'authenticated'
    )
    return useSuccessRegister
}