import { useSelector } from "react-redux";
import { RootState } from "../redux/state";

export function useHasLogin() {
    const hasLogin: boolean = useSelector(
        (state: RootState) => state.auth.status === 'authenticated'
    )
    return hasLogin
}