import { useEffect, useMemo, useState } from 'react'
import { APIOrigin } from '../api'

// import { useStorageState } from 'react-use-storage-state'

export function useGet<T>(url: string, initialState: T) {
  const [json, setJSON] = useState(initialState)
//   const [json, setJSON] = useStorageState(url, initialState)

  const reload = useMemo(() => {
    function reload() {
      fetch(APIOrigin + url)
        .then(res => res.json())
        .catch(error => ({ error: String(error) }))
        .then(json => {
          if (!json.error) {
            setJSON(json)
          } else {
            console.error('failed to GET, url:', url, 'error:', json.error)
          }
        })
    }
    return reload
  }, [url])

  // const reload = useMemo(
  //   () => () =>
  //     fetch(API_ORIGIN + url)
  //       .then(res => res.json())
  //       .catch(error => ({ error: String(error) }))
  //       .then(setJSON),
  //   [url],
  // )

  useEffect(reload, [reload])

  return { json, reload }
}

