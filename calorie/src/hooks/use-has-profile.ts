import { useSelector } from "react-redux";
import { RootState } from "../redux/state";

export function useHasProfile() {
    const hasProfile: boolean = useSelector(
        (state: RootState) => !!state.profile.nickname
    )

    // const hasProfile: boolean = !!localStorage.getItem("profile")
    return hasProfile



}