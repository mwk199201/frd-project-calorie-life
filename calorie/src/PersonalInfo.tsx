import {
  IonButton,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  IonProgressBar,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { useState } from "react";

import { post } from "./api";
import { useIonRouter } from "@ionic/react";
import styles from "./PersonalInfo.module.css";
import { routes } from "./Routes";
import { useDispatch } from "react-redux";

import { RootThunkDispatch } from "./redux/dispatch";
import { loadProfileThunk } from "./redux/profile/thunk";

type FormState = {
  nickname: string | null;
  height: number | null;
  weight: number | null;
  age: number | null;
  gender: string;
  activity: string;
  aim: string;
};

const PersonalInfo: React.FC = () => {
  const [state, setState] = useState<FormState>({
    nickname: "",
    height: null,
    weight: null,
    age: null,
    gender: "",
    activity: "",
    aim: "",
  });

  // const [aim, setAim]=useState('')

  function patchState<K extends keyof FormState>(
    field: K,
    value: FormState[K]
  ) {
    setState({ ...state, [field]: value });
  }

  const [step, setStep] = useState(1);
  const MaxStep = 4;
  function nextStep() {
    if (step < MaxStep) {
      setStep(step + 1);
    }
  }
  function prevStep() {
    if (step !== 1) {
      setStep(step - 1);
    }
  }
  const router = useIonRouter();
  // const history = useHistory();
  // const profile = useSelector(selectProfile);
  const dispatch = useDispatch<RootThunkDispatch>();

  function submitFrom() {
    const submit = async () => {
      try {
        const result = await post("/profile", state);
        if (result.success) {
          dispatch(loadProfileThunk());

          router.push(routes.record);
        }
      } catch (error) {
        return { error: String(error) };
      }
    };
    submit();
  }

  const steps: Record<number, JSX.Element> = {
    1: (
      <>
        <h1 className={styles.text}>Goal</h1>

        <IonItem>
          <IonLabel position="floating">What is your Goal?</IonLabel>
          {/* <IonInput
      type="text"
      value={state.aim}
      onIonChange={(e) => patchState("aim", e.detail.value || "")}
    ></IonInput> */}
          <IonSelect
            onIonChange={(e) => {
              patchState("aim", e.detail.value! || null);
            }}
          >
            <IonSelectOption value="KeepFit">Keep fit</IonSelectOption>
            <IonSelectOption value="BuildMuscle">Build muscle</IonSelectOption>
            <IonSelectOption value="LoseWeight">Lose Weight</IonSelectOption>
          </IonSelect>
        </IonItem>
        <div className="ion-text-center d-flex">
          <IonButton className="grow ion-margin" onClick={nextStep}>
            Next
          </IonButton>
        </div>
      </>
    ),
    2: (
      <>
        <h1 className={styles.text}>Your 's Info</h1>
        <IonItem>
          <IonLabel position="floating">What is your nickname?</IonLabel>
          <IonInput
            type="text"
            value={state.nickname}
            placeholder="nickname"
            onIonChange={(e) => patchState("nickname", e.detail.value! || "")}
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel position="floating">What is your height?</IonLabel>
          <IonInput
            type="number"
            value={state.height}
            placeholder="...cm"
            onIonChange={(e) => patchState("height", +e.detail.value! || null)}
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel position="floating">What is your weight?</IonLabel>
          <IonInput
            type="number"
            value={state.weight}
            placeholder="...kg"
            onIonChange={(e) => patchState("weight", +e.detail.value! || null)}
          ></IonInput>
        </IonItem>
        <div className="ion-text-center d-flex">
          <IonButton className="grow ion-margin" onClick={prevStep}>
            Prev
          </IonButton>
          <IonButton className="grow ion-margin" onClick={nextStep}>
            Next
          </IonButton>
        </div>
      </>
    ),
    3: (
      <>
        <h1 className={styles.text}>Age and Gender</h1>
        <IonItem>
          <IonLabel position="floating">What is your Age?</IonLabel>
          <IonInput
            type="number"
            value={state.age}
            onIonChange={(e) => patchState("age", +e.detail.value! || null)}
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel>Gender</IonLabel>
          <IonSelect
            onIonChange={(e) => {
              patchState("gender", e.detail.value! || null);
            }}
          >
            <IonSelectOption value="male">Male</IonSelectOption>
            <IonSelectOption value="female">Female</IonSelectOption>
          </IonSelect>
        </IonItem>
        <div className="ion-text-center d-flex">
          <IonButton className="grow ion-margin" onClick={prevStep}>
            Prev
          </IonButton>
          <IonButton className="grow ion-margin" onClick={nextStep}>
            Next
          </IonButton>
        </div>
      </>
    ),
    4: (
      <>
        <h1 className={styles.text}>Activtiy</h1>
        <IonItem>
          <IonLabel position="floating">What is your activity?</IonLabel>
          <IonSelect
            onIonChange={(e) => {
              patchState("activity", e.detail.value! || null);
            }}
          >
            <IonSelectOption value="1">little or no exercise</IonSelectOption>
            <IonSelectOption value="2">sports 1-3 days/week</IonSelectOption>
            <IonSelectOption value="3">sports 3-5 days/week</IonSelectOption>
            <IonSelectOption value="4">sports 6-7 days a week</IonSelectOption>
            <IonSelectOption value="5">
              sports & physical job or 2x training
            </IonSelectOption>
          </IonSelect>
          {/* <IonInput
            type="text"
            value={state.Activity}
            onIonChange={(e) => patchState("Activity", e.detail.value || "")}
          ></IonInput> */}
        </IonItem>
        <div className="ion-text-center d-flex">
          <IonButton className="grow ion-margin" onClick={prevStep}>
            Prev
          </IonButton>
          <IonButton
            expand="block"
            onClick={() => {
              submitFrom();
            }}
          >
            Submit
          </IonButton>
        </div>
      </>
    ),
  };

  return (
    <IonPage className="centerForm">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Personal info</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className={styles.color}>
        <IonProgressBar value={step / MaxStep}></IonProgressBar>
        {/* <pre>
          <code>{JSON.stringify({ state, step }, null, 2)}</code>
        </pre> */}

        {steps[step]}
      </IonContent>
    </IonPage>
  );
};

export default PersonalInfo;
