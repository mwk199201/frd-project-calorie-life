import {
  IonAlert,
  IonBackButton,
  IonButton,
  IonButtons,
  IonCard,
  IonCardHeader,
  // IonCardTitle,
  IonContent,
  // IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  // IonList,
  IonPage,
  // IonTitle,
  IonToolbar,
  useIonAlert,
  // useIonPicker,
  useIonRouter,
} from "@ionic/react";
// import { push } from "ionicons/icons";

import { useState } from "react";
// import { useDispatch } from "react-redux";
// import { Redirect } from "react-router";
import { post } from "../api";

import { routes } from "../Routes";

import styles from "./LoginPage.module.css";

const RegisterPage: React.FC = () => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [rePassword, setRePassword] = useState<string>("");
  const [showAlert3, setShowAlert3] = useState(false);
  const [successRegister, setSuccessRegister] = useState(false);

  const [presentAlert] = useIonAlert();

  function validateInput(error?: string) {
    if (error) {
      presentAlert({
        header: "Alert",
        subHeader: "Important message",
        message: `${error}`,
        buttons: ["OK"],
      });
      return false;
    }
    if (!username) {
      presentAlert({
        header: "Alert",
        subHeader: "Important message",
        message: "Please input username!",
        buttons: ["OK"],
      });
      return false;
    }
    if (!password) {
      presentAlert({
        header: "Alert",
        subHeader: "Important message",
        message: "Please input password!",
        buttons: ["OK"],
      });
      return false;
    }
    if (password !== rePassword) {
      presentAlert({
        header: "Alert",
        subHeader: "Important message",
        message: "Not match password!",
        buttons: ["OK"],
      });
      return false;
    }
    return true;
  }

  function handleRegister() {
    const register = async () => {
      const result = await post("/register/password", { username, password });
      console.log("register", result);
      if (result.register) {
        setShowAlert3(true);
      }
      if (result.error) {
        validateInput(result.error);
      }
    };
    register();
  }

  const router = useIonRouter();
  if (successRegister) {
    router.push(routes.login);
  }

  return (
    <IonPage className={styles.page}>
      <IonContent fullscreen>
        <div className={styles.waveContainer}>
          <div className={styles.wave}></div>
        </div>{" "}
        <IonToolbar color="nothing">
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.login}></IonBackButton>
          </IonButtons>
        </IonToolbar>
        <IonCard>
          <IonCardHeader className={styles.title}>Register</IonCardHeader>
          <IonItem className="ion-padding">
            <IonLabel>Username : </IonLabel>
            <IonInput
              value={username}
              placeholder="username"
              onIonChange={(e) => setUsername(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem className="ion-padding">
            <IonLabel>Password : </IonLabel>
            <IonInput
              value={password}
              type="password"
              placeholder="password"
              onIonChange={(e) => setPassword(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem className="ion-padding">
            <IonLabel>Re-password : </IonLabel>
            <IonInput
              value={rePassword}
              type="password"
              placeholder="re-password"
              onIonChange={(e) => setRePassword(e.detail.value!)}
            ></IonInput>
          </IonItem>
        </IonCard>
        <IonButton
          className="ion-padding"
          expand="block"
          onClick={() => {
            validateInput() && handleRegister();
          }}
        >
          Register
        </IonButton>
        <IonAlert
          isOpen={showAlert3}
          onDidDismiss={() => setShowAlert3(false)}
          cssClass="my-custom-class"
          header={"Confirm!"}
          message={"Congratulation!!! Your account is already"}
          buttons={[
            {
              text: "Cancel",
              role: "cancel",
              cssClass: "secondary",
              handler: (blah) => {
                console.log("Confirm Cancel: blah");
              },
            },
            {
              text: "Okay",
              handler: () => {
                setSuccessRegister(true);
              },
            },
          ]}
        />
      </IonContent>
    </IonPage>
  );
};

export default RegisterPage;
