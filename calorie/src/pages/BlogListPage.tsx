import {
  IonAvatar,
  IonButton,
  IonButtons,
  IonCard,
  IonContent,
  IonFab,
  IonFabButton,
  IonGrid,
  IonHeader,
  IonIcon,
  IonImg,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonModal,
  IonNote,
  IonPage,
  IonRow,
  IonSearchbar,
  IonTextarea,
  IonTitle,
  IonToolbar,
  useIonActionSheet,
  useIonAlert,
  useIonRouter,
} from "@ionic/react";
// import { Camera, CameraResultType } from "@capacitor/camera";
import { add, camera } from "ionicons/icons";
import { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import BlogListItem from "../components/blog_components/BlogListItem";
import { selectBlogList, selectBlogStatus } from "../redux/blogList/reducer";
import { BlogItem as BlogItemType, RootState } from "../redux/state";
import styles from "./BlogListPage.module.css";
import { selectImage } from "@beenotung/tslib/file";
import { compressMobilePhoto, dataURItoBlob } from "@beenotung/tslib/image";
import { useDispatch } from "react-redux";
import { RootThunkDispatch } from "../redux/dispatch";
import {
  loadBlogListBySearchThunk,
  loadBlogListThunk,
} from "../redux/blogList/thunk";
import { APIOrigin, get, post } from "../api";
import { OverlayEventDetail } from "@ionic/react/dist/types/components/react-component-lib/interfaces";
// import { useHasLogin } from "../hooks/use-has-login";
// import { AuthState } from "../redux/auth/state";
// import { UpdateAvatarResult } from "../api.types";
import defaultIcon from "../default-user-icon-13.jpeg";
import { routes } from "../Routes";
import { selectProfile } from "../redux/profile/reducer";

export type Profile = {
  nickname: string;
  height: number;
  weight: number;
  age: number;
  activity: string;
  aim: string;
  icon_image?: string;
  user_role: string;
};
const BlogListPage: React.FC = () => {
  const blogItems = useSelector(selectBlogList);
  //// for take token
  // let token;
  // const auth: AuthState = useSelector((state: RootState) => state.auth);
  // if (auth.status === "authenticated") token = auth.token;
  // console.log(token);
  ////
  const [searchText, setSearchText] = useState("");
  const [text, setText] = useState<string>(``);
  const [titleText, setTitleText] = useState<string>("");

  const [newAvatarFile, setNewAvatarFile] = useState<File | null>(null);
  const [previewUrl, setPreviewUrl] = useState<string | null>(null);

  const blogStatus = useSelector(selectBlogStatus);
  const dispatch = useDispatch<RootThunkDispatch>();

  const modal = useRef<HTMLIonModalElement>(null);
  const input = useRef<HTMLIonInputElement>(null);

  const [message, setMessage] = useState("");

  function confirm() {
    modal.current?.dismiss(input.current?.value, "confirm");
  }
  const router = useIonRouter();

  function onWillDismiss(ev: CustomEvent<OverlayEventDetail>) {
    if (ev.detail.role === "confirm") {
      setMessage(`Hello, ${ev.detail.data}!`);
      dispatch(loadBlogListThunk());
      setMessage("");
      setTitleText("");
      setText("");
      setNewAvatarFile(null);
    }
  }
  async function uploadImage(
    titleText: string,
    text: string,
    newAvatarFile?: File
  ) {
    confirm();
    // console.log(text, newAvatarFile);
    if (validateInput() === false) {
      return;
    }
    if (newAvatarFile) {
      let formData = new FormData();
      formData.append("title", titleText);
      formData.append("content", text);
      formData.append("image", newAvatarFile);
      const res = await fetch(`${APIOrigin}/blog`, {
        method: "POST",
        body: formData,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      const result = await res.json();
      // console.log("post with image", result);
    } else {
      let formData = new FormData();
      formData.append("title", titleText);
      formData.append("content", text);
      const res = await fetch(`${APIOrigin}/blog`, {
        method: "POST",
        body: formData,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      const result = await res.json();
      // console.log("post without image", result);
    }

    // if (uploadResult.error) {
    //   return;
    // }

    setNewAvatarFile(null);
  }

  useEffect(() => {
    // dispatch(loadBlogListThunk());
    let body = {
      value: "",
    };
    dispatch(loadBlogListBySearchThunk(body));
  }, [message]);

  const openScanner = async () => {
    // const data = await BarcodeScanner.scan();
    // console.log(`Barcode data: ${data.text}`);
    let files = await selectImage({
      multiple: false,
      // capture: true // to force using camera
    });
    let file = files[0];
    if (!file) {
      return;
    }
    let previewUrl = await compressMobilePhoto({ image: file });
    let blob = dataURItoBlob(previewUrl);
    file = new File([blob], file.name, {
      lastModified: file.lastModified,
      type: blob.type,
    });
    setNewAvatarFile(file);
    setPreviewUrl(previewUrl);
  };

  const [icon_image, setIconImage] = useState<Profile>();
  const [nickname, setNickname] = useState("");
  useEffect(() => {
    getProfile();
  }, []);
  const getProfile = async () => {
    const profile = await get("/profile");
    setNickname(profile[0].nickname);
    setIconImage(profile[0].icon_image);
  };
  const { REACT_APP_API_ORIGIN } = process.env;

  let icon = `${REACT_APP_API_ORIGIN}/uploads/` + icon_image;

  function searchFor(value: string) {
    setSearchText(value);

    let body = {
      value: value,
    };
    dispatch(loadBlogListBySearchThunk(body));
  }
  const [presentAlert] = useIonAlert();

  function validateInput() {
    if (!titleText) {
      presentAlert({
        header: "Alert",
        subHeader: "Important message",
        message: "Please input Title!",
        buttons: ["OK"],
      });
      return false;
    }
    if (!text) {
      presentAlert({
        header: "Alert",
        subHeader: "Important message",
        message: "Please input Content!",
        buttons: ["OK"],
      });
      return false;
    }

    return true;
  }
  return (
    <IonPage className={styles.blogPage}>
      <IonHeader>
        <IonToolbar color="orange">
          <IonTitle>Social</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent color="--ion-background-color" fullscreen>
        <IonToolbar className={styles.searchBar}>
          {/* <IonButtons slot="primary">
            <IonButton onClick={() => {}}>
              <IonIcon slot="icon-only" icon={search} />
            </IonButton>
          </IonButtons> */}
          <IonSearchbar
            value={searchText}
            onIonChange={(e) => searchFor(e.detail.value!)}
            placeholder="Search for title..."
          ></IonSearchbar>
        </IonToolbar>
        <IonList className={styles.list}>
          {blogStatus === "loading" && <div>Loading...</div>}
          {blogItems?.map(
            ({
              id,
              title,
              content,
              poster_id,
              icon_image,
              nickname,
              like_count,
              updated_at,
              has_like,
              has_save,
              has_block,
              has_report,
              image,
            }: // comments,
            BlogItemType) => {
              // console.log(image);
              return (
                <BlogListItem
                  key={id}
                  content={content}
                  poster_id={poster_id}
                  id={id}
                  icon_image={icon_image}
                  image={image}
                  title={title}
                  like_count={like_count}
                  updated_at={updated_at}
                  has_like={has_like}
                  has_save={has_save}
                  has_block={has_block}
                  has_report={has_report}
                  nickname={nickname}
                  commentListStatus={"loading"} // comments={comments}
                />
              );
            }
          )}
        </IonList>
      </IonContent>
      {/* <button id="open-modal" color="danger" className={styles.postButton}>
        <IonIcon className={styles.postIcon} icon={addCircleOutline} />
      </button> */}
      <IonFab
        vertical="bottom"
        style={{ paddingBottom: "60px" }}
        horizontal="end"
        slot="fixed"
      >
        <IonFabButton color="danger">
          <IonIcon icon={add} id="open-modal" />
        </IonFabButton>
      </IonFab>
      <IonModal
        ref={modal}
        trigger="open-modal"
        onWillDismiss={(ev) => onWillDismiss(ev)}
      >
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonButton onClick={() => modal.current?.dismiss()}>
                Cancel
              </IonButton>
            </IonButtons>
            <IonTitle>Welcome</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding" id="post-form">
          <IonCard>
            <IonGrid>
              <IonRow className="ion-padding">
                <IonAvatar>
                  {icon_image ? (
                    <IonImg src={icon} />
                  ) : (
                    <IonImg src={defaultIcon} />
                  )}
                </IonAvatar>
                <IonLabel className="ion-padding">{nickname}</IonLabel>
              </IonRow>
              <IonItem>
                {/* <IonButton> */}
                <IonIcon
                  color="success"
                  icon={camera}
                  onClick={openScanner}
                ></IonIcon>
                {/* </IonButton> */}
              </IonItem>
              <IonItem>
                <IonInput
                  value={titleText}
                  placeholder="Enter title here..."
                  onIonChange={(e) => setTitleText(e.detail.value!)}
                ></IonInput>
              </IonItem>
              <IonItem>
                <IonTextarea
                  id="uploadContent"
                  auto-grow={true}
                  placeholder="Enter your story here..."
                  value={text}
                  onIonChange={
                    (e) => setText(e.detail.value!)
                    // console.log(e.detail.value!.split("\n"))
                  }
                ></IonTextarea>
              </IonItem>
            </IonGrid>
            {previewUrl ? (
              <img
                id="uploadImage"
                itemType="file"
                className="avatar"
                src={previewUrl}
                alt="avatar of this user account"
              ></img>
            ) : (
              <IonNote className="ion-padding">you may upload photo</IonNote>
            )}
          </IonCard>
          <IonButton
            strong={true}
            expand="block"
            type="submit"
            onClick={
              newAvatarFile
                ? () => uploadImage(titleText, text, newAvatarFile)
                : () => uploadImage(titleText, text)
            }
          >
            Confirm
          </IonButton>
        </IonContent>
      </IonModal>
    </IonPage>
  );
};

export default BlogListPage;
