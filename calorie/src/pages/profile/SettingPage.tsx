import {
  IonAccordion,
  IonButton,
  IonCard,
  IonChip,
  IonContent,
  IonHeader,
  IonIcon,
  IonLabel,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import { personAddOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { APIOrigin, get } from "../../api";
import { Redirect, useHistory, useParams } from "react-router";
import ProfileItem from "../../components/profile_compenont/ProfileItem";
import { AuthState } from "../../redux/auth/state";

import { RootState } from "../../redux/state";
// import Logo from "./icon.png";
import styles from "./SettingPage.module.css";
// import { json } from "express";
import { selectImage } from "@beenotung/tslib/file";
import { compressMobilePhoto, dataURItoBlob } from "@beenotung/tslib/image";
import defaultIcon from "../../default-user-icon-13.jpeg";
// import { Profile } from "../../redux/profile/state";
import { RootThunkDispatch } from "../../redux/dispatch";
import { loadProfileThunk } from "../../redux/profile/thunk";
import { selectProfile } from "../../redux/profile/reducer";
import { routes } from "../../Routes";

const SettingPage: React.FC = () => {
  let token: string;
  let history = useHistory();
  const auth: AuthState = useSelector((state: RootState) => state.auth);
  if (auth.status === "authenticated") token = auth.token;
  const [newAvatarFile, setNewAvatarFile] = useState<File | null>(null);

  const dispatch = useDispatch<RootThunkDispatch>();
  useEffect(() => {
    // getProfile();
    dispatch(loadProfileThunk());
  }, []);
  useEffect(() => {
    // getProfile();
    dispatch(loadProfileThunk());
  }, [newAvatarFile]);
  let profile = useSelector(selectProfile);
  const nickname = profile.nickname;
  const icon_image = profile.icon_image;

  // const [previewUrl, setPreviewUrl] = useState<string | null>(null);
  const openScanner = async () => {
    let files = await selectImage({
      multiple: false,
      // capture: true // to force using camera
    });
    let file = files[0];
    if (!file) {
      return;
    }
    let newAvatarFile = await compressMobilePhoto({ image: file });
    let blob = dataURItoBlob(newAvatarFile);
    file = new File([blob], file.name, {
      lastModified: file.lastModified,
      type: blob.type,
    });

    setNewAvatarFile(file);

    uploadImage(file);

    // setPreviewUrl(previewUrl);
  };
  const router = useIonRouter();

  async function uploadImage(file: File) {
    // confirm();

    if (file) {
      let formData = new FormData();
      console.log(file);

      formData.append("image", file);
      const res = await fetch(`${APIOrigin}/profile/iconImage`, {
        method: "POST",
        body: formData,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      const result = await res.json();

      console.log("upload image profile", result);
    }

    setNewAvatarFile(null);
  }

  const { REACT_APP_API_ORIGIN } = process.env;

  let icon = `${REACT_APP_API_ORIGIN}/uploads/` + icon_image;

  return (
    <IonPage className={styles.page}>
      <IonContent fullscreen>
        <IonHeader>
          <IonToolbar color="orange">
            <IonTitle>Profile</IonTitle>
          </IonToolbar>
        </IonHeader>

        <div className={styles.userHeader}>
          <div className={styles.userPhoto}>
            {icon_image ? <img src={icon} /> : <img src={defaultIcon} />}
            <button
              className={styles.button}
              onClick={() => {
                openScanner();
              }}
            >
              <IonIcon icon={personAddOutline} size="large" />
            </button>
          </div>
          <div className={styles.usernames}>nickname : {nickname}</div>
        </div>
        <div className={styles.line}></div>
        <ProfileItem />
      </IonContent>

      <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
    </IonPage>
  );
};

export default SettingPage;
