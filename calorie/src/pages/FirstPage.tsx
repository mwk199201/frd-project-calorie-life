import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";

const FirstPage: React.FC = () => {
  return (
    <IonPage>
      <IonContent fullscreen>
        <div className="length"></div>
        <div className="container">
          <button id="btn">Login</button>
          <br />
          <button id="btn">Sign Up</button>
          <div className="change">
            <h3>
              <span id="font">or</span>
            </h3>
          </div>
          <button id="btn">Google</button>
          <br />
          <button id="btn">Facebook</button>
        </div>
        <div className="loading-effect"></div>

        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large"></IonTitle>
          </IonToolbar>
        </IonHeader>
      </IonContent>
    </IonPage>
  );
};

export default FirstPage;
