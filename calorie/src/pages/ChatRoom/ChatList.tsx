import {
  IonAvatar,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonPage,
  IonSearchbar,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import moment from "moment";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { RouteComponentProps } from "react-router";

import { useSocket } from "../../context/SocketContent";
import { ChatListItem } from "../../redux/chatroom/state";
import { loadChatListThunk } from "../../redux/chatroom/thunk";
import { RootThunkDispatch } from "../../redux/dispatch";
import { RootState } from "../../redux/state";
import styles from "./ChatList.module.css";
import defaultIcon from "../../default-user-icon-13.jpeg";

const ChatList = () => {
  const io = useSocket();
  const [searchText, setSearchText] = useState("");
  const date = new Date().toDateString();
  const [room, SetRoom] = useState("");

  const chatList = useSelector((state: RootState) => state.chatList.items);
  const chatStatus = useSelector((state: RootState) => state.chatList.status);
  const dispatch = useDispatch<RootThunkDispatch>();

  useEffect(() => {
    dispatch(loadChatListThunk());
  }, [room]);

  useEffect(()=>{
    dispatch(loadChatListThunk());
  },[]/*component is loaded*/)

  // const joinRoom = (roomName: number) => {
  //   io.emit("join_room", roomName);
  // };
  {
    chatStatus === "loading" && <div></div>;
  }
  const { REACT_APP_API_ORIGIN } = process.env;

  return (
    <IonPage>

      <IonHeader>
        <IonToolbar color="orange">
          <IonTitle>Chat Room</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
   
        <IonListHeader>Recent Conversations</IonListHeader>
        <IonList>
          <IonSearchbar
            value={searchText}
            onIonChange={(e) => setSearchText(e.detail.value!)}
          ></IonSearchbar>
          <br />

          {
            chatList.filter((item: ChatListItem) => {
              if(searchText === '') {
                return item;
              }else if (item.nickname.toLowerCase().includes(searchText.toLowerCase())) {
                return chatList;}
            }).
            map((item: ChatListItem) => (
              <div key={item.room_id}>
                <IonItem
                  key={item.room_id}
                  routerLink={"/chatroom/" + item.room_id}
                >
                  <IonAvatar slot="start">
                    {!item.icon_image
                    ? <img src={defaultIcon} />
                    : <img  src={`${REACT_APP_API_ORIGIN}/uploads/${item.icon_image}`}/>
                    }
                  </IonAvatar>
                  <IonLabel>
                    <h2 className={styles.text}>{item.nickname}</h2>
                    <h3 className={styles.text}>{item.user_role}</h3>
                    <p className={styles.upper}>{moment(item.created_at).fromNow()}</p>
                  </IonLabel>
                </IonItem>
              </div>
            ))
          }
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default ChatList;
