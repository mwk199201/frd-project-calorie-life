import {
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { arrowBack, text } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";
import { ReQuickAddItem } from "../../redux/calories_record/record_state";
import {
  changeQuickAddCalorieRecord,
  loadQuickAddFoodDataByIdThunk,
} from "../../redux/calories_record/record_thunk";
import { RootThunkDispatch } from "../../redux/dispatch";
import { RootState } from "../../redux/state";
import { routes } from "../../Routes";
import styles from "./quickAddDetailPage.module.css";

const QuickDetailPage: React.FC = () => {
  const dispatch = useDispatch<RootThunkDispatch>();
  const [calories, setCalories] = useState(0);
  const [carbohydrate, setCarbohydrate] = useState(0);
  const [fat, setFat] = useState(0);
  const [protein, setProtein] = useState(0);
  const [recordList, setRecordList] = useState<ReQuickAddItem>({
    calories: 0,
    carbohydrate: 0,
    protein: 0,
    fat: 0,
  });

  let params = useLocation();
  let foodID = params.pathname.split("/")[3];
  let day=params.pathname.split("/")[4]
  let today=new Date().toISOString().split('T')[0]
  useEffect(() => {
    dispatch(loadQuickAddFoodDataByIdThunk(+foodID));
  }, []);

  useEffect(() => {
    setRecordList({ calories, carbohydrate, protein, fat });
  }, [calories, carbohydrate, protein, fat]);

  const quickDietList = useSelector(
    (state: RootState) => state.calorieRecord.quickItem
  );

  useEffect(() => {
    if (quickDietList) {
      setCalories(quickDietList[0].calories);
      setCarbohydrate(quickDietList[0].carbohydrate);
      setFat(quickDietList[0].fat);
      setProtein(quickDietList[0].protein);
    }
  }, [quickDietList]);

  return (
    <IonPage>
      <IonContent fullscreen>
        <IonHeader>
          <IonToolbar className={styles.header}>
         
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Link to={routes.record} style={{ color: "white" }}>
                  <div className={styles.icon}>
                    <IonIcon icon={arrowBack}></IonIcon>
                  </div>
                </Link>
                <div>Quick Add Detail</div>
                {today===day?
                <Link
                  to={routes.record}
                  style={{ color: "white", textDecoration: "none" }}
                >
                  <div
                    onClick={() =>
                      recordList !== undefined
                        ? dispatch(
                            changeQuickAddCalorieRecord(+foodID, recordList)
                          )
                        : null
                    }
                  >
                    SAVE
                  </div>
                </Link>:null
                }
              </div>
           
          </IonToolbar>
        </IonHeader>
        <IonList>
          <IonItem key={"Description"} className={styles.container}>
            <div className={styles.detailItem}>Description:</div>
            {quickDietList !== undefined ? (
              <div className={styles.itemValue}>
                {quickDietList[0].food_name}
              </div>
            ) : (
              <div className={styles.itemValue}>null</div>
            )}
          </IonItem>

          <IonItem key={"Calories"} className={styles.container}>
            <div className={styles.detailItem}>Calories:</div>
            {today===day? quickDietList !== undefined ? (
              <input
                type="number"
                className={styles.itemValue}
                value={calories}
                onChange={(e) => setCalories(+e.target.value)}
              ></input>
            ) : (
              <div className={styles.itemValue}>0</div>
            ):<div>{calories}</div>}
          </IonItem>

          <IonItem key={"Carbohydrate"} className={styles.container}>
            <div className={styles.detailItem}>Carbohydrate:</div>
            {today===day?quickDietList !== undefined ? (
              <input
                type="number"
                className={styles.itemValue}
                value={carbohydrate}
                onChange={(e) => setCarbohydrate(+e.target.value)}
              ></input>
            ) : (
              <div className={styles.itemValue}>0</div>
            ):<div>{carbohydrate}</div>}
          </IonItem>

          <IonItem key={"Fat"} className={styles.container}>
            <div className={styles.detailItem}>Fat:</div>
            {today===day? quickDietList !== undefined ? (
              <input
                type="number"
                className={styles.itemValue}
                value={fat}
                onChange={(e) => setFat(+e.target.value)}
              ></input>
            ) : (
              <div className={styles.itemValue}>0</div>
            ):<div>{fat}</div>}
          </IonItem>

          <IonItem key={"Protein"} className={styles.container}>
            <div className={styles.detailItem}>Protein:</div>
            {today===day?quickDietList !== undefined ? (
              <input
                type="number"
                className={styles.itemValue}
                value={protein}
                onChange={(e) => setProtein(+e.target.value)}
              ></input>
            ) : (
              <div className={styles.itemValue}>0</div>
            ):<div>{protein}</div>}
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default QuickDetailPage;
