import {IonContent,IonHeader,IonIcon,IonItem,IonList,IonPage,IonTitle,IonToolbar
  } from "@ionic/react";
import { arrowBack, caretBack, caretForward } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";
// import Record_detail_table from "../../components/daily_calories_record/daily_record_detail_table";
import { loadCalorieRecordThunk, loadQuickAddFoodDataThunk } from "../../redux/calories_record/record_thunk";
import { RootThunkDispatch } from "../../redux/dispatch";
import { RootState } from "../../redux/state";
import { routes } from "../../Routes";
import styles from './daily_record_more_detailPage.module.css';


  type DietNutrition={
    calories:number;
    carbohydrates:number;
    cholesterol:number;
    dietary:number;
    protein:number;
    saturated_fat:number;
    sodium:number;
    sugar:number;
    total_fat:number;
    trans_fat:number;
  }
  type QuickDietNutrition={
    calories:number;
    carbohydrates: number;
    fat:number;
    protein:number;
  }

  const RecordPage_moreDetailPage: React.FC = () => {
    const dispatch = useDispatch<RootThunkDispatch>();
    const [recentNutrition,setRecentNutrition]=useState<DietNutrition[]>([])
    const [quickNutrition,setQuickNutrition]=useState<QuickDietNutrition[]>([])
    let parma=useLocation()
    let parmaDate=parma.pathname.split('/')[4]
    parmaDate=new Date(parmaDate).getDate().toString()
    const [day,setDay]=useState<number>(+parmaDate)
    
    
    var today = new Date()
    today.setDate(day)
    let date=today.toDateString()
    let toDispatchDate=today.toISOString().split('T')[0]

    useEffect(() => {
      dispatch(loadCalorieRecordThunk(toDispatchDate))
      dispatch(loadQuickAddFoodDataThunk(toDispatchDate))
    },[day])

    const dietList=useSelector((state:RootState)=>state.calorieRecord.items)
    const quickDietList=useSelector((state:RootState)=>state.calorieRecord.quickItem)

    useEffect(()=>{
      let calories=dietList?.reduce((acc,x)=>acc+x.calories,0)
      let carbohydrates=dietList?.reduce((acc,x)=>acc+x.carbohydrate,0)
      let cholesterol=dietList?.reduce((acc,x)=>acc+x.cholesterol,0)
      let dietary=dietList?.reduce((acc,x)=>acc+x.dietary,0)
      let protein=dietList?.reduce((acc,x)=>acc+x.protein,0)
      let saturated_fat=dietList?.reduce((acc,x)=>acc+x.saturated_fat,0)
      let sodium=dietList?.reduce((acc,x)=>acc+x.sodium,0)
      let sugar=dietList?.reduce((acc,x)=>acc+x.sugar,0)
      let total_fat=dietList?.reduce((acc,x)=>acc+x.total_fat,0)
      let trans_fat=dietList?.reduce((acc,x)=>acc+x.trans_fat,0)
      
      if(calories!=undefined&&carbohydrates!=undefined&&cholesterol!=undefined&&dietary!=undefined&&protein!=undefined&&saturated_fat!=undefined&&sodium!=undefined&&sugar!=undefined&&total_fat!=undefined&&trans_fat!=undefined){
        setRecentNutrition([{calories,carbohydrates,cholesterol,dietary,protein,saturated_fat,sodium,sugar,total_fat,trans_fat}])
      }
      // console.log(recentNutrition)
      // if(total_fat!=undefined){
      //   console.log(total_fat)
      // }
      // console.log("()
    },[dietList])

    useEffect(()=>{
      let calories=quickDietList?.reduce((acc,x)=>acc+x.calories,0)
      let carbohydrates=quickDietList?.reduce((acc,x)=>acc+x.carbohydrate,0)
      let fat=quickDietList?.reduce((acc,x)=>acc+x.fat,0)
      let protein=quickDietList?.reduce((acc,x)=>acc+x.protein,0)

      if(calories!==undefined&&carbohydrates!==undefined&&fat!==undefined&&protein!==undefined){
        setQuickNutrition([{calories,carbohydrates,fat,protein}])
      }
      // console.log(quickNutrition)
    },[quickDietList])

    function preDay(){
      setDay(day-1)
     
  }
  function nextDay(){
      setDay(day+1)
  }


    return (
      <IonPage>
        <IonContent>
          <IonHeader >
            <IonToolbar className={styles.header}>
            <Link to ={routes.record} style={{display: 'flex',color:'white',textDecoration: 'none'}}>
              <div>
                <IonIcon icon={arrowBack} className={styles.icon}></IonIcon>
              </div>
              <IonTitle className={styles.title}>Detail Nutrition</IonTitle>
            </Link>
            </IonToolbar>
          </IonHeader>
          <div className={styles.date}>
            <button onClick={preDay}>
            <IonIcon className={styles.chevron} icon={caretBack}/>
            </button>
            
            <div>{date}</div>
          
            <button>
            <IonIcon onClick={nextDay} className={styles.chevron} icon={caretForward} hidden={date===new Date().toDateString()}/>
            </button>
          </div>
          {/* <Record_detail_table/> */}
          <IonList>
              <IonItem>
                  <div className={styles.total}>Total</div>
              </IonItem>
              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Calories:</div>
                  {quickNutrition[0]===undefined&&recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].calories).toFixed(1)}</div>:
                  recentNutrition[0]===undefined&&quickNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(quickNutrition[0].calories).toFixed(1)}</div>:
                  recentNutrition[0]!==undefined||quickNutrition[0]!==undefined?
                    <div className={styles.nutritionValue}>{(recentNutrition[0].calories+quickNutrition[0].calories).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div> 
                  }
                </div>
              </IonItem>

              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Carbohydrates:</div>
                  {quickNutrition[0]===undefined&&recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].carbohydrates).toFixed(1)}</div>:
                  recentNutrition[0]===undefined&&quickNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(quickNutrition[0].carbohydrates).toFixed(1)}</div>:
                  recentNutrition[0]!==undefined||quickNutrition[0]!==undefined?
                    <div className={styles.nutritionValue}>{(recentNutrition[0].carbohydrates+quickNutrition[0].carbohydrates).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div> 
                  }
                </div>
              </IonItem>

              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Protein:</div>
                  {quickNutrition[0]===undefined&&recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].protein).toFixed(1)}</div>:
                  recentNutrition[0]===undefined&&quickNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(quickNutrition[0].protein).toFixed(1)}</div>:
                  recentNutrition[0]!==undefined||quickNutrition[0]!==undefined?
                    <div className={styles.nutritionValue}>{(recentNutrition[0].protein+quickNutrition[0].protein).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div> 
                  }
                </div>
              </IonItem>

              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Total Fat:</div>
                  {quickNutrition[0]===undefined&&recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].total_fat).toFixed(1)}</div>:
                  recentNutrition[0]===undefined&&quickNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(quickNutrition[0].fat).toFixed(1)}</div>:
                  recentNutrition[0]!==undefined||quickNutrition[0]!==undefined?
                    <div className={styles.nutritionValue}>{(recentNutrition[0].total_fat+quickNutrition[0].fat).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div> 
                  }
                </div>
              </IonItem>

              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Saturated Fat</div>
                  {recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].saturated_fat).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div>}
                </div>
              </IonItem>

              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Trans Fat</div>
                  {recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].trans_fat).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div>}
                </div>
              </IonItem>

              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Cholesterol</div>
                  {recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].cholesterol).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div>}
                </div>
              </IonItem>

              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Dietary Fibre</div>
                  {recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].dietary).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div>}
                </div>
              </IonItem>

              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Sodium</div>
                  {recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].sodium).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div>}
                </div>
              </IonItem>
              
              <IonItem>
                <div style={{display: 'flex'}}>
                  <div className={styles.nutritionName}>Sugar</div>
                  {recentNutrition[0]!==undefined?
                  <div className={styles.nutritionValue}>{(recentNutrition[0].sugar).toFixed(1)}</div>:
                    <div className={styles.nutritionValue}>0</div>}
                </div>
              </IonItem>






          </IonList>
        </IonContent>
      </IonPage>

    );
  };
  
  export default RecordPage_moreDetailPage;
  