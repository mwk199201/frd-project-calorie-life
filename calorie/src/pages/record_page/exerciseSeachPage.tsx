import {
    IonContent,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonPage,
    IonToolbar,
    useIonAlert,
  } from "@ionic/react";
import Dialog from "@reach/dialog";
import { arrowBack ,close, trash} from "ionicons/icons";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { APIOrigin } from "../../api";
import ExerciseQuickAdd from "../../components/daily_calories_record/exercise_search_page_quick";
import ExerciseRecent from "../../components/daily_calories_record/exercise_search_page_recent";
import { routes } from "../../Routes";
  import styles from './exerciseSearchPage.module.css'

export type Exercise={
    quickId:number;
    description:string;
    calories:number;
    exercsieMins:number;
  }

export type RecentExercise={
    id:number;
    exercise_type:string;
    exercise_mins:number;
    exercise_METs:number;
    isselected:boolean;
}
  
  const ExerciseSearchPage: React.FC = () => {
    let [inputValue, setInputValue]=useState('')
    let [quickExerciseList,setQuickExerciseList] = useState<Exercise[]>([]) 
    let [recentExerciseList,setRecentExerciseList] = useState<Exercise[]>([]) 
    const [isRecent, setIsRecent]=useState(true)
    const [selectedQuicNumber, setSelectedQuicNumber] = useState(0)
    const [selectedRecentNumber, setSelectedRecentNumber] = useState(0)
    const [selectedQuicCal, setSelectedQuicCal] = useState(0)
    const [selectedRecentCal, setSelectedRecentCal] = useState(0)
    const [userWeights, setUserWeights] = useState(70)
    const [showDialog, setShowDialog] = useState(false);
    const [deleteRecentItemId,setDeleteRecentItemId]=useState(0)
    const [deleteQuickItemId,setDeleteQuickItemId]=useState(0)
    const [isPressing, setIsPressing] = useState(false)
    const [presentAlert] = useIonAlert();
    const open = () => setShowDialog(true);
    const closes = () => setShowDialog(false)

    useEffect(() =>{
        setSelectedQuicNumber(quickExerciseList.length)
        setSelectedQuicCal((quickExerciseList.reduce((acc, item) => acc + (item.calories),0)))
    },[quickExerciseList.length])

    useEffect(() =>{
        setSelectedRecentNumber(recentExerciseList.length)
        setSelectedRecentCal((recentExerciseList.reduce((acc, item) => acc + (item.calories),0)))
    },[recentExerciseList])

    function quickAddExercise(exercise:Exercise[]){
        let newQuickAddExercise=exercise.slice()
        setQuickExerciseList(newQuickAddExercise)
      }

    function recentAddExercise(exercise:RecentExercise[]){
        let newRecentAddExerciseArray:Exercise[]=[]
       
        exercise.map((item) => {
            let newRecentAddExerciseObject:Exercise={ quickId:item.id,
                description:item.exercise_type,
                calories:userWeights*(item.exercise_mins/60)*item.exercise_METs,
                exercsieMins:item.exercise_mins}
            newRecentAddExerciseArray.push(newRecentAddExerciseObject)
            }
            
        )
        // console.log(newRecentAddExerciseArray)
        setRecentExerciseList(newRecentAddExerciseArray)
      }

      
    function deleteRecentFoodItem(exerciseId:number){
        recentExerciseList= recentExerciseList.filter(exercise => exercise.quickId !== exerciseId)
        setRecentExerciseList(recentExerciseList)
        setDeleteRecentItemId(exerciseId)
      }
      
    function deleteQuickFoodItem(exerciseId:number){
        quickExerciseList= quickExerciseList.filter(exercise => exercise.quickId !== exerciseId)
        setQuickExerciseList(quickExerciseList)
        setDeleteQuickItemId(exerciseId)
      }

      function postExerciseList(userId:number,exercise:Exercise[]){
        if(exercise.length!=0){
        exercise.map(exercise =>{
          fetch(APIOrigin + "/exerciseData/exercise",{
            method: "POST",
            headers: {
                "content-type": "application/json; charset=utf-8",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                // id:exercise.quickId,
                exercise_type:exercise.description,
                exercise_calories:Math.floor(exercise.calories),
                exercise_mins:exercise.exercsieMins
            }),
           })
            .then(res => res.json())
            .catch(error => ({ error: String(error) }))
            .then(json => {
                if (!json.error) {
                  console.log('inserted',exercise)
                } else {
                  console.error('failed to post, food:', exercise, 'error:', json.error)
                }
              })
      
             } )
            
            }
      }

    return (
        <IonPage>
          <IonContent fullscreen>
            <IonHeader>
            <IonToolbar className={styles.header}>
            <div className={styles.container}>
              <Link to={routes.record}>
              <div>
                <IonIcon icon={arrowBack} className={styles.icon}></IonIcon>
              </div>
              </Link>
              <div>Exercise</div>
              <Link style={{textDecoration: 'none'}} to={routes.record}>
              <button className={styles.log} onClick={()=>postExerciseList(1,recentExerciseList.concat(quickExerciseList))}>Log</button>
              </Link>
              
            </div>
              </IonToolbar>
        </IonHeader>

        <IonItem>
          <IonInput placeholder="Search your exercise" onIonChange={e =>inputValue=e.detail.value!}></IonInput>
          <button className={styles.searchBtn} onClick={()=>{setInputValue(inputValue)
          setIsPressing(true)
          if(inputValue===''){
            presentAlert({
              header: 'empty search item?',
              buttons: [
                {
                  text: 'Cancel',
                  role: 'cancel',
                },
                {
                  text: 'OK',
                  role: 'confirm',
                }
              ],
              // onDidDismiss: (e: CustomEvent) => setRoleMessage(`Dismissed with role: ${e.detail.role}`)
            })
    
           }
        }
          }>Search</button>
        </IonItem>

        <div className={styles.chooseAddMethod}>
          <div onClick={()=>{setIsRecent(true)}} className={isRecent? styles.realChoose:styles.null}>RECENT</div>
          <div onClick={()=>{setIsRecent(false)}} className={isRecent? styles.null:styles.realChoose}>QUICK ADD</div>
        </div>
        {isRecent?<ExerciseRecent searchItem={inputValue} selectedItem={item=>recentAddExercise(item)} deleteFoodId={deleteRecentItemId} isPressing={isPressing}/>:<ExerciseQuickAdd quickAddList={list=>quickAddExercise(list)} deleteFoodId={deleteQuickItemId}/>}

        <div className={styles.selectedItemBox} onClick={open}>
        <Dialog isOpen={showDialog} onDismiss={closes} className={styles.dialog} aria-label="selectedItem">
        <div>
            <div className={styles.dialogSelectedItemBox} onClick={closes}>
              <div style={{flexGrow:1}}>
                <div className={styles.selectedItem}>{selectedQuicNumber+selectedRecentNumber} item selected</div>
                <div className={styles.selectedItemCal}>{selectedQuicCal+selectedRecentCal} cal total</div>
              </div>
              <div className={styles.closeBtn}  onClick={closes}>
                <IonIcon aria-hidden icon={close} ></IonIcon>
              </div>
            </div>
            <div style={{height:'2rem'}} onClick={closes}>ada</div>
            {recentExerciseList.length!==0 || quickExerciseList.length!==0?
            <IonItem className={styles.dialogContent} >
            Exercise
          </IonItem>:null            
          }

            {recentExerciseList.map(exercise =><IonItem>
              <div style={{display: 'flex'}}>
                <IonIcon className={styles.trash} icon={trash} onClick={()=>deleteRecentFoodItem(exercise.quickId)}></IonIcon>
                <div className={styles.list}>
                  <div style={{fontWeight:'bold'}}>{exercise.description}</div>
                  <div className={styles.foodWeight}>{exercise.exercsieMins} mins</div>
                </div>
                <div className={styles.foodName}>
                  <div style={{marginRight:'1.5rem'}}>{exercise.calories} Cal</div>
                </div>
              
              </div>
            </IonItem>)}

            {quickExerciseList.map(exercise =><IonItem>
              <div style={{display: 'flex'}}>
                 <IonIcon icon={trash} className={styles.trash} onClick={()=>deleteQuickFoodItem(exercise.quickId)}></IonIcon>
                <div className={styles.list}>
                  <div style={{fontWeight:'bold'}}>{exercise.description}</div>
                  <div className={styles.foodWeight}>{exercise.exercsieMins} mins</div>
                </div>
                <div className={styles.foodName}>
                  <div style={{marginRight:'1.5rem'}}>{exercise.calories}</div>
                </div>
                </div>
            </IonItem>)}
            
          </div>
        </Dialog>
          <div className={styles.selectedItem}>{selectedQuicNumber+selectedRecentNumber} item selected</div>
          <div className={styles.selectedItemCal}>{selectedQuicCal+selectedRecentCal} cal total</div>
          </div>
        </IonContent>
      {/* <div>hi</div> */}
    </IonPage>
  );
};

export default ExerciseSearchPage;
