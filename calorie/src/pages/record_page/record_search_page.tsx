import {
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonToolbar,
  useIonAlert,
} from "@ionic/react";
import { arrowBack ,close, trash} from "ionicons/icons";
import { useEffect, useState } from "react";
import {  useLocation } from "react-router";
import { APIOrigin } from "../../api";
import QuickAdd, { QuickAddItem } from "../../components/daily_calories_record/record_search_page_quick_add";
import Recent, { FoodDataList } from "../../components/daily_calories_record/record_search_page_recent";
import styles from "./record_search_page.module.css";
import { Dialog } from "@reach/dialog";
import "@reach/dialog/styles.css";
import { Link } from "react-router-dom";
import { routes } from "../../Routes";

const Record_search_page: React.FC = () => {
    let locateParams=useLocation()
    let params:string
    let [selectedFood,setSelectedFood]=useState<FoodDataList[]>([])
    let [quickAddSelectedFood,setQuickAddSelectedFood]=useState<QuickAddItem[]>([])
    const [selectedFoodNumber,setSelectedFoodNumber]=useState(0)
    const [selectedFoodCal,setSelectedFoodCal]=useState(0)
    const [showDialog, setShowDialog] = useState(false);
    let [deleteRecentItemId,setDeleteRecentItemId]=useState<number|undefined>()
    let [deleteQuickItemId,setDeleteQuickItemId]=useState<number|undefined>()
    const [isPressing,setIsPressing] = useState(false)
    const open = () => setShowDialog(true);
    const closes = () => setShowDialog(false);
    const [presentAlert] = useIonAlert();
 
      params=locateParams.pathname.split('/')[3]
   
    const [isRecent, setIsRecent]=useState(true)
    let [inputValue, setInputValue]=useState('')

  
  
    let result=localStorage.getItem('searchInputValue')

    useEffect(() =>{
      if(result){
        setInputValue(result)
      }else{setInputValue('')}
    },[result])

    // console.log(inputValue)

    useEffect(() => {
      let totalCal=0
      setSelectedFoodNumber(selectedFood.length+quickAddSelectedFood.length)
      if(selectedFood.length===0){
        setSelectedFoodCal(0)
      }
      selectedFood.map((food) => {
        totalCal+=(+food.calories)
        setSelectedFoodCal(totalCal)
        return food})

      quickAddSelectedFood.map((food) => {
        totalCal+=(+food.calories)
        setSelectedFoodCal(totalCal)
        return food
      })

    },[selectedFood])

    useEffect(()=>{
      let totalCal=0
      setSelectedFoodNumber(selectedFood.length+quickAddSelectedFood.length)
      // console.log(quickAddSelectedFood)
      
      if(quickAddSelectedFood.length===0){
        setSelectedFoodCal(0)
      }

      selectedFood.map((food) => {
        totalCal+=(+food.calories)
        setSelectedFoodCal(totalCal)
        return food})

      quickAddSelectedFood.map((food) => {
        totalCal+=(+food.calories)
        setSelectedFoodCal(totalCal)
        return food
      })
      // setSelectedFood(selectedFood)
      
    },[quickAddSelectedFood.length])

    function postSelectedFood(userId:number,food:FoodDataList[],quickAddFood:QuickAddItem[]){
      localStorage.removeItem ('searchInputValue')
      if(food.length!=0){
      food.map(food =>
        fetch(APIOrigin + "/foodData/foodItem",{
          method: "POST",
          headers: {
              "content-type": "application/json; charset=utf-8",
              "Authorization": `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
              food_name: food.food_eng_name,
              diet_type: params,
              food_type: food.food_type,
              calories: food.calories,
              protein: food.protein,
              carbohydrate: food.carbohydrate,
              total_fat: food.total_fat,
              sugar: food.sugar,
              saturated_fat: food.saturated_fat,
              trans_fat: food.trans_fat,
              cholesterol: food.cholesterol,
              dietary_fiber: food.dietary_fibre,
              sodium: food.sodium,
              food_weight: food.food_weight,
          }),
         })
          .then(res => res.json())
          .catch(error => ({ error: String(error) }))
          .then(json => {
              if (!json.error) {
                console.log('inserted',food)
              } else {
                console.error('failed to post, food:', food, 'error:', json.error)
              }
            })
    
        )
          }
        if(quickAddFood.length!==0){
          quickAddFood.map(quickFood=>{
            
            fetch(APIOrigin + "/foodData/quickAddFoodItem",{
              method: "POST",
              headers: {
                  "content-type": "application/json; charset=utf-8",
                  "Authorization": `Bearer ${localStorage.getItem('token')}`
              },
              body: JSON.stringify({
                food_name:quickFood.foodName,
                calories:quickFood.calories,
                diet_type: params,
                protein:quickFood.protein,
                carbohydrate:quickFood.carbohydrates,
                fat:quickFood.fat
              }),
            })
            .then(res => res.json())
            .catch(error => ({ error: String(error) }))
            .then(json => {
              if (!json.error) {
                console.log('inserted',food)
              } else {
                console.error('failed to post, food:', food, 'error:', json.error)
              }
            })
          })
        }
    }

    function deleteRecentFoodItem(foodId:number){
      selectedFood= selectedFood.filter(food => food.id !== foodId)
      setSelectedFood(selectedFood)
      setDeleteRecentItemId(foodId)
    }

    function deleteQuickFoodItem(foodId:number){
      quickAddSelectedFood= quickAddSelectedFood.filter(food => food.id !== foodId)
      setQuickAddSelectedFood(quickAddSelectedFood)
      setDeleteQuickItemId(foodId)
    }

    function quickAddFood(food:QuickAddItem[]){
      let newQuickAddFood=food.slice()
      setQuickAddSelectedFood(newQuickAddFood)
    }
    // const params:{add_diet:string} = useParams()
    // console.log(params)
  return (
    <IonPage>
      <IonContent fullscreen>
        <IonHeader>
          <IonToolbar className={styles.header}>
            <div style={{display: 'flex'}}>
              <Link to={routes.record}>
              <div>
                <IonIcon icon={arrowBack} className={styles.icon} onClick={()=>{localStorage.removeItem('searchInputValue')
              setIsPressing(false)
              }}></IonIcon>
              </div>
              </Link>
              <div className={styles.chooseDiet}>
              <div style={{display: 'flex',alignItems: 'center'}}>
              {params}
              </div>
              </div>
              <Link style={{textDecoration: 'none'}} to={routes.record}>
              <button className={styles.log} onClick={()=>{postSelectedFood(1,selectedFood,quickAddSelectedFood)
              setIsPressing(false)
              }}>Log</button>
              </Link>
              
            </div>
          </IonToolbar>
        </IonHeader>
        <IonItem>
          <IonInput placeholder="Search your diet" onIonChange={e =>inputValue=e.detail.value!}></IonInput>
          <button className={styles.searchBtn} onClick={()=>{setInputValue(inputValue)
           localStorage.setItem('searchInputValue',inputValue)
           setIsPressing(true)
           if(inputValue===''||localStorage.getItem('searchInputValue')===''){
            presentAlert({
              header: 'empty search item?',
              buttons: [
                {
                  text: 'Cancel',
                  role: 'cancel',
                },
                {
                  text: 'OK',
                  role: 'confirm',
                }
              ],
              // onDidDismiss: (e: CustomEvent) => setRoleMessage(`Dismissed with role: ${e.detail.role}`)
            })
    
           }
           }}>Search</button>
        </IonItem>
    
        <div className={styles.chooseAddMethod}>
          <div onClick={()=>{setIsRecent(true)}} className={isRecent? styles.realChoose:styles.null}>RECENT</div>
          <div onClick={()=>{setIsRecent(false)}} className={isRecent? styles.null:styles.realChoose}>QUICK ADD</div>
        </div>
        {isRecent?<Recent searchItem={inputValue} add_diet={params} selectedFood={food=>setSelectedFood(food)} deleteFoodId={deleteRecentItemId} isPressing={isPressing}/>:<QuickAdd selectedFood={(food)=>{quickAddFood(food)}} deleteFoodId={deleteQuickItemId}/>}

        <div className={styles.selectedItemBox} onClick={open}>
        <Dialog isOpen={showDialog} onDismiss={closes} className={styles.dialog} aria-label="selectedItem">
          <div>
            <div className={styles.dialogSelectedItemBox} onClick={closes}>
              <div style={{flexGrow:1}}>
                <div className={styles.selectedItem}>{selectedFoodNumber} item selected</div>
                <div className={styles.selectedItemCal}>{selectedFoodCal} cal total</div>
              </div>
              <div className={styles.closeBtn}  onClick={closes}>
                <IonIcon aria-hidden icon={close} ></IonIcon>
              </div>
            </div>
            <div style={{height:'2rem'}} onClick={closes}>ada</div>
            {selectedFood.length!==0 || quickAddSelectedFood.length!==0?
            <IonItem className={styles.dialogContent} >
            {params.replace('Add ','')}
          </IonItem>:null            
          }
            
            {selectedFood.map((food,i) =><IonItem key={i+'recent'}>
              <div  style={{display: 'flex'}}>
                <IonIcon className={styles.trash} icon={trash} onClick={()=>deleteRecentFoodItem(food.id)}></IonIcon>
                <div className={styles.list}>
                  <div style={{fontWeight:'bold'}}>{food.food_eng_name}</div>
                  <div className={styles.foodWeight}>{food.calories} cal</div>
                </div>
                <div className={styles.foodName}>
                  <div style={{marginRight:'1.5rem'}}>{food.food_weight} g</div>
                </div>
              
              </div>
            </IonItem>)}

            {quickAddSelectedFood.map((food,i) =><IonItem key={i+'quick'}>
              <div  style={{display: 'flex'}}>
                 <IonIcon icon={trash} className={styles.trash} onClick={()=>deleteQuickFoodItem(food.id)}></IonIcon>
                <div className={styles.list}>
                  <div style={{fontWeight:'bold'}}>{food.foodName}</div>
                  <div className={styles.foodWeight}>1 serving </div>
                </div>
                <div className={styles.foodName}>
                  <div style={{marginRight:'1.5rem'}}>{food.calories}</div>
                </div>
              
              </div>
            </IonItem>)}
        
          </div>

        </Dialog>
          <div className={styles.selectedItem}>{selectedFoodNumber} item selected</div>
          <div className={styles.selectedItemCal}>{selectedFoodCal} cal total</div>
          </div>
      </IonContent>
      {/* <div>hi</div> */}
    </IonPage>
  );
};

export default Record_search_page;
