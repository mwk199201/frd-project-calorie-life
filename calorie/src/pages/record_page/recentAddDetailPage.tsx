import {
    IonContent,
    IonHeader,
    IonIcon,
    IonItem,
    IonList,
    IonPage,
    IonTitle,
    IonToolbar,
  } from "@ionic/react";
  import styles from "./food_detail_page.module.css";
  import { useLocation } from "react-router";
  import { arrowBack, checkbox } from "ionicons/icons";
  import { useEffect, useState } from "react";
  import { Link } from "react-router-dom";
  import { routes } from "../../Routes";
import { useDispatch, useSelector } from "react-redux";
import { RootThunkDispatch } from "../../redux/dispatch";
import {  changeRecentAddCalorieRecord, loadRecentAddFoodDataByIdThunk } from "../../redux/calories_record/record_thunk";
import { RootState } from "../../redux/state";
import {  RecentFoodList } from "../../redux/calories_record/record_state";
  
  const RecentAddDetailPage: React.FC = () => {
    const dispatch = useDispatch<RootThunkDispatch>();
    const [isConfirm, setIsConfirm] = useState(false);
    const [foodValue, setFoodValue] = useState(1);
    const [foodWeight, setFoodWeight] = useState(0);
    const [recordLis,setRecordList]= useState<RecentFoodList>()
    const param = useLocation();
    // let foodID=param.pathname.replace('/Food_detail_page/','')
    let foodID = param.pathname.split("/")[3];
    let day = param.pathname.split("/")[4];
    let today=new Date().toISOString().split('T')[0]
    // console.log(foodID)
    // let diet = param.pathname.split("/")[3];
    useEffect(()=>{
        dispatch(loadRecentAddFoodDataByIdThunk(+foodID))   
    },[])

    const dietList=useSelector((state:RootState)=>state.calorieRecord.items)

    useEffect(()=>{
        if(dietList){
            setFoodWeight(dietList[0].food_weight)
            setRecordList(dietList[0])
        }
    },[dietList])

    // useEffect(()=>{
    //     if(recordLis){
    //         if(foodWeight>recordLis.food_weight){
    //             setRecordList({
    //                 calories:+((recordLis.calories*(foodWeight/100)).toFixed(2)),
    //                 carbohydrate: +(recordLis.carbohydrate*(foodWeight/100)).toFixed(2),
    //                 cholesterol: +(recordLis.cholesterol*(foodWeight/100)).toFixed(2),
    //                 dietary: +(recordLis.dietary*(foodWeight/100)).toFixed(2),
    //                 food_weight:+(recordLis.food_weight*(foodWeight/100)).toFixed(2),
    //                 protein: +(recordLis.protein*(foodWeight/100)).toFixed(2),
    //                 saturated_fat: +(recordLis.saturated_fat*(foodWeight/100)).toFixed(2),
    //                 sodium: +(recordLis.sodium*(foodWeight/100)).toFixed(2),
    //                 sugar: +(recordLis.sugar*(foodWeight/100)).toFixed(2),
    //                 total_fat: +(recordLis.total_fat*(foodWeight/100)).toFixed(2),
    //                 trans_fat: +(recordLis.trans_fat*(foodWeight/100)).toFixed(2),
    //             })}else{
    //         setRecordList({
    //             calories:+((recordLis.calories/(foodWeight/100)).toFixed(2)),
    //             carbohydrate: +(recordLis.carbohydrate/(foodWeight/100)).toFixed(2),
    //             cholesterol: +(recordLis.cholesterol/(foodWeight/100)).toFixed(2),
    //             dietary: +(recordLis.dietary/(foodWeight/100)).toFixed(2),
    //             food_weight:+(recordLis.food_weight/(foodWeight/100)).toFixed(2),
    //             protein: +(recordLis.protein/(foodWeight/100)).toFixed(2),
    //             saturated_fat: +(recordLis.saturated_fat/(foodWeight/100)).toFixed(2),
    //             sodium: +(recordLis.sodium/(foodWeight/100)).toFixed(2),
    //             sugar: +(recordLis.sugar/(foodWeight/100)).toFixed(2),
    //             total_fat: +(recordLis.total_fat/(foodWeight/100)).toFixed(2),
    //             trans_fat: +(recordLis.trans_fat/(foodWeight/100)).toFixed(2),
    //         })
    //     }
    // }
    // },[foodWeight])

  
    function confirmValue(newFoodWeight: number,lastFoodWeight: number) {
      setFoodValue(newFoodWeight / (lastFoodWeight));
        let value =(newFoodWeight / (lastFoodWeight))
      setIsConfirm(true);
      if(recordLis){
            setRecordList({
                calories:+(recordLis.calories*value).toFixed(0),
                carbohydrate: +(recordLis.carbohydrate*value).toFixed(2),
                cholesterol: +(recordLis.cholesterol*value).toFixed(2),
                dietary: +(recordLis.dietary*value).toFixed(2),
                food_weight:Math.floor(recordLis.food_weight*value),
                protein: +(recordLis.protein*value).toFixed(2),
                saturated_fat: +(recordLis.saturated_fat*value).toFixed(2),
                sodium: +(recordLis.sodium*value).toFixed(2),
                sugar: +(recordLis.sugar*value).toFixed(2),
                total_fat: +(recordLis.total_fat*value).toFixed(2),
                trans_fat: +(recordLis.trans_fat*value).toFixed(2),
                            })
                        }
                        
        }
      
    
    function save(id:number,recordList:RecentFoodList) {
      setIsConfirm(false);
      dispatch(changeRecentAddCalorieRecord(id,recordList))

    }

    return (
      <IonPage style={{ marginBottom: "50px" }}>
        <IonContent>
          <IonHeader>
            <IonToolbar className={styles.header}>
              <IonTitle>
                <div style={{ display: "flex" ,justifyContent:'space-between',alignItems: 'center' }}>
                <Link to={routes.record} style={{color:'white'}}>
                <div style={{fontSize:'larger',marginRight:'1rem'}} className={styles.icon}>
                  <IonIcon icon={arrowBack}></IonIcon>
                </div>
                </Link>
                  <div style={{ flexGrow: 1 }}>Food Detail</div>
                  {today===day?
                  <Link style={{color: "white",textDecoration:"none"}}
                   to={routes.record}
                  >
                    <div className={styles.edit} onClick={() =>recordLis!==undefined? save(+foodID,recordLis):null}>
                      SAVE
                    </div>
                  </Link>:null}
                </div>
              </IonTitle>
            </IonToolbar>
          </IonHeader>
  
          {dietList === undefined ? null : (
            <div>
              {dietList.map((food,i) => (
                <div key={i}>
                  <IonList key={'list'}>
                    <IonItem key={i+'foodName'} className={styles.foodName}>
                      {food.food_name}
                    </IonItem>
                    <IonItem key={i+'foodWeight'} className={styles.servingNumber}>
                      <div style={{ flexGrow: 1 }}>Number of serving</div>
                      {today===day?
                      <input
                        type="number"
                        value={foodWeight}
                        placeholder="(e.g 100)"
                        onChange={(e) => setFoodWeight(+e.target.value)}
                      ></input>:<div>{foodWeight}</div>}
                      g
                      {today===day?
                      <IonIcon
                        icon={checkbox}
                        className={isConfirm ? styles.colorIcon : styles.icon}
                        onClick={() => confirmValue(foodWeight,food.food_weight)}
                      ></IonIcon>:null}
                    </IonItem>
                  </IonList>
                  <IonList>
                    <IonItem key={i+'DetailNutrition'} className={styles.detailNutrition}>
                      Detail Nutrition
                    </IonItem>
                    <IonItem key={i+'Calories'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Calories</div>
                      <div>{(+food.calories * +foodValue).toFixed(2)} Cal</div>
                    </IonItem>
                    <IonItem key={i+'carbohydrate'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Carbohydrate</div>
                      <div>{(+food.carbohydrate * +foodValue).toFixed(2)} g</div>
                    </IonItem>
                    <IonItem key={i+'cholesterol'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Cholesterol</div>
                      <div>{(+food.cholesterol * +foodValue).toFixed(2)} mg</div>
                    </IonItem>
                    <IonItem key={i+'Dietary'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Dietary Fibre</div>
                      <div>{(+food.dietary * +foodValue).toFixed(2)} g</div>
                    </IonItem>
                    <IonItem key={i+'protein'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Protein</div>
                      <div>{(+food.protein * +foodValue).toFixed(2)}g</div>
                    </IonItem>
                    <IonItem key={i+'total_fat'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Total Fat</div>
                      <div>{(+food.total_fat * +foodValue).toFixed(2)}g</div>
                    </IonItem>
                    <IonItem key={i+'saturated_fat'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Saturated Fat</div>
                      <div>{(+food.saturated_fat * +foodValue).toFixed(2)}g</div>
                    </IonItem>
                    <IonItem key={i+'trans_fat'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Trans Fat</div>
                      <div>{(+food.trans_fat * +foodValue).toFixed(2)}g</div>
                    </IonItem>
                    <IonItem key={i+'sugar'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Sugar</div>
                      <div>{(+food.sugar * +foodValue).toFixed(2)}g</div>
                    </IonItem>
                    <IonItem key={i+'sodium'} className={styles.detailContent}>
                      <div style={{ flexGrow: 1 }}>Sodium</div>
                      <div>{(+food.sodium * +foodValue).toFixed(2)}g</div>
                    </IonItem>
                  </IonList>
                </div>
              ))}
            </div>
          )}
        </IonContent>
      </IonPage>
    );
  };
  
  export default RecentAddDetailPage;