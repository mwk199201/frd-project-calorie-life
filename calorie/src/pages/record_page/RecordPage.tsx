import { IonContent, IonPage } from "@ionic/react";
import AddDietButton, {
  AddDiet,
} from "../../components/daily_calories_record/AddDietButton";
import Daily_calories_record, {
  DailyRecordCalculate,
} from "../../components/daily_calories_record/Daily_calories";
// import ExploreContainer from '../components/ExploreContainer';
import styles from "./RecordPage.module.css";
import breakfast from "./breakfast.jpg";
import lunchImg from "./lunch-time.svg";
import dinnerImg from "./dinner.jpg";
import exerciseImg from "./exercise.jpg";
import snack from "./shack.jpg";
import AddWaterButton from "../../components/daily_calories_record/AddWaterButton";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  loadCalorieRecordThunk,
  loadExerciseThunk,
  loadQuickAddFoodDataThunk,
  loadWaterThunk,
} from "../../redux/calories_record/record_thunk";
import { RootState } from "../../redux/state";
import { RootThunkDispatch } from "../../redux/dispatch";
import {
  ExerciseList,
  FoodDataList,
  QuickAddItem,
} from "../../redux/calories_record/record_state";
import { loadProfileThunk } from "../../redux/profile/thunk";
import { calculateCalories } from "../../calculateCalories";

// fetch(APIOrigin + '/foodData')
// .then(res=>res.json())
// .then(json=>console.log({json}))

const Daily_record_page: React.FC = () => {
  const dispatch = useDispatch<RootThunkDispatch>();
  const [breakfastList, setAddBreakfastList] = useState<FoodDataList[]>([]);
  const [lunchList, setLunchList] = useState<FoodDataList[]>([]);
  const [dinnerList, setDinnerList] = useState<FoodDataList[]>([]);
  const [snackList, setSnackList] = useState<FoodDataList[]>([]);
  const [quickBreakfastList, setQuickBreakfastList] = useState<QuickAddItem[]>(
    []
  );
  const [quickLunchList, setQuickLunchList] = useState<QuickAddItem[]>([]);
  const [quickDinnerList, setQuickDinnerList] = useState<QuickAddItem[]>([]);
  const [quickSnackList, setQuickSnackList] = useState<QuickAddItem[]>([]);
  const [budget, setBudget] = useState(0);
  const [dayValue, setDayValue] = useState("");
  const [breakfastCal, setBreakfastCal] = useState(0);
  const [lunchCal, setLunchCal] = useState(0);
  const [dinnerCal, setDinnerCal] = useState(0);
  const [snackCal, setSnackCal] = useState(0);
  const [quickBreakfastCal, setQuickBreakfastCal] = useState(0);
  const [quickLunchCal, setQuickLunchCal] = useState(0);
  const [quickDinnerCal, setQuickDinnerCal] = useState(0);
  const [quickSnackCal, setQuickSnackCal] = useState(0);
  const [deleteItemId, setDeleteItemId] = useState(0);
  const [cupOfWater, setCupOfWater] = useState(0);
  
  const [exerciseList, setExerciseList] = useState<ExerciseList[]>([]);

  let [recentRecordCalculate, setRecentRecordCalculate] = useState<number>(0);
  let [quickRecordCalculate, setQuickRecordCalculate] = useState<number>(0);

  useEffect(() => {
    dispatch(loadProfileThunk());
    dispatch(loadWaterThunk(dayValue));
    dispatch(loadExerciseThunk(dayValue))
  }, []);

  const userInfo = useSelector((state: RootState) => state.profile);
  const water = useSelector((state: RootState) => state.calorieRecord.water);
  const exercise = useSelector((state: RootState) => state.calorieRecord.exercise);


  useEffect(() => {
    if (
      userInfo.age !== null &&
      userInfo.weight !== null &&
      userInfo.height !== null &&
      userInfo.activity !== null &&
      userInfo.gender !== null &&
      userInfo.aim !== null
    ) {
      let result = calculateCalories(
        userInfo.age,
        userInfo.weight,
        userInfo.height,
        +userInfo.activity,
        userInfo.gender,
        userInfo.aim
      );
      setBudget(result);
    }
  }, [userInfo]);

  // useEffect(() => {
  //   console.log(dayValue);
  //   dispatch(loadWaterThunk(dayValue));
  // }, []);
  useEffect(() => {
    if(exercise!==undefined){
      setExerciseList(exercise)
    }
  },[exercise]);

  useEffect(() => {
    if (dayValue) {
      dispatch(loadCalorieRecordThunk(dayValue));
      dispatch(loadWaterThunk(dayValue));
      if (water !== undefined && water.length > 0) {
        setCupOfWater(water[0].cup_of_water);
        // console.log(water[0].cup_of_water)
      } else {
        setCupOfWater(0);
      }
    }
    // console.log(dayValue)
  }, [dayValue, deleteItemId]);

  // useEffect(() => {
  //   if (deleteItemId) {
  //     dispatch(loadCalorieRecordThunk(dayValue));
  //   }
  // }, [deleteItemId]);

  useEffect(() => {
    if (dayValue) {
      dispatch(loadQuickAddFoodDataThunk(dayValue));
      dispatch(loadExerciseThunk(dayValue))
    }
  }, [dayValue]);

  useEffect(() => {
    if (deleteItemId) {
      dispatch(loadQuickAddFoodDataThunk(dayValue));
      dispatch(loadCalorieRecordThunk(dayValue));
      dispatch(loadExerciseThunk(dayValue))
    }
  }, [deleteItemId]);

  const dietList = useSelector((state: RootState) => state.calorieRecord.items);
  const quickDietList = useSelector(
    (state: RootState) => state.calorieRecord.quickItem
  );

  useEffect(() => {
    if (water !== undefined && water.length > 0) {
      setCupOfWater(water[0].cup_of_water);
    }
  }, [water]);
  //  console.log(quickDietList)
  //  if(newbra){
  //   setAddBreakfastList(newbra)
  //  }

  useEffect(() => {
    let diet = dietList?.reduce((acc, x) => acc + x.calories, 0);
    let addBreakfastList = dietList?.filter(
      (item) => item.diet_type === "Add Breakfast"
    );
    let addLunch = dietList?.filter((item) => item.diet_type === "Add Lunch");
    let addDinner = dietList?.filter((item) => item.diet_type === "Add Dinner");
    let addSnack = dietList?.filter((item) => item.diet_type === "Add Shack");
    let breakfast = 0;
    let lunch = 0;
    let dinner = 0;
    let snack = 0;
    // let dietConsumed=0

    if (addBreakfastList) {
      setAddBreakfastList(addBreakfastList);
      addBreakfastList.map((diet) => (breakfast += +diet.calories));
      setBreakfastCal(breakfast);
      // setConsumed(consumed+breakfast)
    }
    if (addLunch) {
      setLunchList(addLunch);
      addLunch.map((diet) => (lunch += +diet.calories));
      setLunchCal(lunch);
      // setConsumed(consumed+lunch)
    }
    if (addDinner) {
      setDinnerList(addDinner);
      addDinner.map((diet) => (dinner += +diet.calories));
      setDinnerCal(dinner);
      // setConsumed(consumed+dinner)
    }
    if (addSnack) {
      setSnackList(addSnack);
      addSnack.map((diet) => (snack += +diet.calories));
      setSnackCal(snack);
      // setConsumed(consumed+snack)
    }

    if (diet === undefined && diet === 0) {
      setRecentRecordCalculate(0);
    }
    if (diet !== undefined) {
      recentRecordCalculate = diet;
      setRecentRecordCalculate(recentRecordCalculate);
    }

    // if(diet){
    //   // console.log(diet)
    //   // diet.map(diet=>dietConsumed+=(diet))
    //   setConsumed(dietConsumed)
    // }

    // setOver(over)
    // setBudget(calorie)
    // if(dietList){
    // setConsumed(dietConsumed)
    //   dailyRecordCalculate=[{budget,consumed,exercise,over}]
    //   setDailyRecordCalculate(dailyRecordCalculate)
    // }
  }, [dietList]);
  // console.log(breakfastList)

  useEffect(() => {
    let diet = quickDietList?.reduce((acc, x) => acc + x.calories, 0);
    let quickAddBreakfastList = quickDietList?.filter(
      (item) => item.diet_type === "Add Breakfast"
    );
    let quickAddLunch = quickDietList?.filter(
      (item) => item.diet_type === "Add Lunch"
    );
    let quickAddDinner = quickDietList?.filter(
      (item) => item.diet_type === "Add Dinner"
    );
    let quickAddSnack = quickDietList?.filter(
      (item) => item.diet_type === "Add Shack"
    );
    let breakfast = 0;
    let lunch = 0;
    let dinner = 0;
    let snack = 0;

    if (quickAddBreakfastList) {
      setQuickBreakfastList(quickAddBreakfastList);
      quickAddBreakfastList.map((diet) => (breakfast += +diet.calories));
      setQuickBreakfastCal(breakfast);
    }
    if (quickAddLunch) {
      setQuickLunchList(quickAddLunch);
      quickAddLunch.map((diet) => (lunch += +diet.calories));
      setQuickLunchCal(lunch);
    }
    if (quickAddLunch) {
      setQuickLunchList(quickAddLunch);
      quickAddLunch.map((diet) => (lunch += +diet.calories));
      setQuickLunchCal(lunch);
    }
    if (quickAddDinner) {
      setQuickDinnerList(quickAddDinner);
      quickAddDinner.map((diet) => (dinner += +diet.calories));
      setQuickDinnerCal(dinner);
    }
    if (quickAddSnack) {
      setQuickSnackList(quickAddSnack);
      quickAddSnack.map((diet) => (snack += +diet.calories));
      setQuickSnackCal(snack);
    }

    if (diet === undefined && diet === 0) {
      setQuickRecordCalculate(0);
    }
    if (diet !== undefined) {
      quickRecordCalculate = diet;
      setQuickRecordCalculate(quickRecordCalculate);
    }

    // quickDietList?.map(diet =>consumed+=(+diet.calories))
    // setConsumed(consumed)
    // let calorie=budget-consumed
    // // setOver(over)
    // setBudget(calorie)
    // if(quickDietList){
    //   dailyRecordCalculate=[{budget,consumed,exercise,over}]
    //   setDailyRecordCalculate(dailyRecordCalculate)
    // }
  }, [quickDietList]);

  const diet: AddDiet[] = [
    {
      id: 1,
      dietType: "Add Breakfast",
      dietImg: breakfast,
      dietList: breakfastList,
      quickdietList: quickBreakfastList,
      recommendCalories: Math.floor(budget*0.4),
      recentDietTotalCal: breakfastCal,
      quickDietTotalCal: quickBreakfastCal,
    },
    {
      id: 2,
      dietType: "Add Lunch",
      dietImg: lunchImg,
      dietList: lunchList,
      quickdietList: quickLunchList,
      recommendCalories:Math.floor(budget*0.3) ,
      recentDietTotalCal: lunchCal,
      quickDietTotalCal: quickLunchCal,
    },
    {
      id: 3,
      dietType: "Add Dinner",
      dietImg: dinnerImg,
      dietList: dinnerList,
      quickdietList: quickDinnerList,
      recommendCalories: Math.floor(budget*0.2),
      recentDietTotalCal: dinnerCal,
      quickDietTotalCal: quickDinnerCal,
    },
    {
      id: 4,
      dietType: "Add Shack",
      dietImg: snack,
      dietList: snackList,
      quickdietList: quickSnackList,
      recommendCalories: Math.floor(budget*0.1),
      recentDietTotalCal: snackCal,
      quickDietTotalCal: quickSnackCal,
    },
    {
      id: 5,
      dietType: "Exercise",
      dietImg: exerciseImg,
      dietList: [],
      quickdietList: [],
      exerciseList: exerciseList,
      recommendCalories: userInfo.aim==="BuildMuscle"?Math.floor(budget*0.3):Math.floor(budget*0.15),
      recentDietTotalCal: 0,
      quickDietTotalCal: 0,
    },
  ];

  return (
    <IonPage>
      {/* <IonHeader>
        <IonToolbar>
          <IonTitle>Daily_record_page</IonTitle>
        </IonToolbar>
      </IonHeader> */}
      <IonContent fullscreen className={styles.content}>
        <div className={styles.header}>
          <Daily_calories_record
            className={styles.header}
            recentRecordCalculate={recentRecordCalculate}
            quickRecordCalculate={quickRecordCalculate}
            dayValue={(date) => setDayValue(date)}
            userBudget={budget}
            exercise={exerciseList}
          />
        </div>
        <div className={styles.content}>
          {Array.isArray(diet) &&
            diet.map((item) => (
              //   <AddDietButton id={0} dietType={''} dietImg={''} dietList={[]} recommendCalories={0}/>
              <AddDietButton
                key={item.id}
                diet={item}
                className={styles.content}
                deleteItem={(id) => setDeleteItemId(id)}
                date={dayValue}
              />
            ))}
        </div>
        <div className={styles.addWater}>
          <AddWaterButton date={dayValue} cupOfWater={cupOfWater} />
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Daily_record_page;
