import {
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import styles from "./food_detail_page.module.css";
import { useHistory, useLocation } from "react-router";
import { FoodDataList } from "../../components/daily_calories_record/record_search_page_recent";
import { useGet } from "../../hooks/useGet";
import { arrowBack, checkbox } from "ionicons/icons";
import { useState } from "react";
import { Link } from "react-router-dom";
import { routes } from "../../Routes";

const FoodDetailPage: React.FC = () => {
  const [inputValue, setInputValue] = useState(0);
  const [isConfirm, setIsConfirm] = useState(false);
  const [foodValue, setFoodValue] = useState(1);
  const [foodCalories, setFoodCalories] = useState(0);
  const param = useLocation();
  // let foodID=param.pathname.replace('/Food_detail_page/','')
  let foodID = param.pathname.split("/").slice(-2)[0];
  let diet = param.pathname.split("/")[3];
  console.log(diet)
  const { json, reload } = useGet<{
    foodData: FoodDataList[];
    error: string;
  }>("/foodData/id/" + foodID, {
    error: "loading",
    foodData: [],
  });
  
let history = useHistory()
  function confirmValue(foodCalories: number) {
    setFoodValue(inputValue / 100);
    setIsConfirm(true);
    setFoodCalories(foodCalories);
  }
  // function edit() {
  //   setIsConfirm(false);
  //   setInputValue(0);
  // }
  return (
    <IonPage style={{ marginBottom: "50px" }}>
      <IonContent>
        <IonHeader>
          <IonToolbar className={styles.header}>
            <IonTitle>
              <div className={styles.title}>
                <button className={styles.icon} onClick={()=>history.goBack()}>
                <IonIcon  icon={arrowBack} ></IonIcon>
                </button>
                <div >Food Detail</div>
                {/* <Link
                  to={routes.record_search_page({
                    add_diet:
                      diet 
                      +
                      "/" +
                      "?" +
                      foodID +
                      "?" +
                      foodValue * 100 +
                      "?" +
                      foodCalories * foodValue,
                  })}
                > */}
                  {/* <div className={styles.edit} onClick={() => edit()}>
                    EDIT
                  </div> */}
                {/* </Link> */}
              </div>
            </IonTitle>
          </IonToolbar>
        </IonHeader>
          
        {json.error !== undefined ? null : (
          <div>
            {json.foodData.map((food,i) => (
              <div key={i}>
                <IonList>
                  <IonItem className={styles.foodName}>
                    {food.food_eng_name}
                  </IonItem>
                  <IonItem className={styles.servingNumber}>
                    <div style={{ flexGrow: 1 }}>Number of serving</div>
                    {/* <input
                      type="number"
                      value={inputValue}
                      placeholder="(e.g 100g)"
                      onChange={(e) => setInputValue(+e.target.value)}
                    ></input> */}
                    {food.food_weight}
                    g
                    {/* <IonIcon
                      icon={checkbox}
                      className={isConfirm ? styles.colorIcon : styles.icon}
                      onClick={() => confirmValue(+food.calories)}
                    ></IonIcon> */}
                  </IonItem>
                </IonList>
                <IonList>
                  <IonItem className={styles.detailNutrition}>
                    Detail Nutrition
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Calories</div>
                    <div>{(+food.calories * +foodValue).toFixed(2)} Cal</div>
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Carbohydrate</div>
                    <div>{(+food.carbohydrate * +foodValue).toFixed(2)} g</div>
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Cholesterol</div>
                    <div>{(+food.cholesterol * +foodValue).toFixed(2)} mg</div>
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Dietary Fibre</div>
                    <div>{(+food.dietary_fibre * +foodValue).toFixed(2)} g</div>
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Protein</div>
                    <div>{(+food.protein * +foodValue).toFixed(2)}g</div>
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Total Fat</div>
                    <div>{(+food.total_fat * +foodValue).toFixed(2)}g</div>
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Saturated Fat</div>
                    <div>{(+food.saturated_fat * +foodValue).toFixed(2)}g</div>
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Trans Fat</div>
                    <div>{(+food.trans_fat * +foodValue).toFixed(2)}g</div>
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Sugar</div>
                    <div>{(+food.sugar * +foodValue).toFixed(2)}g</div>
                  </IonItem>
                  <IonItem className={styles.detailContent}>
                    <div style={{ flexGrow: 1 }}>Sodium</div>
                    <div>{(+food.sodium * +foodValue).toFixed(2)}g</div>
                  </IonItem>
                </IonList>
              </div>
            ))}
          </div>
        )}
      </IonContent>
    </IonPage>
  );
};

export default FoodDetailPage;
