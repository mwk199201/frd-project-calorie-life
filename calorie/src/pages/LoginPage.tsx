import {
  IonButton,
  IonCard,
  IonCardHeader,
  IonContent,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  useIonAlert,
  useIonRouter,
} from "@ionic/react";
// import { push } from "ionicons/icons";

import { useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
// import { Redirect } from "react-router";
// import WaveHeader from "../components/WaveHeader/WaveHeader";
import { useHasLogin } from "../hooks/use-has-login";
// import { loginAction } from "../redux/auth/action";
import { loginThunk } from "../redux/auth/thunk";
import { RootThunkDispatch } from "../redux/dispatch";
import { RootState } from "../redux/state";
import { routes } from "../Routes";

import styles from "./LoginPage.module.css";
// import RegisterPage from "./RegisterPage";

const LoginPage: React.FC = () => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  // const error = useSelector((state: RootState) => {
  //   if (state.auth.status === "error") {
  //     return state.auth.error;
  //   } else {
  //     return false;
  //   }
  // });
  // console.log("error", error);

  const [presentAlert] = useIonAlert();

  function validateInput(error?: string) {
    if (error) {
      presentAlert({
        header: "Alert",
        subHeader: "Important message",
        message: `${error}`,
        buttons: ["OK"],
      });
      return false;
    }
    if (!username) {
      presentAlert({
        header: "Alert",
        subHeader: "Important message",
        message: "Please input username!",
        buttons: ["OK"],
      });
      return false;
    }
    if (!password) {
      presentAlert({
        header: "Alert",
        subHeader: "Important message",
        message: "Please input password!",
        buttons: ["OK"],
      });
      return false;
    }

    return true;
  }
  const hasLogin = useHasLogin();
  const router = useIonRouter();
  if (hasLogin) {
    router.push(routes.record);
  }
  const dispatch = useDispatch<RootThunkDispatch>();

  async function handleLogin() {
    const error = await dispatch(loginThunk({ username, password }));
    console.log(error);
    if (error) {
      validateInput(error);
    }
  }
  return (
    <IonPage className={styles.page}>
      <IonContent>
        <div className={styles.waveContainer}>
          <div className={styles.wave}></div>
        </div>{" "}
        <IonCard>
          <IonCardHeader className={styles.title}>Login</IonCardHeader>
          <IonItem className="ion-padding">
            <IonLabel>Username : </IonLabel>
            <IonInput
              value={username}
              placeholder="username"
              onIonChange={(e) => setUsername(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem className="ion-padding">
            <IonLabel>Password : </IonLabel>
            <IonInput
              value={password}
              type="password"
              placeholder="password"
              onIonChange={(e) => setPassword(e.detail.value!)}
            ></IonInput>
          </IonItem>
        </IonCard>
        <IonButton
          className="ion-padding"
          expand="block"
          onClick={() => {
            validateInput() && handleLogin();
          }}
        >
          Login
        </IonButton>
        <IonButton
          className="ion-padding"
          expand="block"
          routerLink="/register"
        >
          Go to register
        </IonButton>
      </IonContent>
    </IonPage>
  );
};

export default LoginPage;
