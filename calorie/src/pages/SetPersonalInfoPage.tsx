import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCard,
  IonCardHeader,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonItemDivider,
  IonItemOption,
  IonItemSliding,
  IonLabel,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar,
  useIonAlert,
  useIonRouter,
} from "@ionic/react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { post } from "../api";
import { RootThunkDispatch } from "../redux/dispatch";
import { updateProfileAction } from "../redux/profile/action";
import { loadProfileThunk } from "../redux/profile/thunk";
import { RootState } from "../redux/state";
import { routes } from "../Routes";

type newProfile = {
  nickname: string | null;
  height: number | null | string;
  weight: number | null | string;
  age: number | null | string;
  gender: string | null;
  activity: string | null;
  aim: string | null;
};

const SetPersonalInfoPage: React.FC = () => {
  let profileState = useSelector((state: RootState) => state.profile);
  const dispatch = useDispatch<RootThunkDispatch>();
  const router = useIonRouter();

  const [newProfile, setNewProfile] = useState<newProfile>({
    nickname: profileState.nickname,
    height: profileState.height,
    weight: profileState.weight,
    age: profileState.age,
    gender: profileState.gender,
    activity: profileState.activity,
    aim: profileState.aim,
  });

  function updateProfile() {
    console.log(newProfile);

    const submit = async () => {
      try {
        const result = await post("/profile", {
          ...newProfile,
          height: newProfile.height ? +newProfile.height : -1,
          weight: newProfile.weight ? +newProfile.weight : -1,
          age: newProfile.age ? +newProfile.age : -1,
        });
        console.log(result);
        if (result.success) {
          dispatch(loadProfileThunk());
          router.push(routes.profile);
        }
      } catch (error) {
        return { error: String(error) };
      }
    };
    submit();
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="orange">
          <IonButtons slot="start">
            <IonBackButton defaultHref={routes.profile} />
          </IonButtons>
          <IonTitle>Profile</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonItem>
          <IonLabel>Nickname :</IonLabel>
          <IonInput
            type="text"
            value={newProfile.nickname}
            onIonChange={(e) =>
              setNewProfile({ ...newProfile, nickname: e.detail.value! })
            }
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel>Height :</IonLabel>
          <IonInput
            type="text"
            value={newProfile.height}
            onIonChange={(e) =>
              setNewProfile({ ...newProfile, height: e.detail.value! })
            }
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel>Weight :</IonLabel>
          <IonInput
            type="text"
            value={newProfile.weight}
            onIonChange={(e) =>
              setNewProfile({ ...newProfile, weight: e.detail.value! })
            }
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel>Age :</IonLabel>
          <IonInput
            type="text"
            value={newProfile.age}
            onIonChange={(e) =>
              setNewProfile({ ...newProfile, age: e.detail.value! })
            }
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel>Gender :</IonLabel>

          <IonSelect
            value={newProfile.gender}
            onIonChange={(e) => {
              setNewProfile({ ...newProfile, gender: e.detail.value! });
            }}
          >
            <IonSelectOption value="male">Male</IonSelectOption>
            <IonSelectOption value="female">Female</IonSelectOption>
          </IonSelect>
        </IonItem>
        <IonItem>
          <IonLabel>Goal :</IonLabel>
          <IonInput
            type="text"
            value={newProfile.aim}
            onIonChange={(e) =>
              setNewProfile({ ...newProfile, aim: e.detail.value! })
            }
          ></IonInput>
        </IonItem>

        <IonItem>
          Activity :
          <IonSelect
            style={{ flex: 1 }}
            value={newProfile.activity}
            onIonChange={(e) => {
              setNewProfile({ ...newProfile, activity: e.detail.value! });
            }}
          >
            <IonSelectOption value={1}>
              Sedentary (little or no exercise)
            </IonSelectOption>
            <IonSelectOption value={2}>
              Lightly active (light exercise/sports 1-3 days/week)
            </IonSelectOption>
            <IonSelectOption value={3}>
              Moderately active (moderate exercise/sports 3-5 days/week)
            </IonSelectOption>
            <IonSelectOption value={4}>
              Very active (hard exercise/sports 6-7 days a week)
            </IonSelectOption>
            <IonSelectOption value={5}>
              Extra active (very hard exercise/sports & physical job or 2x
              training)
            </IonSelectOption>
          </IonSelect>
        </IonItem>

        <IonButton onClick={() => updateProfile()} className="ion-padding">
          Submit
        </IonButton>
      </IonContent>
    </IonPage>
  );
};

export default SetPersonalInfoPage;
