import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { RootThunkDispatch } from "../dispatch";
import { autoLoginThunk } from "./thunk";

export function AuthProvider(props: { children: any }) {
  const dispatch = useDispatch<RootThunkDispatch>();
  useEffect(() => {
    dispatch(autoLoginThunk());
  }, [dispatch]);
  return <>{props.children}</>;
}
