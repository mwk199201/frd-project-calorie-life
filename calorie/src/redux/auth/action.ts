

export function loginAction(input: {
    token: string
    user_id: number

}) {
    return { type: '@@Auth/login' as const, ...input }
}

export function logoutAction() {
    return { type: '@@Auth/logout' as const }

}
export function loginFailedAction(error: string) {
    return { type: '@@Auth/loginFail' as const, error }

}

export type AuthAction =
    | ReturnType<typeof loginAction>
    | ReturnType<typeof logoutAction>
    | ReturnType<typeof loginFailedAction>
