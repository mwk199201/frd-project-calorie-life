
import { Dispatch } from "react";
import { post } from "../../api";

import { RootThunkDispatch } from '../dispatch'
import { AuthAction, loginAction, loginFailedAction, logoutAction } from "./action";
import jwt from 'jwt-decode'
import { JWTPayload } from "../../api.types";
import { storages } from "./storage";

export function loginThunk(input: {
    username: string
    password: string
}) {

    return async (dispatch: Dispatch<AuthAction>) => {
        try {
            console.log(input);

            const result = await post(`/login/password`, input)
            // const result = await res.json()

            console.log(result);
            storages.token.setValue(result.token)

            if (result.error) {
                console.log(result);
                dispatch(loginFailedAction(result.error))
                return result.error
            } else {
                console.log(result);
                let payload: JWTPayload = jwt(result.token)

                dispatch(loginAction({
                    token: result.token,
                    user_id: payload.id,
                }));

            }
        } catch (e) {
            return { error: String(e) };

        }
    }
}

export function autoLoginThunk() {
    return (dispatch: RootThunkDispatch) => {
        let token = storages.token.getValue()

        if (!token) {
            dispatch(logoutAction())
            return
        }
        let payload: JWTPayload = jwt(token)
        dispatch(
            loginAction({
                token: token,
                user_id: payload.id
            }),
        )
    }

}

export function logoutThunk() {
    return (dispatch: RootThunkDispatch) => {
        console.log("logout");
        storages.token.clear()
        localStorage.clear()
        dispatch(logoutAction())
    }
}
