import { AuthAction } from "./action";
import { AuthState } from "./state";
import { storages } from "./storage";
import jwt from 'jwt-decode'
import { JWTPayload } from "../../api.types";


function initialSate(): AuthState {
    try {
        let token = storages.token.getValue()
        if (token) {
            let payload: JWTPayload = jwt(token)
            return { status: 'authenticated', token, user_id: payload.id }
        }
    } catch (error) {
        console.log("fail to decode jwt", error);

    }

    return {
        status: 'guest'
    }
}

export let authReducer = (state: AuthState = initialSate(), action: AuthAction): AuthState => {
    switch (action.type) {
        case '@@Auth/login':
            return { status: 'authenticated', ...action }
        case '@@Auth/loginFail':
            return { status: 'error', error: action.error }
        case '@@Auth/logout':
            return { status: 'guest' }
        default:
            return state
    }

}