export type AuthState =
    | { status: 'guest' }
    | {
        status: 'authenticated'
        user_id: number
        token: string
    }
    | {
        status: 'error'
        error: string
    }