import { Dispatch } from "react"
import {  get } from "../../api"
import { ProfileAction, profileNotReadyAction, showProfileAction, showProfileErrorAction } from "./action"


export function loadProfileThunk() {
    return async (dispatch: Dispatch<ProfileAction>) => {
        const profile = await get("/profile");
        // console.log(profile);

        if (profile.error) {
            dispatch(showProfileErrorAction(profile.error))
        } if (profile === false) {
            dispatch(profileNotReadyAction(profile));

        }
        else {
            const data = profile[0]
            // console.log(data);
            // localStorage.setItem('profile', data)

            dispatch(showProfileAction(data));

        }
    }

}
