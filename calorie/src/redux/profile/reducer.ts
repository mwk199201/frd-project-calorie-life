import { RootState } from "../state"
import { ProfileAction } from "./action"
import { ProfileState } from "./state"



function initialSate(): ProfileState {
    return {
        status: "not-ready",
        nickname: null,
        height: null,
        weight: null,
        age: null,
        activity: null,
        aim: null,
        icon_image: null,
        user_role: "member",
        gender: null
    }
}

// function commentInitialSate(): CommentItem {
//     return {
//         id:0,

//     }
// }


export function profileReducer(
    state: ProfileState = initialSate(),
    action: ProfileAction,
): ProfileState {
    switch (action.type) {
        case '@@Profile/showProfile': {
            return {
                ...state, status: 'ready', ...action.profile
            }
        }
        case "@@Profile/notReady": {
            return {
                ...state = initialSate()
            }
        }

        case '@@Profile/showError':
            return { ...state, status: 'error', error: action.error }
        default:
            return state
    }
}

// export function blogItemReducer(state: CommentItem = blogItemInitialSate(),
//     action: BlogListAction): CommentItem {

// }



export const selectProfile = (state: RootState) => state.profile





