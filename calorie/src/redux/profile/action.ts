import { Profile } from "./state";

export function showProfileAction(profile: Profile) {
    return { type: '@@Profile/showProfile' as const, profile }
}

export function profileNotReadyAction(profile: Profile) {
    return { type: '@@Profile/notReady' as const, profile }

}


export function showProfileErrorAction(error: string) {
    return { type: '@@Profile/showError' as const, error }
}

export function updateProfileAction(newProfile: Profile) {
    return { type: '@@Profile/updateProfile' as const, newProfile }
}


export type ProfileAction =
    | ReturnType<typeof showProfileAction>
    | ReturnType<typeof showProfileErrorAction>
    | ReturnType<typeof profileNotReadyAction>
    | ReturnType<typeof updateProfileAction>
