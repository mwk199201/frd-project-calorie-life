

export type ProfileState = Profile & {
    error?: string
    status: string
}
export type Profile = {
    nickname: string | null
    height: number | null
    weight: number | null;
    age: number | null;
    activity: string | null;
    aim: string | null;
    icon_image?: string | null;
    user_role: string
    gender: string | null
}



