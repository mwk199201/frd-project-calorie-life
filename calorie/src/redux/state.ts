import { AuthState } from "./auth/state"
import {  BlogListState } from "./blogList/state"
import { RecordListState } from "./calories_record/record_state"
// import { RecordListState } from "./calories_record/state"
import { ChatListState } from "./chatroom/state"
import { ProfileState } from "./profile/state"
export * from './blogList/state'

export type RootState = {
    blogList: BlogListState
    calorieRecord: RecordListState
    chatList: ChatListState
    auth: AuthState
    profile: ProfileState

}