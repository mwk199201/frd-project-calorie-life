import { applyMiddleware, compose, legacy_createStore as createStore } from "redux";
import { rootReducer } from "./reducer";
// import logger from 'redux-logger'
import thunk from 'redux-thunk'




declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION__: any
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any

    }
}


let composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose


let enhancer = composeEnhancers(
    // applyMiddleware(logger),
    applyMiddleware(thunk),
    // applyMiddleware(logger),
)


export let store = createStore(rootReducer, enhancer)

