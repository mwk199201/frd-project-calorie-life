import { BlogItem } from "./state";

export function showBlogListAction(blogList: BlogItem[]) {
    return { type: '@@BlogList/showList' as const, blogList }
}
// export function showBlogLikeAction(blog_likes: any) {
//     return { type: '@@BlogList/showLike' as const, blog_likes }
// }
export function showBlogCommentAction(comment_records: any, blog_id: number) {
    return { type: '@@BlogList/showComment' as const, comment_records, blog_id }
}
export function showBlogCommentErrorAction(error: string) {
    return { type: '@@BlogList/showCommentError' as const, error }
}
export function showBlogListErrorAction(error: string) {
    return { type: '@@BlogList/showError' as const, error }
}

export function updateSavedBlogs(blog_id: number, hasSaved: string) {
    return { type: '@@BlogList/updateSavedBlogs' as const, blog_id, hasSaved }
}
export function delBlogAction(blog_id: number) {
    return { type: '@@BlogList/delBlog' as const, blog_id }
}
export function delBlogErrorAction(error: string) {
    return { type: '@@BlogList/delBlogError' as const, error }
}

export function delLikeAction(blog_id: number, has_like: string) {
    return { type: '@@BlogList/delLike' as const, blog_id, has_like }
}
export function updateBlockedBlogs(blog_id: number, has_block: string) {
    return { type: '@@BlogList/updateBlockedBlogs' as const, blog_id, has_block }
}
export function addLikeAction(blog_id: number, has_like: string) {
    return { type: '@@BlogList/addLike' as const, blog_id, has_like }
}

export type BlogListAction =
    | ReturnType<typeof showBlogListAction>
    | ReturnType<typeof showBlogCommentAction>
    | ReturnType<typeof showBlogCommentErrorAction>
    | ReturnType<typeof showBlogListErrorAction>
    | ReturnType<typeof updateSavedBlogs>
    | ReturnType<typeof delBlogAction>
    | ReturnType<typeof delBlogErrorAction>
    | ReturnType<typeof delLikeAction>
    | ReturnType<typeof addLikeAction>
    | ReturnType<typeof updateBlockedBlogs>

