import { Dispatch } from "react"
import {
    BlogListAction, delBlogAction, delBlogErrorAction, showBlogCommentAction,
    showBlogCommentErrorAction,
    showBlogListAction,
    showBlogListErrorAction,

} from "./action"
import { APIOrigin } from "../../api"
// import { RootState } from "../state"
// import { BlogItem } from "./state"

export function loadBlogListThunk() {
    return async (dispatch: Dispatch<BlogListAction>) => {
        const res = await fetch(`${APIOrigin}/blogs`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "content-type": "application/json; charset=utf-8"
            },

        })

        const result = await res.json()

        if (result.error) {
            dispatch(showBlogListErrorAction(result.error))
        } else {
            dispatch(showBlogListAction(result.blogList));

        }
    }

}
export function loadBlogListBySearchThunk(body: any) {
    return async (dispatch: Dispatch<BlogListAction>) => {
        const res = await fetch(`${APIOrigin}/blogs/search`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "content-type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(body)
        })

        const result = await res.json()

        if (result.error) {
            dispatch(showBlogListErrorAction(result.error))
        } else {
            dispatch(showBlogListAction(result.blogList));

        }
    }

}

export function loadBlogCommentsThunk(blog_id: number, counter: number) {

    return async (dispatch: Dispatch<BlogListAction>) => {
        const formObject = {
            id: blog_id,
            counter: counter
        };
        const res = await fetch(`${APIOrigin}/blog/${blog_id}`, {
            method: "POST",
            headers: {
                "content-type": "application/json; charset=utf-8",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(formObject),
        })
        const result = await res.json()
        const comment_records = result.comment_records.rows
        // console.log('comment_records', comment_records);


        if (comment_records.error) {
            dispatch(showBlogCommentErrorAction(result.error))
        } else {
            dispatch(showBlogCommentAction(comment_records, blog_id));
        }
    }

}

export function postCommentThunk(blog_id: number, commentContent: string) {
    return async (dispatch: Dispatch<BlogListAction>) => {
        const formObject = {
            blog_id: blog_id,
            commentContent: commentContent,

        };
        const res = await fetch(`${APIOrigin}/blog/post/${blog_id}`, {
            method: "POST",
            headers: {
                "content-type": "application/json; charset=utf-8",
                "Authorization": `Bearer ${localStorage.getItem('token')}`

            },
            body: JSON.stringify(formObject),
        })
        const result = await res.json()
        const comment_records = result.comment_records.rows
        // console.log("result", result);
        if (result.error) {
            dispatch(showBlogCommentErrorAction(result.error))
        } else {
            dispatch(showBlogCommentAction(comment_records, blog_id));
        }
    }
}

export function delPostThunk(blog_id: number) {
    return async (dispatch: Dispatch<BlogListAction>) => {
        const res = await fetch(`${APIOrigin}/blog/del/${blog_id}`, {
            method: "DELETE",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },

        })
        const result = await res.json()
        if (result.error) {
            dispatch(delBlogErrorAction(result.error))

        } else {
            dispatch(delBlogAction(blog_id))
        }


    }
}