
export type BlogListState = {
    status: 'loading' | 'error' | 'ready'
    error?: string
    items?: BlogItem[]
}




export type BlogItem = {
    id: number
    poster_id: number
    nickname: string
    icon_image: string
    title: string
    content: string
    image: string | null
    like_count: number
    has_like: boolean | string
    has_save: boolean | string
    has_block: boolean | string
    has_report: boolean | string
    updated_at: string
    commentListStatus: 'loading' | 'error' | 'ready'
    commentList?: CommentItem[]
}



export type CommentItem =
    {
        id: number
        commenter_name: string
        commenter_icon: string
        commenter_content: string
        comment_time: string
    }

