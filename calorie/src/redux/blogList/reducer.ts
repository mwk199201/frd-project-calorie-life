import { RootState } from "../state";
import { BlogListAction } from "./action";
import { BlogItem, BlogListState } from "./state";

function initialSate(): BlogListState {
    return {
        status: 'loading',
        // items: []
    }
}

// function commentInitialSate(): CommentItem {
//     return {
//         id:0,

//     }
// }


export function blogListReducer(
    state: BlogListState = initialSate(),
    action: BlogListAction,
): BlogListState {
    switch (action.type) {
        case '@@BlogList/showList': {

            return {

                status: 'ready',
                items: action.blogList.map((item: BlogItem) =>
                // (console.log(item)
                (
                    // console.log('item', item),
                    {
                        id: item.id,
                        nickname: item.nickname,
                        poster_id: item.poster_id,
                        icon_image: item.icon_image,
                        content: item.content,
                        image: item.image,
                        title: item.title,
                        like_count: item.like_count,
                        commentListStatus: 'loading',
                        updated_at: item.updated_at,
                        has_like: item.has_like,
                        has_save: item.has_save,
                        has_block: item.has_block,
                        has_report: item.has_report,
                        commentList: []
                        // blog_comments: item.comments,
                    }
                )

                )
            }
        }
        case '@@BlogList/showComment': {
            // console.log(action.comment_records)

            const finalData = state.items?.map(v => {
                if (v.id !== action.blog_id) {
                    return v
                }
                else {
                    return {
                        ...v,
                        commentList: action.comment_records
                    }
                }
            })

            return {
                status: 'ready',
                items: finalData
            }

        }

        case '@@BlogList/showError':
            return { status: 'error', error: action.error }

        case '@@BlogList/updateSavedBlogs': {
            const hasSaved = action.hasSaved

            const newItems = state.items?.map((item) => {
                if (item.id === action.blog_id) {
                    item.has_save = hasSaved
                }
                return item
            })
            return {
                status: 'ready',
                items: newItems
            }
        }
        case '@@BlogList/delBlog': {
            const newBlogList = state.items?.filter(item =>
                item.id !== action.blog_id
            )
            return {
                status: 'ready',
                items: newBlogList
            }
        }
        case '@@BlogList/addLike': {
            const has_like = action.has_like
            const newItems = state.items?.map((item) => {
                if (item.id === action.blog_id) {
                    item.like_count = +item.like_count + 1
                    item.has_like = has_like
                }
                return item
            })
            return {
                status: 'ready',
                items: newItems
            }
        }
        case '@@BlogList/delLike': {
            const newItems = state.items?.map((item) => {
                if (item.id === action.blog_id) {
                    item.like_count = +item.like_count - 1
                    item.has_like = action.has_like
                }
                return item
            })
            return {
                status: 'ready',
                items: newItems
            }
        }
        case '@@BlogList/updateBlockedBlogs': {
            const newBlogList = state.items?.map((item) => {
                if (item.id === action.blog_id) {
                    item.has_block = action.has_block
                }
                return item
            })
            return {
                status: 'ready',
                items: newBlogList
            }
        }

        case '@@BlogList/delBlogError':
            return { status: 'error', error: action.error }

        default:
            return state
    }
}

// export function blogItemReducer(state: CommentItem = blogItemInitialSate(),
//     action: BlogListAction): CommentItem {

// }



export const selectBlogList = (state: RootState) => state.blogList.items



export const selectBlogStatus = (state: RootState) => state.blogList.status

