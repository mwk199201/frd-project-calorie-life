import { AuthAction } from "./auth/action";
import { BlogListAction } from "./blogList/action";
import { CalorieRecordAction } from "./calories_record/record_action";
// import { CalorieRecordAction } from "./calories_record/action";
import { ChatListAction } from "./chatroom/action";

export type RootAction =
    | BlogListAction
    | CalorieRecordAction
    | ChatListAction
    | AuthAction
