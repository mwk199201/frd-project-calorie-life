import { combineReducers, Reducer } from "redux";
import { RootAction } from "./action";
import { authReducer } from "./auth/reducer";
import { blogListReducer } from "./blogList/reducer";
import { calorieRecordReducer } from "./calories_record/record_reducer";
// import { calorieRecordReducer } from "./calories_record/reducer";
import { ChatListReducer } from "./chatroom/reducer";
import { profileReducer } from "./profile/reducer";
import { RootState } from "./state";


export let rootReducer: Reducer<RootState, RootAction> = combineReducers({
    blogList: blogListReducer,
    calorieRecord: calorieRecordReducer,
    chatList: ChatListReducer,
    auth: authReducer,
    profile: profileReducer
})