
import { ChatListAction } from "./action";
import { ChatListState } from "./state";

function initialSate(): ChatListState {
  return {
    status: "loading",
    items: [],
  };
}

export function ChatListReducer(
  state: ChatListState = initialSate(),
  action: ChatListAction
): ChatListState {
  switch (action.type) {
    case "@@ChatList/showList": {
      return {
        status: "ready",
        items: action.chatList,
      };
    }

    default:
      return state;
  }
}
