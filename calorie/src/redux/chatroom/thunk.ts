import { Dispatch } from "react"
import {
    ChatListAction,
 showChatListAction
} from "./action"
import { APIOrigin } from "../../api"

export function loadChatListThunk() {
    return async (dispatch: Dispatch<ChatListAction>) => {
        const res = await fetch(`${APIOrigin}/chatrooms`,{
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const result = await res.json()
        console.log(result);
        dispatch(showChatListAction(result));
    }

}


