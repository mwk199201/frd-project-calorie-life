
export type ChatListState = {
    status: 'loading' | 'error' | 'ready'
    error?: string
    items: ChatListItem[]
}

export type ChatListItem = {
    id: number
    room_id:number
    receiver_id: number
    nickname:string
    user_role:string
    icon_image: string | undefined
    created_at:  string
    message:string

}


