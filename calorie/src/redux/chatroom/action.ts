import { ChatListItem} from "./state";



export function showChatListAction(chatList: ChatListItem[]) {
  return { type: "@@ChatList/showList" as const, chatList };
}

export type ChatListAction =
  | ReturnType<typeof showChatListAction>

