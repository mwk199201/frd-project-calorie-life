import { Dispatch } from "react";
import { APIOrigin } from "../../api";
import { CalorieRecordAction, showCalorieRecordAction, showCalorieRecordErrorAction, showExerciseAction, showFoodDataAction, showQuickAddCalorieRecord, showWaterAction } from "./record_action";
import { RecentFoodList, ReQuickAddItem } from "./record_state";

export function loadFoodDataThunk(searchItem:string){
    return async(dispatch: Dispatch<CalorieRecordAction>)=>{
        const res=await fetch(`${APIOrigin}/foodData/:${searchItem}`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const result = await res.json();
        console.log('thrunk',result)
        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        } else {
            dispatch(showFoodDataAction(result))
        }
    }
}

export function loadCalorieRecordThunk(date:string){
    return async(dispatch: Dispatch<CalorieRecordAction>)=>{
        const res=await fetch(`${APIOrigin}/foodDate/caloriesRecord/${date}`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const result = await res.json();
        // console.log('thrunk',result)
        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        } else {
            dispatch(showCalorieRecordAction(result))
        }
    }
}

export function loadQuickAddFoodDataThunk(date:string){
    return async(dispatch:Dispatch<CalorieRecordAction>)=>{
        const res=await fetch(`${APIOrigin}/foodData/quickAddCaloriesRecord/:${date}`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const result=await res.json()
        // console.log('quick',result)
        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        } else {
            dispatch(showQuickAddCalorieRecord(result))
        }
    }

}

export function loadQuickAddFoodDataByIdThunk(foodID: number) {
    return async (dispatch: Dispatch<CalorieRecordAction>) => {
        const res = await fetch(`${APIOrigin}/foodData/quickAddFoodDetail/${foodID}`)
        const result = await res.json()

        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        } else {
            dispatch(showQuickAddCalorieRecord(result))
        }
    }
}

export function loadRecentAddFoodDataByIdThunk(foodID: number) {
    return async (dispatch: Dispatch<CalorieRecordAction>) => {
        const res = await fetch(`${APIOrigin}/foodData/recentAddFoodDetail/${foodID}`)
        const result = await res.json()

        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        } else {
            dispatch(showCalorieRecordAction(result))
        }
    }
}

export function changeQuickAddCalorieRecord(foodID: number, recordList: ReQuickAddItem) {
    return async (dispatch: Dispatch<CalorieRecordAction>) => {
        const res = await fetch(`${APIOrigin}/foodData/newQuickFoodItem/${foodID}`, {
            method: 'PATCH',
            headers: { "content-type": "application/json; charset=utf-8" },
            body: JSON.stringify(recordList)
        })
        const result = await res.json()
        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        }
        else {
            // dispatch(showQuickAddCalorieRecord(result))
            console.log('done', recordList)
        }
    }
}

export function changeRecentAddCalorieRecord(foodID: number, recordList: RecentFoodList) {
    return async (dispatch: Dispatch<CalorieRecordAction>) => {
        const res = await fetch(`${APIOrigin}/foodData/newRecentFoodItem/${foodID}`, {
            method: 'PATCH',
            headers: { "content-type": "application/json; charset=utf-8" },
            body: JSON.stringify(recordList)
        })
        const result = await res.json()
        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        }
        else {
            // dispatch(showQuickAddCalorieRecord(result))
            console.log('done', recordList)
        }
    }

}

export function deleteRecentItem(foodID: number) {
    return async (dispatch: Dispatch<CalorieRecordAction>) => {
        const res = await fetch(`${APIOrigin}/foodData/deleteRecentFoodItem/${foodID}`, { method: 'DELETE' })
        const result = await res.json()
        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        } else {
            // dispatch(showQuickAddCalorieRecord(result))
            console.log('deleted')
        }
    }
}

export function deleteQuickItem(foodID: number) {
    return async (dispatch: Dispatch<CalorieRecordAction>) => {
        const res = await fetch(`${APIOrigin}/foodData/deleteQuickFoodItem/${foodID}`, { method: 'DELETE' })
        const result = await res.json()
        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        } else {
            // dispatch(showQuickAddCalorieRecord(result))
            console.log('deleted')
        }
    }
}

export function deleteExerciseItem(ID: number){
    return async(dispatch:Dispatch<CalorieRecordAction>)=>{
        const res=await fetch(`${APIOrigin}/exerciseData/deleteExerciseItem/${ID}`,{ method: 'DELETE' })
        const result = await res.json()
        if(result.error){
            dispatch(showCalorieRecordErrorAction(result.error))
        }   else{
            // dispatch(showQuickAddCalorieRecord(result))
            console.log('deleted')
        }   
    }
}

export function addWaterThunk(user_id: number, cupOfWater: number, date:string) {

    return async (dispatch: Dispatch<CalorieRecordAction>) => {
        const res = await fetch(`${APIOrigin}/addWater/${date}`, {
            method: "POST",
            headers: {
                "content-type": "application/json; charset=utf-8",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({ cupOfWater }),
        })
        const result = await res.json()
        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        } else {
            // dispatch(showQuickAddCalorieRecord(result))
            console.log('complete')
        }
    }

}

export function loadWaterThunk(date: string) {
    return async (dispatch: Dispatch<CalorieRecordAction>) => {
        const res = await fetch(`${APIOrigin}/water/${date}`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })

        const result = await res.json()

        if (result.error) {
            dispatch(showCalorieRecordErrorAction(result.error))
        } else {
            dispatch(showWaterAction(result));

        }
    }

}

export function loadExerciseThunk(date:string) {
    return async (dispatch:Dispatch<CalorieRecordAction>) => {
        const res = await fetch(`${APIOrigin}/exerciseDataRecord/${date}`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const result = await res.json()
        // console.log({result})
        if(result.error){
            dispatch(showCalorieRecordErrorAction(result.error))
        }   else{
            dispatch(showExerciseAction(result));
        }
    }

}