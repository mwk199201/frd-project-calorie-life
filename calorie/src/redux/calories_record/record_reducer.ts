import { CalorieRecordAction } from "./record_action";
import { ExerciseList, FoodDataList, FoodList, QuickAddItem, RecordListState, WaterList } from "./record_state";

function initialSate(): RecordListState {
    return {
        status: 'loading',
        // items: []
    }
}


export function calorieRecordReducer(
    state: RecordListState= initialSate(),
    action:CalorieRecordAction,
): RecordListState {
    switch (action.type) {
        case '@@dietRecord/showList': {
            return{
                status:'ready',
                items:action.recordList
                .map((item: FoodDataList) =>
                ({
                    calories: item.calories,
                    carbohydrate: item.carbohydrate,
                    cholesterol: item.cholesterol,
                    created_at: item.created_at,
                    dietary: item.dietary,
                    food_name: item.food_name,
                    food_type: item.food_type,
                    food_weight: item.food_weight,
                    id: item.id,
                    protein: item.protein,
                    saturated_fat: item.saturated_fat,
                    sodium: item.sodium,
                    sugar: item.sugar,
                    total_fat: item.total_fat,
                    trans_fat: item.trans_fat,
                    diet_type: item.diet_type,
                    user_id: item.user_id,
                })    
                )
            }
    }
        case '@@dietRecord/showFoodDataList': {
            return{
                status:'ready',
                data:action.recordList
                .map((item: FoodList) =>
                ({
                    calories: item.calories,
                    carbohydrate: item.carbohydrate,
                    cholesterol: item.cholesterol,
                    dietary: item.dietary,
                    food_chi_name: item.food_chi_name,
                    food_eng_name: item.food_eng_name,
                    food_type: item.food_type,
                    food_weight: item.food_weight,
                    id: item.id,
                    protein: item.protein,
                    saturated_fat: item.saturated_fat,
                    sodium: item.sodium,
                    sugar: item.sugar,
                    total_fat: item.total_fat,
                    trans_fat: item.trans_fat,
                    isselected: item.isselected,
                })    
                )
            }
    }
    case '@@dietRecord/showQuickAddList':{
        return{
            status:'ready',
            quickItem:action.recordList
                .map((item:QuickAddItem)=>({
                    id:item.id,
                    user_id: item.user_id,
                    food_name: item.food_name,
                    diet_type: item.diet_type,
                    calories: item.calories,
                    protein: item.protein,
                    carbohydrate: item.carbohydrate,
                    fat:item.fat
                }))
        }
    }

    case '@@dietRecord/showWater':{
        return{
            status:'ready',
            water:action.water
                .map((item:WaterList)=>({
                    id:item.id,
                    user_id: item.user_id,
                    cup_of_water: item.cup_of_water,
                }))
        }
    }

    case '@@dietRecord/showExercise':{
        return{
            status:'ready',
            exercise:action.exercise
                .map((item:ExerciseList)=>({
                    id:item.id,
                    exercise_type:item.exercise_type,
                    exercise_mins: item.exercise_mins,
                    exercise_calories: item.exercise_calories,
                }))
        }
    }
    case '@@dietRecord/showError':
        return { status: 'error', error: action.error }
    default:
            return state
}
}