export type RecordListState = {
    status: 'loading' | 'error' | 'ready'
    error?: string
    items?: FoodDataList[]
    data?: FoodList[]
    quickItem?:QuickAddItem[]
    water?: WaterList[]
    exercise?: ExerciseList[]
}

export type FoodDataList = {
    calories: number;
    carbohydrate: number;
    cholesterol: number;
    created_at: string;
    dietary: number;
    food_name: string;
    food_type: string;
    diet_type: string;
    food_weight: number;
    id: number;
    protein: number;
    saturated_fat: number;
    sodium: number;
    sugar: number;
    total_fat: number;
    trans_fat: number;
    user_id: number;
  };

export type FoodList = {
    calories: number;
    carbohydrate: number;
    cholesterol: number;
    dietary: number;
    food_chi_name: string;
    food_eng_name: string;
    food_type: string;
    food_weight: number;
    id: number;
    protein: number;
    saturated_fat: number;
    sodium: number;
    sugar: number;
    total_fat: number;
    trans_fat: number;
    isselected: boolean;
  };

  export type QuickAddItem={
    id:number
    user_id:number
    food_name:string,
    diet_type:string,
    calories:number,
    carbohydrate:number,
    protein:number,
    fat:number,
}

  
  export type ReQuickAddItem={
    calories:number,
    carbohydrate:number,
    protein:number,
    fat:number
  }

  export type RecentFoodList = {
    calories: number;
    carbohydrate: number;
    cholesterol: number;
    dietary: number;
    food_weight: number;
    protein: number;
    saturated_fat: number;
    sodium: number;
    sugar: number;
    total_fat: number;
    trans_fat: number;
  };

  export type WaterList = {
    id:number;
    user_id:number;
    cup_of_water: number;
  }

  export type ExerciseList={
    id:number;
    exercise_type:string;
    exercise_mins:number;
    exercise_calories:number;
  }