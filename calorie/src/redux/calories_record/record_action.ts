import { ExerciseList, FoodDataList, FoodList, QuickAddItem, WaterList } from "./record_state";

export function showCalorieRecordAction(recordList:FoodDataList[]){
    return { type: '@@dietRecord/showList' as const, recordList }
}

export function showFoodDataAction(recordList:FoodList[]){
    return { type: '@@dietRecord/showFoodDataList' as const, recordList }
}

export function showQuickAddCalorieRecord(recordList:QuickAddItem[]){
    return { type: '@@dietRecord/showQuickAddList' as const, recordList }
}

export function showCalorieRecordErrorAction(error:string){
    return {type:'@@dietRecord/showError' as const, error}
}

export function showWaterAction(water:WaterList[]){
    return {type:'@@dietRecord/showWater' as const, water}
}

export function showExerciseAction(exercise:ExerciseList[]){
    return {type:'@@dietRecord/showExercise' as const, exercise}
}



export type CalorieRecordAction =
    | ReturnType<typeof showFoodDataAction>
    | ReturnType<typeof showCalorieRecordAction>
    | ReturnType<typeof showCalorieRecordErrorAction>
    | ReturnType<typeof showQuickAddCalorieRecord>
    | ReturnType<typeof showWaterAction>
    | ReturnType<typeof showExerciseAction>